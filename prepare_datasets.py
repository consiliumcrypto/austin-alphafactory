import json
from src.TargetLabeler import TargetLabeler
from src.CandleDataset import CandleDataset
from src.enum_classes import *
import multiprocessing as mp
from src.cc_ts_utils import get_trade_candles
#from src.cc_ts_utils import read_data_file
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
import platform
import csv
from pathlib import Path
import os
import hashlib

dirname = os.path.dirname(__file__)
homepath=str(Path.home())

#This determines whether to run a smaller "test" run (shorter duration) or full length
TESTING = False
GOOGLE_STORAGE_BUCKET_NAME = 'austin-alphafactory-jan24-2020'

################################################################################
################################################################################
with open('credentials/consilium_api_key.txt') as f:
    CONSILIUM_API_KEY = f.readlines()
    CONSILIUM_API_KEY = [x.strip() for x in CONSILIUM_API_KEY][0]

# Load Consilium API Key --> Used to pull raw market data/candles from internal API
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]
    path_to_gs_creds = '/home/ubuntu/austin-alphafactory/credentials/cc-master-storage-restricted-readonly.json'
else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]
    path_to_gs_creds = '/Users/austin/Documents/consilium/bitbucket/austin-alphafactory/credentials/cc-master-storage-restricted-readonly.json'
################################################################################
# Cloud storage variables
# NOTE: You must already have an environment variable setup with GCP key path

google_storage_obj = GoogleStorageClient(GOOGLE_STORAGE_BUCKET_NAME)

################################################################################
# Assets/exchanges, time periods, target labels
#GDAX Pairs that are available as of Sept 1, 2018
#ASSET_NAMES = ["BTC_EUR","LTC_EUR","ETH_USD","ETH_EUR","ETC_BTC","LTC_USD","ETC_EUR","BCH_BTC","ETC_USD","BTC_USD","BTC_GBP","BCH_EUR","ETH_BTC","BCH_USD"]

#Need to be in "XXX_YYY" --> standardized Consilium format
ASSET_NAMES = ['ETH_BTC']
EXCHANGE_NAME = ['binance']
CANDLE_PERIODS = ['30T', '1H', '3H']
#CANDLE_PERIODS = ['6H']
SAMPLING_TYPES = ['time_bars']
TERNARY_MOMENTUM_THRESHOLDS = [0.0015, 0.002, 0.003, 0.005]
PREDICTION_HORIZONS = [1]
COL_TO_LABEL = 'price_close'

TARGET_LABEL_TYPES = [TargetType.BINARY_MOMENTUM, TargetType.TERNARY_MOMENTUM]

################################################################################
# Local (temp) storage directories for labeled files
#TODO: Add dynamic folder naming based on sampling_type
BINARY_MOMENTUM_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_binary_momentum')
TERNARY_MOMENTUM_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_ternary_momentum')
PRICE_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_price')
ABS_RETURN_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_abs_return')
PERC_RETURN_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_perc_return')

labeled_filepaths = {
    TargetType.BINARY_MOMENTUM: BINARY_MOMENTUM_DESTINATION_FOLDER,
    TargetType.TERNARY_MOMENTUM: TERNARY_MOMENTUM_DESTINATION_FOLDER,
    TargetType.PRICE: PRICE_DESTINATION_FOLDER,
    TargetType.ABS_RETURN: ABS_RETURN_DESTINATION_FOLDER,
    TargetType.PERC_RETURN: PERC_RETURN_DESTINATION_FOLDER
}
################################################################################

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'prepare_datasets.py', use_dict_cursor=True )

################################################################################
################################################################################
# Create all possible permutations of candle_period and pred_horizon, store them in "dataset_periods"
dataset_periods = []

for candle_period in CANDLE_PERIODS:
    for pred_horizon in PREDICTION_HORIZONS:
        dataset_periods.append((candle_period, pred_horizon))

TARGET_SHIFT_PERIODS = [x[1] for x in dataset_periods]
################################################################################

if TESTING:
    date_from = '2019-01-01'
    date_to = '2019-04-01'
else:
    date_from = '2018-09-01'
    date_to = '2021-02-01'

#Pairs that don't have data starting from "date_from"
exception_pairs = []

RAW_DATA_DICTS = list()
candle_data_objects = []
good_assets = []

#OFFSET = datetime.timedelta(minutes=0)

for asset_pair in ASSET_NAMES:
    for exchange in EXCHANGE_NAME:
        for sampling_period in CANDLE_PERIODS:
            print ('ASSET PAIR:  ' + asset_pair + '     EXCHANGE:    ' + exchange + '     SAMPLING PERIOD:    ' + sampling_period)
            local_filename = 'data/original_data/' + asset_pair + '_' + sampling_period + '_' + exchange + '.csv'
            cloud_filename = 'original_data/' + asset_pair + '_' + sampling_period + '_' + exchange + '.csv'
            try:
                #df = get_bar_candles(exchange, asset_pair, date_from, date_to, CONSILIUM_API_KEY, 'bars', sampling_period)
                df = get_trade_candles(exchange=exchange, asset=asset_pair, date_from=date_from, date_to=date_to,
                                       candle_type='bars', resolution=sampling_period, path_to_creds=path_to_gs_creds,
                                       offset=None)
                #TODO: Add end date checking too! NOTE: This is not as easy as it seems
                #if((str(min(df.index.values))) == (date_from + 'T00:00:00.000000000')) and ((str(max(df.index.values))) == (date_to + 'T00:00:00.000000000')):
                if((str(min(df.index.values))) == (date_from + 'T00:00:00.000000000')):
                    good_assets.append(asset_pair)
                    candle_obj = CandleDataset(local_filename, exchange, asset_pair, df, SampleType.TIME, sampling_period, GOOGLE_STORAGE_BUCKET_NAME, col_to_label=COL_TO_LABEL, labeled_filepaths=labeled_filepaths)
                else:
                    exception_pairs.append(asset_pair)
                    continue

            except Exception as e:
                print ('EXCEPTION for   ' + asset_pair)
                print(e)
                exception_pairs.append(asset_pair)
                continue

            candle_obj.save_to_csv(local_filename)
            google_storage_obj.upload_blob_from_file(local_filename, cloud_filename)
            candle_data_objects.append(candle_obj)

#TODO: ConsiliumLogger here for any pair/exchange combinations that error out
exception_pairs = list(set(exception_pairs))
print ('EXCEPTION PAIRS:')
print (exception_pairs)



################################################################################
for target_type in TARGET_LABEL_TYPES:
    if target_type == TargetType.TERNARY_MOMENTUM:
        for threshold in TERNARY_MOMENTUM_THRESHOLDS:
            for candle_obj in candle_data_objects:
                labeled_file = candle_obj.label_dataset(target_type=target_type, neutral_perc_threshold=threshold)
                candle_obj.save_to_google_storage(google_storage_obj, target_type, labeled_file, labeled_file.split('austin-alphafactory/')[1])
                candle_obj.save_to_sql(db_manager, target_type)
    else:
        for candle_obj in candle_data_objects:

            labeled_file = candle_obj.label_dataset(target_type=target_type)
            candle_obj.save_to_google_storage(google_storage_obj, target_type, labeled_file, labeled_file.split('austin-alphafactory/')[1])
            candle_obj.save_to_sql(db_manager, target_type)
