#from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.RandomForestFeatureGenerator import RandomForestFeatureGenerator
from src.enum_classes import *
from src.GoogleStorageClient import GoogleStorageClient

from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import pandas as pd
import joblib
import numpy
import time
import os
import csv


################################################################################################################################################################
#                                                           INIT CODE
################################################################################################################################################################
dirname = os.path.dirname(__file__)

MODEL_IDS = [14995, 32300, 50131, 67722]
#LOCAL_DATA_FILE = 'data/time_bars/target_ternary_momentum/BTC_USD_gdax_1H_time_1P_target_ternary_momentum_0.005.csv' #TODO
#MODEL_FILE_DIRECTORY = 'models/'
#DEV_SET_PREDICTIONS_FILE = 'predictions/model_id_54020_dataset_id_13_model_performance_id_0_DEV.csv'

BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'

try:
    google_storage_obj = GoogleStorageClient(BUCKET_NAME_GS)
except Exception as e:
    print('Google storage exception!')
    print (e)
    send_text_msg ('5146547219', 'Code FAILED :( ')
    exit()

################################################################################
# Establish connection to database
################################################################################
with open('credentials/sql_db_creds.txt') as f:
    sql_db_creds = f.readlines()
    sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'randomforest_launcher.py', use_dict_cursor=True)

################################################################################
# Get all metadata for this model from "models table"
################################################################################

for model_id in MODEL_IDS:

    sql = "SELECT * FROM random_forest_models WHERE id='" + str(model_id) + "'"
    model_to_run = db_manager.fetch_mysql_result(sql)[0]

    model_id = model_to_run['id']
    dataset_id = model_to_run['dataset_id']
    normalization_type = model_to_run['normalization_type']
    max_depth = model_to_run['max_depth']
    max_features = model_to_run['max_features']
    max_samples = model_to_run['max_samples']
    num_estimators = model_to_run['num_estimators']
    pca_variance_perc = model_to_run['pca_variance_perc']
    test_set_perc = model_to_run['test_set_perc']
    dev_set_perc = model_to_run['dev_set_perc']
    random_seed = model_to_run['random_seed']
    split_criterion = model_to_run['split_criterion']

    ################################################################################
    # Get all relevant dataset parameters from datasets table
    ################################################################################
    sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
    dataset = db_manager.fetch_mysql_result(sql)[0]

    target_type = dataset['target_type']
    index_col_name = dataset['index_col_name']
    target_col_name = dataset['target_col_name']

    filename_gs = dataset['filename_gs']
    filename_local = 'data/' + filename_gs
    filename_local = os.path.join(dirname, filename_local)
    ################################################################################
    # Load raw data into pandas DF
    ################################################################################
    ##### LOAD FILE FROM GOOGLE STORAGE, SAVE LOCALLY #####
    try:
        #google_storage_obj = GoogleStorageClient(bucket_name_gs)
        google_storage_obj.download_blob_to_dest(filename_gs, filename_local)
    except Exception as e:
    	print('Google storage exception!')
    	print (e)
    	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
    	exit()

    raw_ohlcv_df = pd.read_csv(filename_local,header=0, parse_dates=[0], index_col=index_col_name)
    target_col_df = raw_ohlcv_df[target_col_name]

    ################################################################################
    # Generate all features from raw data for train set, calculate class imbalances
    ################################################################################
    feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc, random_seed, target_col_name)
    feature_generator.generate_all_features()
    train_X, train_y, train_index = feature_generator.get_df_dataset(DatasetSplit.TRAIN)
    test_X, test_y, test_index = feature_generator.get_df_dataset(DatasetSplit.TEST)
    train_class_weights = feature_generator.train_class_weights

    ################################################################################
    # Fit scaler and PCA to train set, transform train set
    ################################################################################
    if normalization_type == 'MIN_MAX':
        scaler = MinMaxScaler()

    elif normalization_type == 'Z_SCORE':
        scaler = StandardScaler()

    train_X = scaler.fit_transform(train_X)
    test_X = scaler.transform(test_X)


    if pca_variance_perc > 0:
        pca = PCA(pca_variance_perc)
        #pca.fit(train_X)
        train_X = pca.fit_transform(train_X)
        test_X = pca.transform(test_X)

    ################################################################################
    # Train classifier on preprocessed data
    ################################################################################
    rf_clf = RandomForestClassifier(max_features=max_features, random_state=random_seed, class_weight=train_class_weights, max_depth=max_depth, n_estimators=num_estimators, criterion=split_criterion, max_samples=max_samples, n_jobs=-1)

    start_time = time.process_time()
    rf_clf = rf_clf.fit(train_X, train_y)
    train_time_seconds = time.process_time() - start_time
    print ('Model train time: ' + str(train_time_seconds))



    ################################################################################
    # Predict test set, save to file
    ################################################################################
    y_pred_test = rf_clf.predict(test_X)


    test_filename =  'predictions/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TEST.csv'
    test_filename_local = os.path.join(dirname, test_filename)

    #NOTE: This only works if the bucket/folder structure in Google Storage is the same as local
    test_filename_gs = test_filename

    with open(test_filename_local, "w") as f:
        writer = csv.writer(f)
        writer.writerow(['index', 'y', 'y_pred'])
        for i in range(0, len(test_index)):
            writer.writerow([test_index[i], test_y[i], y_pred_test[i]])

    google_storage_obj.upload_blob_from_file(test_filename_local, test_filename_gs)
    os.remove(test_filename_local)

    sql = "UPDATE random_forest_models SET test_predictions_file_gs = '" + test_filename_gs + "' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)
