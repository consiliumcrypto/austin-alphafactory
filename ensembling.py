from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
from src.enum_classes import *

import pandas as pd
import numpy as np
import json
import hashlib
import datetime
import time
import random
import csv
import platform
import os


def strict_layered_ternary_signals_50bps (row):
    if row['ternary_classifer_50bps'] == 1:
        if row['ternary_classifer_30bps'] == 1:
            if row['ternary_classifer_15bps'] == 1:
                return 1

    elif row['ternary_classifer_50bps'] == -1:
        if row['ternary_classifer_30bps'] == -1:
            if row['ternary_classifer_15bps'] == -1:
                return -1
    return 0

def strict_layered_ternary_signals_30bps (row):
    if row['ternary_classifer_30bps'] == 1:
        if row['ternary_classifer_15bps'] == 1:
            return 1

    elif row['ternary_classifer_30bps'] == -1:
        if row['ternary_classifer_15bps'] == -1:
            return -1
    return 0


def flexible_layered_ternary_signals (row):
    ##############################################################
    # At least 2 of 3 confirmations for 50bps
    if row['ternary_classifer_50bps'] == 1:
        if (row['ternary_classifer_30bps'] == 1) or (row['ternary_classifer_15bps'] == 1):
            return 1

    elif row['ternary_classifer_50bps'] == -1:
        if (row['ternary_classifer_30bps'] == -1) or (row['ternary_classifer_15bps'] == -1):
            return -1

    return 0


def binary_ternary_15bps (row):
    if row['ternary_classifer_15bps'] == 1:
        if row['binary_classifier'] == 1:
            return 1

    elif row['ternary_classifer_15bps'] == -1:
        if row['binary_classifier'] == -1:
            return -1

    return 0


def binary_ternary_30bps (row):
    if row['ternary_classifer_30bps'] == 1:
        if row['binary_classifier'] == 1:
            return 1

    elif row['ternary_classifer_30bps'] == -1:
        if row['binary_classifier'] == -1:
            return -1

    return 0

def binary_ternary_50bps (row):
    if row['ternary_classifer_50bps'] == 1:
        if row['binary_classifier'] == 1:
            return 1

    elif row['ternary_classifer_50bps'] == -1:
        if row['binary_classifier'] == -1:
            return -1

    return 0

################################################################################
dirname = os.path.dirname(__file__)

# Establish connection to database
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'randomforest_launcher.py', use_dict_cursor=True )


BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'

try:
    google_storage_obj = GoogleStorageClient(BUCKET_NAME_GS)
except Exception as e:
    print('Google storage exception!')
    print (e)
    send_text_msg ('5146547219', 'Code FAILED :( ')
    exit()



model_ids = dict()

model_ids['binary_classifier'] = 14995
model_ids['ternary_classifer'] = dict()
model_ids['ternary_classifer']['15bps'] = 32300
model_ids['ternary_classifer']['30bps'] = 50131
model_ids['ternary_classifer']['50bps'] = 67722

master_df = pd.DataFrame()



################################################################################

SPLIT = 'test'

for k,v in model_ids.items():
    #Ternary
    if isinstance(v,dict):
        clf_category = k
        for threshold, model_id in v.items():
            sql = "SELECT * FROM random_forest_models WHERE id='" + str(model_id) + "'"
            model_dict = db_manager.fetch_mysql_result(sql)[0]

            predictions_file_gs = model_dict[SPLIT + '_predictions_file_gs']
            predictions_file_local = os.path.join(dirname, predictions_file_gs)

            try:
                #google_storage_obj = GoogleStorageClient(bucket_name_gs)
                google_storage_obj.download_blob_to_dest(predictions_file_gs, predictions_file_local)
            except Exception as e:
                print('Google storage exception!')
                print (e)
                send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id))
                exit()

            y_ypred_df = pd.read_csv(predictions_file_local, header=0, index_col='index')

            master_df[clf_category + '_' + threshold] = y_ypred_df['y_pred']

    #Binary models
    else:
        sql = "SELECT * FROM random_forest_models WHERE id='" + str(v) + "'"
        model_dict = db_manager.fetch_mysql_result(sql)[0]
        print (model_dict)

        predictions_file_gs = model_dict[SPLIT + '_predictions_file_gs']

        print ('STUFF')
        print (predictions_file_gs)
        predictions_file_local = os.path.join(dirname, predictions_file_gs)

        try:
            #google_storage_obj = GoogleStorageClient(bucket_name_gs)
            google_storage_obj.download_blob_to_dest(predictions_file_gs, predictions_file_local)
        except Exception as e:
            print('Google storage exception!')
            print (e)
            send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id))
            exit()

        y_ypred_df = pd.read_csv(predictions_file_local, header=0, index_col='index')

        master_df[k] = y_ypred_df['y_pred']





master_df['strict_layered_ternary_signals_50bps'] = master_df.apply(strict_layered_ternary_signals_50bps, axis=1)
master_df['strict_layered_ternary_signals_30bps'] =  master_df.apply(strict_layered_ternary_signals_30bps, axis=1)
master_df['flexible_layered_ternary_signals'] =  master_df.apply(flexible_layered_ternary_signals, axis=1)
master_df['binary_ternary_15bps'] =  master_df.apply(binary_ternary_15bps, axis=1)
master_df['binary_ternary_30bps'] = master_df.apply(binary_ternary_30bps, axis=1)
master_df['binary_ternary_50bps'] = master_df.apply(binary_ternary_50bps, axis=1)
master_df['pure_ternary_50bps'] = master_df['ternary_classifer_50bps']
master_df['pure_ternary_30bps'] = master_df['ternary_classifer_30bps']
master_df['pure_ternary_15bps'] = master_df['ternary_classifer_15bps']


print (master_df.head(20))
master_df.drop(['binary_classifier', 'ternary_classifer_15bps', 'ternary_classifer_30bps', 'ternary_classifer_50bps'], axis=1, inplace=True)
print (master_df.head(20))
master_df.to_csv('results/ensembles/master_may2_2020_6H.csv')

print ('####################################################')
print ('TOTAL length:')
print (len(master_df))

print ('####################################################')
print ('Num signals for 50 BPS STRICT ternary layered signals:')
print (len(master_df[master_df['strict_layered_ternary_signals_50bps'] != 0]))
print (len(master_df[master_df['strict_layered_ternary_signals_50bps'] == 1]))
print (len(master_df[master_df['strict_layered_ternary_signals_50bps'] == -1]))
print (len(master_df[master_df['strict_layered_ternary_signals_50bps'] == 0]))

nums = master_df['strict_layered_ternary_signals_50bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass


print ('####################################################')
print ('Num signals for 30 BPS STRICT ternary layered signals:')
print (len(master_df[master_df['strict_layered_ternary_signals_30bps'] != 0]))
print (len(master_df[master_df['strict_layered_ternary_signals_30bps'] == 1]))
print (len(master_df[master_df['strict_layered_ternary_signals_30bps'] == -1]))
print (len(master_df[master_df['strict_layered_ternary_signals_30bps'] == 0]))

nums = master_df['strict_layered_ternary_signals_30bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass


print ('####################################################')
print ('Num signals for FLEXIBLE ternary layered signals:')
print (len(master_df[master_df['flexible_layered_ternary_signals'] != 0]))
print (len(master_df[master_df['flexible_layered_ternary_signals'] == 1]))
print (len(master_df[master_df['flexible_layered_ternary_signals'] == -1]))
print (len(master_df[master_df['flexible_layered_ternary_signals'] == 0]))

nums = master_df['flexible_layered_ternary_signals'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass

print ('####################################################')
print ('15 BPS Binary-Ternary num signals:')
print (len(master_df[master_df['binary_ternary_15bps'] != 0]))
print (len(master_df[master_df['binary_ternary_15bps'] == 1]))
print (len(master_df[master_df['binary_ternary_15bps'] == -1]))
print (len(master_df[master_df['binary_ternary_15bps'] == 0]))

nums = master_df['binary_ternary_15bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass

print ('####################################################')
print ('30 BPS Binary-Ternary num signals:')
print (len(master_df[master_df['binary_ternary_30bps'] != 0]))
print (len(master_df[master_df['binary_ternary_30bps'] == 1]))
print (len(master_df[master_df['binary_ternary_30bps'] == -1]))
print (len(master_df[master_df['binary_ternary_30bps'] == 0]))

nums = master_df['binary_ternary_30bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass

print ('####################################################')
print ('50 BPS Binary-Ternary num signals:')
print (len(master_df[master_df['binary_ternary_50bps'] != 0]))
print (len(master_df[master_df['binary_ternary_50bps'] == 1]))
print (len(master_df[master_df['binary_ternary_50bps'] == -1]))
print (len(master_df[master_df['binary_ternary_50bps'] == 0]))

nums = master_df['binary_ternary_50bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass
print ('####################################################')
print ('15 BPS Pure Ternary num signals:')
print (len(master_df[master_df['pure_ternary_15bps'] != 0]))
print (len(master_df[master_df['pure_ternary_15bps'] == 1]))
print (len(master_df[master_df['pure_ternary_15bps'] == -1]))
print (len(master_df[master_df['pure_ternary_15bps'] == 0]))

nums = master_df['pure_ternary_15bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass

print ('####################################################')
print ('30 BPS Pure Ternary num signals:')
print (len(master_df[master_df['pure_ternary_30bps'] != 0]))
print (len(master_df[master_df['pure_ternary_30bps'] == 1]))
print (len(master_df[master_df['pure_ternary_30bps'] == -1]))
print (len(master_df[master_df['pure_ternary_30bps'] == 0]))

nums = master_df['pure_ternary_30bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass

print ('####################################################')
print ('50 BPS Pure Ternary num signals:')
print (len(master_df[master_df['pure_ternary_50bps'] != 0]))
print (len(master_df[master_df['pure_ternary_50bps'] == 1]))
print (len(master_df[master_df['pure_ternary_50bps'] == -1]))
print (len(master_df[master_df['pure_ternary_50bps'] == 0]))

nums = master_df['pure_ternary_50bps'].values
run_starts = np.where(np.diff(nums))[0]
runs = np.diff(np.hstack((0, run_starts + 1, nums.size)))
idxes = np.hstack((run_starts, run_starts[-1]+1))
for label in (-1, 0, 1):
    try:
        m = runs[np.where(nums[idxes] == label)[0]].max()
        print(f'{label}: {m}')
    except:
        print ('No runs of label:   ' + str(label))
        pass



print ('DONE DONE DONE')
