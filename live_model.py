from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *

from keras.callbacks import EarlyStopping
from keras.utils.np_utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import LSTM
from keras.models import load_model

import tensorflow as tf

from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MinMaxScaler

import pandas as pd
import numpy
import json
import hashlib
import datetime
import time
import random
import csv
import platform

import os
dirname = os.path.dirname(__file__)


def model_predict (raw_ohlcv_df, model_dict):

	num_steps = model_dict['num_steps']
	model_filename = model_dict['model_filename']
	batch_size = model_dict['batch_size']
	#######################################################

	################################################################################
	# Data config variables
	################################################################################
	#TODO: Replace with Fadi's DF
	#raw_ohlcv_df = pd.read_csv(filename_local,header=0, parse_dates=[0], index_col=index_col_name)
	ohlcv_columns = list(raw_ohlcv_df.columns.values)

	#################### TA Features #####################
	features_TA_df = compute_basic_technicals(raw_ohlcv_df.copy())

	#Drop NaNs
	features_TA_df = features_TA_df.dropna()

	#####################   NEW FEATURES  TRAIN, DEV, TEST SETS #############################

	#TODO: Split into train, dev, test before creating new features
	features_new_df = compute_new_features(raw_ohlcv_df.copy())

	features_new_df = features_new_df.dropna()

	################################################################################

	print ('Done creating TA features')

	############## CREATE MOVING AVERAGES AND DIFFS OF ALL FEATURES ################


	TA_cols_to_merge = features_TA_df.columns.difference(features_new_df.columns)

	all_features_df = pd.merge(features_TA_df[TA_cols_to_merge], features_new_df, on=['time_period_start'])


	#TODO: Remove this once the issue is solved at the root (e.g. API call for extended candles)
	if 'time_period_start.1' in all_features_df.columns.values:
		all_features_df.drop('time_period_start.1', inplace=True, axis=1)


	print (all_features_df.head())
	for i in all_features_df.columns.values:
		print(i)


	#Compute MAs and diffs of all features together
	all_features_ma_diffs_df = compute_ma_diffs(all_features_df)

	all_features_ma_diffs_df = all_features_ma_diffs_df.dropna()


	####################################################################

	print ('Done creating diffs and MAs')


	############################################################################
	#remove_cols = ohlcv_columns + [target_col_name]
	#remove_cols = [target_col_name]
	#all_features_ma_diffs_df = all_features_ma_diffs_df.drop(remove_cols, axis=1)



	############################################################################
	num_features = len(all_features_ma_diffs_df.columns.values)

	X = all_features_ma_diffs_df.copy()


	#Loop through each feature column, creating lagged features
	for f in all_features_ma_diffs_df.columns.values:
		#if f not in remove_cols:
		for step in range(0, num_steps):
			X[f + '_lag_' + str(step)] = X[f].shift(step)

		X.drop(f, inplace=True, axis=1)


	print ('HEAD ')
	print (X.head())
	print ('TAIL')
	print (X.tail())

	X = X.values

	# reshape input to be 3D [samples, timesteps, features]
	X = X.reshape((X.shape[0], num_steps, num_features))

	scaler = MinMaxNormalizer()
	X = scaler.normalize(tensor=X, axis=(1, ))

	#Drop NaNs
	X = X[(num_steps-1):, :]



	#TODO:  Confirm with Vlad/Andrei if batch_size matters when making predictions

	num_rows_to_drop = (len(X)) % batch_size
	print ('Train rows to drop:' + str(num_rows_to_drop))
	print (X.shape)
	print ('BEFORE --> START')
	print (X[0])
	print ('BEFORE --> END')
	print (X[-1])
	if num_rows_to_drop > 0:
		X = X[(num_rows_to_drop):, :]

	print (X.shape)
	print ('AFTER --> START')
	print (X[0])
	print ('AFTER --> END')
	print (X[-1])



	print ('###############################################################')
	print ('Done creating lags and dropping NaNs')

	model = load_model(model_filename)

	#TODO: Confirm about batch_size parameter
	#y_pred = model.predict(X, verbose=2, batch_size=batch_size)
	y_pred = model.predict(X, verbose=2)
	y_pred = [x[0] for x in y_pred]

	return y_pred




df = pd.read_csv('data/live_sample_btcusd_5T.csv', header=0, parse_dates=[0], index_col='time_period_start')

y_pred = model_predict(df, model_config)

print (y_pred)
print (len(y_pred))
