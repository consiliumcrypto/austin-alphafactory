from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
from src.enum_classes import *

import pandas as pd
import numpy as np
import json
import hashlib
import datetime
import time
import random
import csv
import platform
import os

################################################################################
dirname = os.path.dirname(__file__)

# Establish connection to database
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'randomforest_launcher.py', use_dict_cursor=True )


BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'

try:
    google_storage_obj = GoogleStorageClient(BUCKET_NAME_GS)
except Exception as e:
    print('Google storage exception!')
    print (e)
    send_text_msg ('5146547219', 'Code FAILED :( ')
    exit()



model_ids = [5303, 5684, 5202, 5631]
master_df = pd.DataFrame()

y_master_df = pd.DataFrame()


for model_id in model_ids:
    sql = "SELECT * FROM random_forest_models WHERE id='" + str(model_id) + "'"
    model_dict = db_manager.fetch_mysql_result(sql)[0]

    dev_predictions_file_gs = model_dict['dev_predictions_file_gs']
    dev_predictions_file_local = os.path.join(dirname, dev_predictions_file_gs)

    try:
        #google_storage_obj = GoogleStorageClient(bucket_name_gs)
        google_storage_obj.download_blob_to_dest(dev_predictions_file_gs, dev_predictions_file_local)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id))
        exit()

    y_ypred_df = pd.read_csv(dev_predictions_file_local, header=0, index_col='index')

    y_master_df['model_id_' + str(model_id)] = y_ypred_df['y']
    master_df['model_id_' + str(model_id)] = y_ypred_df['y_pred']



print (master_df.head(50))
consistent_rows = y_master_df.eq(y_master_df.iloc[:, 0], axis=0).all(1)

#Find all rows where all values are the same (because each individual model should have been trained to predict the same output)
# all rows shold be the same to make sure the stacked models were all trained on the same output
num_consistent_rows = len(consistent_rows[consistent_rows==True])

if len(y_master_df) != num_consistent_rows:
    print('These models were not all trained to predict the same output! Exiting now.')
    exit()

print ('DONE')

print (master_df.head())

exit()


################################################################################
