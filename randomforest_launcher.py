from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
from src.RandomForestFeatureGenerator import RandomForestFeatureGenerator
from src.enum_classes import *
from src.RFOptimizer import RFOptimizer

from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import pandas as pd
import joblib
import numpy
import time
import random
import platform
import os
import traceback
import wandb
from wandb.sklearn import plot_feature_importances, plot_learning_curve, plot_confusion_matrix, plot_summary_metrics, plot_class_proportions, plot_roc, plot_precision_recall

dirname = os.path.dirname(__file__)
################################################################################
# Establish connection to database
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

db_kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (db_kwargs, 'randomforest_launcher.py', use_dict_cursor=True )



while True:

    ################################################################################
    # Get all "WAITING" models (models that need to be run)
    sql = "SELECT * FROM random_forest_models WHERE model_status='WAITING'"
    waiting_models = db_manager.fetch_mysql_result(sql)

    if len(waiting_models) == 0:
    	#send_text_msg ('5146547219', 'No more models to run, shutting down server!')
    	time.sleep(300)
    	shutdown_server()
    	exit()

    model_to_run = random.choice(waiting_models)
    model_id = model_to_run['id']
    print ('###################################################')
    print ('MODEL ID:   ' + str(model_id))
    print ('###################################################')
    sql = "UPDATE random_forest_models SET model_status = 'RUNNING' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)

    ################################################################################
    #Get all relevant model parameters
    dataset_id = model_to_run['dataset_id']
    bucket_name_gs = model_to_run['bucket_name_gs']
    normalization_type = model_to_run['normalization_type']
    max_depth = model_to_run['max_depth']
    max_features = model_to_run['max_features']
    max_samples = model_to_run['max_samples']
    num_estimators = model_to_run['num_estimators']
    pca_variance_perc = model_to_run['pca_variance_perc']
    test_set_perc = model_to_run['test_set_perc']
    dev_set_perc = model_to_run['dev_set_perc']
    random_seed = model_to_run['random_seed']
    split_criterion = model_to_run['split_criterion']
    num_optuna_model_trials = model_to_run['num_optuna_model_trials']
    optuna_model_objective = model_to_run['optuna_model_objective']

    if optuna_model_objective == OptunaModelMetric.ROC_AUC.value:
        optuna_model_objective = OptunaModelMetric.ROC_AUC
    elif optuna_model_objective == OptunaModelMetric.BALANCED_ACCURACY.value:
        optuna_model_objective = OptunaModelMetric.BALANCED_ACCURACY

    ################################################################################
    #Initialize google storage bucket object
    try:
        google_storage_obj = GoogleStorageClient(bucket_name_gs)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        print(str(traceback.format_exc()))
        send_text_msg ('5146547219', 'Code FAILED :( ')
        exit()

    ################################################################################
    #Get all relevant model parameters
    sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
    dataset = db_manager.fetch_mysql_result(sql)[0]

    target_type = dataset['target_type']
    index_col_name = dataset['index_col_name']
    target_col_name = dataset['target_col_name']
    filename_gs = dataset['filename_gs']
    filename_local = os.path.join(dirname, filename_gs)

    print ('TARGET TYPE:      ' + str(target_type))
    ################################################################################
    ##### LOAD FILE FROM GOOGLE STORAGE, SAVE LOCALLY #####
    try:
        #google_storage_obj = GoogleStorageClient(bucket_name_gs)print(str(traceback.format_exc()))
        google_storage_obj.download_blob_to_dest(filename_gs, filename_local)
    except Exception as e:
        print(str(traceback.format_exc()))
        print('Google storage exception!')
        print (e)
        sql = "UPDATE random_forest_models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
        db_manager.update_database(sql)
        send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
        exit()

    ################################################################################
    # Set WANDB config
    config_dict = model_to_run
    config_dict['sampling_period'] = dataset['sampling_period']
    config_dict['asset_pair'] = dataset['asset_pair']
    config_dict['neutral_perc_threshold'] = dataset['neutral_perc_threshold']

    wandb.init(
        project=db_kwargs['database'],
        config=config_dict,
        reinit=True,
    )

    ################################################################################
    # Data config variables
    numpy.random.seed(random_seed)
    raw_ohlcv_df = pd.read_csv(filename_local,header=0, parse_dates=[0], index_col=index_col_name)
    os.remove(filename_local)
    ohlcv_columns = list(raw_ohlcv_df.columns.values)
    target_col_df = raw_ohlcv_df[target_col_name]
    ################################################################################
    # Generate all features from raw data, save processed files to google storage
    feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc, random_seed, target_col_name)
    best_ta_params = feature_generator.generate_all_features()

    feature_generator.save_processed_inputs(model_id, dataset_id, dirname, google_storage_obj)

    train_X, train_y, train_index = feature_generator.get_df_dataset(DatasetSplit.TRAIN)
    dev_X, dev_y, dev_index = feature_generator.get_df_dataset(DatasetSplit.DEV)
    train_class_weights = feature_generator.train_class_weights


    #Drop first 50 rows to reduce indicator and rolling calc "warmup" period included in dataset
    #TODO: Find a better way to calculate the number of rows to drop (e.g. how to choose "50")
    train_X = train_X[50:]
    train_y = train_y[50:]
    train_index = train_index[50:]

    dev_X = dev_X[50:]
    dev_y = dev_y[50:]
    dev_index = dev_index[50:]

    feature_names = train_X.columns.values

    #Normalization
    if normalization_type == 'MIN_MAX':
        scaler = MinMaxScaler()
        train_X = scaler.fit_transform(train_X)
        dev_X = scaler.transform(dev_X)
    elif normalization_type == 'Z_SCORE':
        scaler = StandardScaler()
        train_X = scaler.fit_transform(train_X)
        dev_X = scaler.transform(dev_X)

    #PCA
    if pca_variance_perc > 0:
        pca = PCA(n_components=pca_variance_perc, svd_solver='full', random_state=random_seed)
        train_X = pca.fit_transform(train_X)
        dev_X = pca.transform(dev_X)
        pca_params_dict = pca.get_params()
        feature_names = []
    else:
        pca_params_dict = {}

    try:
        rf_optimizer = RFOptimizer(train_X, train_y, dev_X, dev_y, train_class_weights, random_seed,
                                   optuna_model_objective)
        rf_optimizer.tune_rf(num_trials=num_optuna_model_trials)
        best_params = rf_optimizer.optimal_params

    except Exception as e:
        send_text_msg('5146547219', 'Code FAILED @ RFOptimizer :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
        print(e)
        sql = "UPDATE random_forest_models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
        db_manager.update_database(sql)
        exit()


    max_features = best_params['rf_max_features']
    #random_seed = best_params['rf_random_seed']
    max_depth = best_params['rf_max_depth']
    max_samples = best_params['rf_max_samples']
    num_estimators = best_params['rf_num_estimators']
    split_criterion = best_params['rf_split_criterion']

    #NOTE: Use "max_features=None" when using trees for feature importance only
    rf_clf = RandomForestClassifier(max_features=max_features, random_state=random_seed, class_weight=train_class_weights, max_depth=max_depth, n_estimators=num_estimators, criterion=split_criterion, max_samples=max_samples, n_jobs=-1)

    start_time = time.process_time()
    rf_clf = rf_clf.fit(train_X, train_y)
    train_time_seconds = time.process_time() - start_time

    sql = "UPDATE random_forest_models SET train_time_seconds = '" + str(train_time_seconds) + "', max_features='" + str(max_features) + "', max_depth='" + str(max_depth) + "', max_samples='" + str(max_samples) + "', num_estimators='" + str(num_estimators) + "', optuna_ta_dict=\"" + str(best_ta_params) + "\", pca_params_dict=\"" + str(pca_params_dict) +"\", split_criterion='" + str(split_criterion) + "' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)

    y_pred_train = rf_clf.predict(train_X)
    y_pred_dev = rf_clf.predict(dev_X)
    y_pred_prob_train = rf_clf.predict_proba(train_X)
    y_pred_prob_dev = rf_clf.predict_proba(dev_X)

    ################################################################################
    # WANDB sklearn plots
    if pca_variance_perc > 0:
        plot_feature_importances(rf_clf)
    else:
        plot_feature_importances(rf_clf, list(feature_names))

    plot_learning_curve(rf_clf, train_X, train_y)
    plot_confusion_matrix(dev_y, y_pred_dev)
    plot_summary_metrics(rf_clf, X=train_X, y=train_y, X_test=dev_X, y_test=dev_y)
    plot_class_proportions(train_y, dev_y)
    plot_roc(dev_y, y_pred_prob_dev)
    plot_precision_recall(dev_y, y_pred_prob_dev)
    ################################################################################

    splits = {'TRAIN': [train_index, train_y, y_pred_train, y_pred_prob_train], 'DEV': [dev_index, dev_y, y_pred_dev, y_pred_prob_dev]}

    for split, datasets in splits.items():
        up_accuracy = 0.0
        down_accuracy = 0.0
        neutral_accuracy = 0.0
        f1_weighted = 0.0
        f1_up = 0.0
        f1_down = 0.0
        f1_neutral = 0.0

        index = datasets[0]
        y = datasets[1]
        y_pred = datasets[2]
        y_pred_prob = datasets[3]

        d = {'y':y, 'y_pred':y_pred}

        num_classes = len(y_pred_prob[0])

        if num_classes == 2:
            classes = [-1, 1]
        elif num_classes == 3:
            classes = [-1,0,1]

        for i in range(0,num_classes):
            d['y_pred_prob_'+str(classes[i])] = [x[i] for x in y_pred_prob]


        y_ypred_ypredprob_df = pd.DataFrame(d, index=index)
        y_ypred_ypredprob_df.to_csv('data/'+split+'-y_ypred_ypredprob.csv')

        # NOTE: The keys of this dict must match the column names in the SQL table
        performance_metrics_dict = calc_classifier_performance(y_ypred_ypredprob_df)

        wandb_metrics = dict()

        for k,v in performance_metrics_dict.items():
            wandb_metrics[split+'/'+k] = v

        wandb.log(wandb_metrics)

        sql = "INSERT INTO classifier_performance (model_id,dataset_split,"

        for k in performance_metrics_dict.keys():
            sql += str(k) + ","

        sql = sql[:-1] #Remove "," comma from last entry

        sql += ") VALUES ("
        sql += str(model_id) + ",'" + split + "',"

        for v in performance_metrics_dict.values():
            sql += str(v) + ","

        sql = sql[:-1] #Remove "," comma from last entry
        sql += ")"

        res = db_manager.update_database(sql)

        sql = "SELECT LAST_INSERT_ID();"
        performance_id = db_manager.fetch_mysql_result(sql)[0]['LAST_INSERT_ID()']

        pred_filename =  'predictions/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_model_performance_id_' + str(performance_id) + '_' + split + '.csv'
        pred_filename_local = os.path.join(dirname, pred_filename)

        #NOTE: This only works if the bucket/folder structure in Google Storage is the same as local
        pred_filename_gs = pred_filename

        y_ypred_ypredprob_df.to_csv(pred_filename_local)

        google_storage_obj.upload_blob_from_file(pred_filename_local, pred_filename_gs)
        os.remove(pred_filename_local)

        sql = "UPDATE random_forest_models SET " + split.lower() +  "_predictions_file_gs = '" + pred_filename_gs + "' WHERE id = '" + str(model_id) + "'"
        db_manager.update_database(sql)


    sql = "UPDATE random_forest_models SET performance_measured = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)

    ################################################################################
    #Save model to file and upload to google storage
    model_filename = 'models/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '.joblib'
    local_model_filename = os.path.join (dirname, model_filename)
    joblib.dump(rf_clf, local_model_filename)

    try:
        cloud_model_filename = model_filename
        google_storage_obj.upload_blob_from_file(local_model_filename, cloud_model_filename)
        sql = "UPDATE random_forest_models SET model_file_gs = '" + cloud_model_filename + "' WHERE id = '" + str(model_id) + "'"
        db_manager.update_database(sql)
        os.remove(local_model_filename) #NOTE:Doesn't this remove the model file before uploading to GS?

    except Exception as e:
    	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
    	print(e)
    	sql = "UPDATE random_forest_models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
    	db_manager.update_database(sql)
    	exit()

    ################################################################################
    #Save model to file and upload to google storage
    sql = "UPDATE random_forest_models SET model_status = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)

    try:
    	print ('Skipping twilio for TESTING purposes')
    	#send_text_msg ('5146547219', 'Code is done :)  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
    except Exception as e:
        print ('Twilio ERROR')
        print (e)