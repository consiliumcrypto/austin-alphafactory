import pandas as pd
from tqdm import tqdm
from datetime import timedelta
from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.GoogleStorageClient import *


"""
1) Find all longs / shorts that are greater than THRESHOLD
2) Find all longs / shorts that are tradable (e.g. no consecutive longs or shorts)
"""
#TODO: add shorts




kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
      'database': 'final_model_results',
      'ssl': {'ca': '/Users/austin/Documents/consilium/db_creds/server-ca.pem',
              'key': '/Users/austin/Documents/consilium/db_creds/client-key.pem',
              'cert': '/Users/austin/Documents/consilium/db_creds/client-cert.pem', 'check_hostname': False},
      'port': 3306}

db_manager=DatabaseManager (kwargs, 'test', use_dict_cursor=True )

#sql = "SELECT * FROM models WHERE model_status='COMPLETE'"

#while True:

sql = "SELECT * FROM models WHERE (model_status='COMPLETE' AND performance_measured='COMPLETE')"

finished_models = db_manager.fetch_mysql_result(sql)


if len(finished_models) == 0:
    print('No eligible models for strategy in database')
    send_text_msg ('5146547219', 'Done scoring all models :)')
    time.sleep(120)
    shutdown_server()
    exit()
else:
    model_dict = random.choice(finished_models)

model_id = model_dict['id']
train_predictions_filename = model_dict['train_predictions_file_gs']
dev_predictions_filename = model_dict['dev_predictions_file_gs']
prediction_filenames = [train_predictions_filename, dev_predictions_filename]
dataset_id = model_dict['dataset_id']
bucket_name_gs = model_dict['bucket_name_gs']

sql = "SELECT * FROM datasets WHERE id =" + str(dataset_id)

dataset_dict = db_manager.fetch_mysql_result(sql)[0]
print (dataset_dict)
sample_period = dataset_dict['sampling_period']

print (sample_period)
print (sample_period[-1])
print (sample_period[:-1])


print ('Train filename  ' + train_predictions_filename)
print ('Dev filename  ' + dev_predictions_filename)


google_storage_obj = GoogleStorageClient(bucket_name_gs)
google_storage_obj.download_blob_to_dest(train_predictions_filename, train_predictions_filename)
google_storage_obj.download_blob_to_dest(dev_predictions_filename, dev_predictions_filename)

'''
sql = "UPDATE models SET performance_measured = 'RUNNING' WHERE id = '" + str(model_id) + "'"
db_manager.update_database(sql)
'''

SCALING_FACTOR = 15  # all the predictions will be scaled by ths constant (for plotting only)
THRESHOLD = 0.002  # desired profitability threshold

# data granularity
if sample_period[-1] == 'T':
    DELTA = timedelta(minutes=int(sample_period[:-1]))

elif sample_period[-1] == 'H':
    DELTA = timedelta(hours=int(sample_period[:-1]))

else:
    print ('Unrecognized sample period in strategy!')
    exit()

for pred_filename in prediction_filenames:

    # Read in the csv with the outputs of the model
    #filename = 'predictions_model_id_11_dataset_id_1_model_performance_id_137_TRAIN.csv'
    data = pd.read_csv(pred_filename, index_col=0, parse_dates=True, header=None, names=['datetime', 'perc_return_prediction'])

    # Scale the prediction by a constant factor
    data['perc_return_prediction'] *= SCALING_FACTOR

    # Generate long only signals
    all_longs = data[data['perc_return_prediction'] >= THRESHOLD]
    print(all_longs.head())
    exit()

    # Filter the signals such that we don't take consecutive positions
    position_time = None  # tracks the time the last position was taken
    idx, values = list(), list()
    for ix, row in tqdm(all_longs.iterrows()):
        if position_time is None or ix - position_time > DELTA * 2:
            position_time = ix
            idx.append(ix)
            values.append(row[0])
            continue
        elif ix - position_time < DELTA * 2:
            continue

    signals = pd.DataFrame(values, columns=['long_signal'], index=idx)
    print(signals)

    signals_filename = 'signals/' + pred_filename.split('predictions/')[1].split('.csv')[0] + '_SIGNALS.csv'

    signals.to_csv(signals_filename)
