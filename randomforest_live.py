#from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.RandomForestFeatureGenerator import RandomForestFeatureGenerator
from src.enum_classes import *

from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import pandas as pd
import joblib
import numpy
import time
import os
import csv


################################################################################################################################################################
#                                                           INIT CODE
################################################################################################################################################################
dirname = os.path.dirname(__file__)

MODEL_ID = 54020
LOCAL_DATA_FILE = 'data/time_bars/target_ternary_momentum/BTC_USD_gdax_1H_time_1P_target_ternary_momentum_0.005.csv' #TODO
MODEL_FILE_DIRECTORY = 'models/'
DEV_SET_PREDICTIONS_FILE = 'predictions/model_id_54020_dataset_id_13_model_performance_id_0_DEV.csv'
################################################################################
# Establish connection to database
################################################################################
with open('credentials/sql_db_creds.txt') as f:
    sql_db_creds = f.readlines()
    sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'randomforest_launcher.py', use_dict_cursor=True)

################################################################################
# Get all metadata for this model from "models table"
################################################################################
sql = "SELECT * FROM random_forest_models WHERE id='" + str(MODEL_ID) + "'"
model_to_run = db_manager.fetch_mysql_result(sql)[0]

model_id = model_to_run['id']
dataset_id = model_to_run['dataset_id']
normalization_type = model_to_run['normalization_type']
max_depth = model_to_run['max_depth']
max_features = model_to_run['max_features']
max_samples = model_to_run['max_samples']
num_estimators = model_to_run['num_estimators']
pca_variance_perc = model_to_run['pca_variance_perc']
test_set_perc = model_to_run['test_set_perc']
dev_set_perc = model_to_run['dev_set_perc']
random_seed = model_to_run['random_seed']
split_criterion = model_to_run['split_criterion']

################################################################################
# Get all relevant dataset parameters from datasets table
################################################################################
sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
dataset = db_manager.fetch_mysql_result(sql)[0]

target_type = dataset['target_type']
index_col_name = dataset['index_col_name']
target_col_name = dataset['target_col_name']


################################################################################
# Load raw data into pandas DF
################################################################################
numpy.random.seed(random_seed)
raw_ohlcv_df = pd.read_csv(LOCAL_DATA_FILE,header=0, parse_dates=[0], index_col=index_col_name)
target_col_df = raw_ohlcv_df[target_col_name]

################################################################################
# Generate all features from raw data for train set, calculate class imbalances
################################################################################
feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc, random_seed, target_col_name)
feature_generator.generate_all_features()
train_X, train_y, train_index = feature_generator.get_df_dataset(DatasetSplit.TRAIN)
dev_X, dev_y, dev_index = feature_generator.get_df_dataset(DatasetSplit.DEV)
train_class_weights = feature_generator.train_class_weights
dev_X.to_csv('GDAX_Dev_Features_notransforms.csv')
################################################################################
# Fit scaler and PCA to train set, transform train set
################################################################################
if normalization_type == 'MIN_MAX':
    scaler = MinMaxScaler()

elif normalization_type == 'Z_SCORE':
    scaler = StandardScaler()

train_X = scaler.fit_transform(train_X)
dev_X = scaler.transform(dev_X)


if pca_variance_perc > 0:
    pca = PCA(pca_variance_perc)
    pca.fit(train_X)
    train_X = pca.transform(train_X)
    dev_X = pca.transform(dev_X)

################################################################################
# Train classifier on preprocessed data
################################################################################
rf_clf = RandomForestClassifier(max_features=max_features, random_state=random_seed, class_weight=train_class_weights, max_depth=max_depth, n_estimators=num_estimators, criterion=split_criterion, max_samples=max_samples, n_jobs=-1)

start_time = time.process_time()
rf_clf = rf_clf.fit(train_X, train_y)
train_time_seconds = time.process_time() - start_time
print ('Model train time: ' + str(train_time_seconds))



################################################################################
# Predict dev set, save to file
################################################################################


y_pred_dev = rf_clf.predict(dev_X)
print (y_pred_dev)
print (len(y_pred_dev))

with open(DEV_SET_PREDICTIONS_FILE, newline='') as f:
    reader = csv.reader(f)
    data = list(reader)


del data[0] #get rid of header column
data = [float(x[2]) for x in data] #Only select predictions, cast strings to floats


if list(y_pred_dev) == list(data):
    print ('ALL GOOD')
else:
    print ('Something is broken, exiting!')
    exit()


################################################################################
#Save trained model to file
################################################################################
model_filename = MODEL_FILE_DIRECTORY + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '.joblib'
local_model_filename = os.path.join (dirname, model_filename)
joblib.dump(rf_clf, local_model_filename)


################################################################################################################################################################
#                                                           LIVE PREDICTION CODE
################################################################################################################################################################
raw_ohlcv_df = pd.read_csv('data/austin-df.csv', header=0, parse_dates=[0], index_col=index_col_name)

#raw_ohlcv_df = pd.read_csv('data/original_data/binance-extended-debug.csv', header=0, parse_dates=[0], index_col=index_col_name)
################################################################################
# Generate all features from raw data for train set, calculate class imbalances
################################################################################
#Use entire input data to generate features, no train/dev/test when running live
dev_set_perc = 0
test_set_perc = 0

feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc, random_seed, target_col_name)
feature_generator.generate_all_features()
input_X, _, _ = feature_generator.get_df_dataset(DatasetSplit.TRAIN)

input_X.to_csv('live_binance_features_matrix_notransforms.csv')

################################################################################
# Transform input data using pre-fitted scaler and PCA
################################################################################
input_X = scaler.transform(input_X)

if pca_variance_perc > 0:
    input_X = pca.transform(input_X)

y_pred = rf_clf.predict(input_X)
