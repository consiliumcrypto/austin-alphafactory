from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
from src.FeatureGenerator import FeatureGenerator
from src.enum_classes import *

from keras.callbacks import EarlyStopping
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM, CuDNNLSTM, Bidirectional
from keras.models import load_model

import tensorflow as tf
import pandas as pd
import numpy
import json
import hashlib
import datetime
import time
import random
import csv
import platform
import os


dirname = os.path.dirname(__file__)

################################################################################
# Establish connection to database
DATABASE_NAME = 'austin_march26_2020'

with open(os.path.join(dirname, 'credentials/sql_db_creds.txt')) as f:
    sql_db_creds = f.readlines()
    sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': DATABASE_NAME,
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'lstm_launcher.py', use_dict_cursor=True )

################################################################################
# Get all "WAITING" models (models that need to be run)
sql = "SELECT * FROM models WHERE model_status='WAITING'"
waiting_models = db_manager.fetch_mysql_result(sql)

if len(waiting_models) == 0:
	send_text_msg ('5146547219', 'No more models to run, shutting down server!')
	time.sleep(300)
	shutdown_server()
	exit()

model_to_run = random.choice(waiting_models)
model_id = model_to_run['id']

sql = "UPDATE models SET model_status = 'RUNNING' WHERE id = '" + str(model_id) + "'"
db_manager.update_database(sql)

################################################################################
#Get all relevant model parameters
dataset_id = model_to_run['dataset_id']
max_epochs = model_to_run['max_epochs']
batch_size = model_to_run['batch_size']
num_steps = model_to_run['num_steps']
test_set_perc = model_to_run['test_set_perc']
dev_set_perc = model_to_run['dev_set_perc']
early_stop_patience = model_to_run['early_stop_patience']
early_stop_delta = model_to_run['early_stop_delta']
random_seed = model_to_run['random_seed']
loss_type = model_to_run['loss_type']
optimizer = model_to_run['optimizer']


################################################################################
#Get all relevant model parameters
sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
dataset = db_manager.fetch_mysql_result(sql)[0]

index_col_name = dataset['index_col_name']
target_col_name = dataset['target_col_name']
bucket_name_gs = dataset['bucket_name_gs']
filename_gs = dataset['filename_gs']
filename_local = 'data/' + filename_gs
filename_local = os.path.join(dirname, filename_local)

################################################################################
##### LOAD FILE FROM GOOGLE STORAGE, SAVE LOCALLY #####
try:
    google_storage_obj = GoogleStorageClient(bucket_name_gs)
    google_storage_obj.download_blob_to_dest(filename_gs, filename_local)
except Exception as e:
	print('Google storage exception!')
	print (e)
	sql = "UPDATE models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
	db_manager.update_database(sql)
	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
	exit()

################################################################################
# Data config variables
numpy.random.seed(random_seed)
raw_ohlcv_df = pd.read_csv(filename_local,header=0, parse_dates=[0], index_col=index_col_name)
os.remove(filename_local)
ohlcv_columns = list(raw_ohlcv_df.columns.values)
target_col_df = raw_ohlcv_df[target_col_name]

################################################################################
# Generate all features from raw data, save processed files to google storage
feature_generator = FeatureGenerator(index_col_name, raw_ohlcv_df, batch_size, dev_set_perc, test_set_perc, random_seed, target_col_name, num_steps)
feature_generator.generate_all_features()
feature_generator.save_processed_inputs(model_id, dataset_id, dirname, google_storage_obj)
num_features = feature_generator.get_num_features()

print ('NUM_FEATURES:   ' + str(num_features))

train_X_numpy, train_y_numpy, train_index_numpy = feature_generator.get_dataset(DatasetSplit.TRAIN)
dev_X_numpy, dev_y_numpy, dev_index_numpy = feature_generator.get_dataset(DatasetSplit.DEV)

train_class_weights = feature_generator.train_class_weights


################################################################################


################################################################################
# Create and compile model

model = Sequential()
model.add(Bidirectional(LSTM(50, batch_input_shape=(batch_size, num_steps, num_features), return_sequences=True)))
model.add(Dropout(0.2))
model.add(Bidirectional(LSTM(100, batch_input_shape=(batch_size, num_steps, num_features), return_sequences=True)))
model.add(Dropout(0.2))
model.add(Bidirectional(LSTM(50, batch_input_shape=(batch_size, num_steps, num_features))))
model.add(Dropout(0.2))
model.add(Dense(3, activation='softmax'))
model.compile(loss=loss_type, optimizer=optimizer)


################################################################################
#Train model
time_callback = TimeHistory()
es = EarlyStopping(monitor='val_loss', min_delta=early_stop_delta, patience=early_stop_patience, verbose=2, mode='min', restore_best_weights=True)

history = model.fit(train_X_numpy, train_y_numpy, epochs=max_epochs, batch_size=batch_size, verbose=2, shuffle=False, validation_data=(dev_X_numpy, dev_y_numpy), callbacks=[es, time_callback], class_weight=train_class_weights)

################################################################################
#Save metadata from each epoch to database
for i  in range(0, len(history.history['loss'])):
    sql = "INSERT INTO epochs (model_id, epoch_num, run_time_seconds, training_loss, validation_loss, end_datetime) VALUES ('%s', '%s', '%s', '%s', '%s', NOW())" % (str(model_id), str(i+1), str(int(time_callback.times[i])), str(history.history['loss'][i]), str(history.history['val_loss'][i]))
    db_manager.update_database(sql)
###########################################
#Save model to file and upload to google storage
model_filename = 'models/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '.h5'
local_model_filename = os.path.join (dirname, model_filename)
model.save(local_model_filename)

try:
    cloud_model_filename = model_filename
    google_storage_obj.upload_blob_from_file(local_model_filename, cloud_model_filename)
    sql = "UPDATE models SET model_file_gs = '" + cloud_model_filename + "' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)
    os.remove(local_model_filename) #NOTE:Doesn't this remove the model file before uploading to GS?

except Exception as e:
	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
	print(e)
	sql = "UPDATE models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
	db_manager.update_database(sql)
	exit()

################################################################################
#Save model to file and upload to google storage
sql = "UPDATE models SET model_status = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
db_manager.update_database(sql)

try:
	print ('Skipping twilio for TESTING purposes')
	#send_text_msg ('5146547219', 'Code is done :)  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
except Exception as e:
    print ('Twilio ERROR')
    print (e)

restart_server()
