from src.enum_classes import *
import pandas as pd
import numpy as np
import talib
from src.cc_ts_utils import MinMaxNormalizer
from src.TechnicalsOptimizer import TechnicalsOptimizer
import os
import csv

#NOTE: Currently only supports time-sampled candles
class RandomForestFeatureGenerator:

    def __init__(self, index_col_name, raw_df, dev_set_perc, test_set_perc, random_seed, target_col_name):
        self.__test_set_perc = test_set_perc
        self.__dev_set_perc = dev_set_perc
        self.__random_seed = random_seed
        self.__index_col_name = index_col_name
        self.__target_col_name = target_col_name

        self.__raw_dfs = dict() #Dictionary of DatasetSplit --> raw df
        self.__feature_dfs = dict() #Dictionary of DatasetSplit --> feature df
        self.__feature_numpy = dict() #Dictionary of DatasetSplit --> feature numpy array

        self.__target_dfs = dict() #Dictionary of DatasetSplit --> target df
        self.__target_numpy = dict() #Dictionary of DatasetSplit --> target numpy array

        self.__index_col_numpy = dict()  #Dictionary of DatasetSplit --> index column numpy array
        self.__dataset_splits = {
            DatasetSplit.TRAIN: 1 - (self.__test_set_perc + self.__dev_set_perc),
            DatasetSplit.DEV: self.__dev_set_perc,
            DatasetSplit.TEST: self.__test_set_perc
        }

        self._raw_df_cols = raw_df.columns.values
        self.__num_features = ''

        if 'time_period_start.1' in raw_df.columns.values:
            raw_df.drop('time_period_start.1', inplace=True, axis=1)

        if (self.__test_set_perc > 0) and (self.__test_set_perc > 0):
            self.__train_dev_test_split(raw_df)
        else:
            self.__raw_dfs[DatasetSplit.TRAIN] = raw_df

    def generate_all_features (self):
        best_ta_params = self.optimize_technicals()
        #self.compute_new_features() #TODO: This should be turned on for "extended-bars" but not standard OHLCV
        self.compute_ma_diffs(ignore_cols=[self.__index_col_name, self.__target_col_name])
        #self.__create_lagged_features(ignore_col=self.__target_col_name)
        self.__separate_features_target()
        self.__calc_class_imbalances()
        #self.__convert_dfs_to_numpy()
        #self.__reshape_normalize_features()
        #self.__cleanup_data_dimensions()
        return best_ta_params



    def features_from_dict (self, dict):
        self.generate_dict_features(dict)
        #self.compute_new_features() #TODO: This should be turned on for "extended-bars" but not standard OHLCV
        self.compute_ma_diffs(ignore_cols=[self.__index_col_name, self.__target_col_name])
        #self.__create_lagged_features(ignore_col=self.__target_col_name)
        self.__separate_features_target()
        self.__calc_class_imbalances()


    def generate_dict_features (self, best_params, open_col='price_open', high_col='price_high', low_col='price_low', close_col='price_close', volume_col='volume_traded'):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                df = self.__raw_dfs[dataset_split].copy(deep=True)

                open = df[open_col].values
                high = df[close_col].values
                low = df[low_col].values
                close = df[close_col].values
                volume = df[volume_col].values

                #Optuna optimized TA features
                df['SMA_' + str(best_params['SMA'])] = talib.SMA(close, timeperiod=best_params['SMA'])
                df['EMA_' + str(best_params['EMA'])] = talib.EMA(close, timeperiod=best_params['EMA'])
                df['MOM_' + str(best_params['MOM'])] = talib.MOM(close, timeperiod=best_params['MOM'])
                df['RSI_' + str(best_params['RSI'])] = talib.RSI(close, timeperiod=best_params['RSI'])
                df['ADX_' + str(best_params['ADX'])] = talib.ADX(high, low, close, timeperiod=best_params['ADX'])
                df['ATR_' + str(best_params['ATR'])] = talib.ATR(high, low, close, timeperiod=best_params['ATR'])
                df['CCI_' + str(best_params['CCI'])] = talib.CCI(high, low, close, timeperiod=best_params['CCI'])
                df['SAR_' + str(best_params['SAR'])] = talib.SAR(high, low, acceleration=best_params['SAR'])
                df['AROONOSC_' + str(best_params['AROONOSC'])] = talib.AROONOSC(high, low, timeperiod=best_params['AROONOSC'])
                df['MFI_' + str(best_params['MFI'])] = talib.MFI(high, low, close, volume, timeperiod=best_params['MFI'])
                df['DX_' + str(best_params['DX'])] = talib.DX(high, low, close, timeperiod=best_params['DX'])
                df['WILLR_' + str(best_params['WILLR'])] = talib.WILLR(high, low, close, timeperiod=best_params['WILLR'])
                df['WMA_' + str(best_params['WMA'])] = talib.WMA(close, timeperiod=best_params['WMA'])

                #Fixed parameter TA features
                df['OBV'] = talib.OBV(close, volume)
                df['BOP'] = talib.BOP(open, high, low, close)
                df['WCLPRICE'] = talib.WCLPRICE(high, low, close)
                df['AD'] = talib.AD(high, low, close, volume)

                df.dropna(inplace=True)
                self.__feature_dfs[dataset_split] = df


    def get_numpy_dataset (self, dataset_split):
        X = self.__feature_numpy[dataset_split]
        y = self.__target_numpy[dataset_split]
        index = self.__index_col_numpy[dataset_split]

        return X, y, index

    def get_df_dataset (self, dataset_split):
        if dataset_split in self.__feature_dfs:
            X = self.__feature_dfs[dataset_split]
            index = self.__feature_dfs[dataset_split].index
        else:
            X = None
            index = None


        if dataset_split in self.__target_dfs:
            y = self.__target_dfs[dataset_split]
        else:
            y = None

        return X, y, index


    def save_processed_inputs (self, model_id, dataset_id, dirname, google_storage_obj, folder_path='processed_model_inputs/'):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                features_data = self.__feature_dfs[dataset_split]
                features_data_filename =  folder_path + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + dataset_split.value + '_X.csv'
                features_data_filename_full = os.path.join(dirname, features_data_filename)

                target_data = self.__target_dfs[dataset_split]
                target_data_filename =  folder_path + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + dataset_split.value + '_y.csv'
                target_data_filename_full = os.path.join(dirname, target_data_filename)

                features_data.to_csv(features_data_filename_full)
                google_storage_obj.upload_blob_from_file(features_data_filename_full, features_data_filename)
                os.remove(features_data_filename_full)

                target_data.to_csv(target_data_filename_full)
                google_storage_obj.upload_blob_from_file(target_data_filename_full, target_data_filename)
                os.remove(target_data_filename_full)

    def __separate_features_target (self):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                if self.__target_col_name in self.__feature_dfs[dataset_split].columns:
                    self.__target_dfs[dataset_split] = self.__feature_dfs[dataset_split][self.__target_col_name]
                    self.__feature_dfs[dataset_split].drop(self.__target_col_name, axis=1, inplace=True)

    def __create_lagged_features (self, ignore_col=''):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                df = self.__feature_dfs[dataset_split]
                #Loop through each feature column, creating lagged features
                for f in df.columns.values:
                    if f != ignore_col:
                        for step in range(0, self.__num_steps):
                            df[f + '_lag_' + str(step)] = df[f].shift(step)
                        df.drop(f, inplace=True, axis=1)

                df.dropna(inplace=True)
                self.__feature_dfs[dataset_split] = df

    '''
    def __reshape_normalize_features (self):
        for dataset_split in self.__dataset_splits:
            feature_numpy = self.__feature_numpy[dataset_split]
            #NOTE: "num_features" must be the original number of features (before lags)
            num_features = int(len(self.__feature_dfs[dataset_split].columns.values) / self.__num_steps)
            self.__num_features = num_features
            feature_numpy = feature_numpy.reshape((feature_numpy.shape[0], self.__num_steps, num_features), order='F')
            scaler = MinMaxNormalizer()
            feature_numpy = scaler.normalize(tensor=feature_numpy, axis=(1, ))
            self.__feature_numpy[dataset_split] = feature_numpy
    '''


    def __calc_class_imbalances (self):
        if DatasetSplit.TRAIN in self.__target_dfs:
            class_dists = {

                1: 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[1] / len(self.__target_dfs[DatasetSplit.TRAIN])),
                -1: 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[-1] / len(self.__target_dfs[DatasetSplit.TRAIN])),
            }

            try:
                class_dists[0] = 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[0] / len(self.__target_dfs[DatasetSplit.TRAIN]))
            except:
                pass

            sums = sum(class_dists.values())

            self.train_class_weights = {
                1: class_dists[1] / sums,
                -1: class_dists[-1] / sums,
            }

            try:
                self.train_class_weights[0] = class_dists[0] / sums
            except:
                pass



    def __convert_dfs_to_numpy(self):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                self.__feature_numpy[dataset_split] = self.__feature_dfs[dataset_split].values
                #self.__target_numpy[dataset_split] = self.__target_dfs[dataset_split].values
                self.__index_col_numpy[dataset_split] = self.__target_dfs[dataset_split].index.values



    def __train_dev_test_split (self, raw_df):
        df = raw_df.copy(deep=True)

        dev_perc = self.__dev_set_perc
        test_perc = self.__test_set_perc

        num_train_examples = int(np.floor((len(df) * (1 - test_perc - dev_perc))))
        num_dev_examples = int(np.floor((len(df) * (1 - test_perc)))) - num_train_examples
        num_test_examples = len(df) - num_train_examples - num_dev_examples

        train = df.iloc[:-(num_dev_examples + num_test_examples)]
        dev = df.iloc[-(num_dev_examples + num_test_examples):-num_test_examples]
        test = df.iloc[-num_test_examples:]

        self.__raw_dfs[DatasetSplit.TRAIN] = train
        self.__raw_dfs[DatasetSplit.DEV] = dev
        self.__raw_dfs[DatasetSplit.TEST] = test


    def compute_new_features(self, high_col='price_high', low_col='price_low', close_col='price_close', volume_col='volume_traded'):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                df = self.__raw_dfs[dataset_split].copy(deep=True)

                open = df[open_col]
                high = df[high_col]
                low = df[low_col]
                close = df[close_col]
                total_volume = df[volume_col]

                count_buys = df['count_buys']
                count_sells = df['count_sells']
                volume_buys = df['volume_buys']
                volume_sells = df['volume_sells']
                total_trade_count = df['count_trades']

                #TODO: Handle divisions by zero and replace with a meaningful value (not 'inf')
                df['buy_trades_to_sell_trades'] = count_buys / count_sells
                df['buy_volume_to_sell_volume'] = volume_buys / volume_sells

                #Interbar strength -->  (close-low) / (high-low)
                #TODO: Replace screwed up values with 0
                df['interbar_strength'] = (close - low) / (high - low)
                df['interbar_strength'].replace(np.nan, 0, inplace=True) #For cases where high-low = 0 AND close-low = 0

                df.replace(np.inf, 1, inplace=True)
                cols_to_merge = self.__feature_dfs[dataset_split].columns.difference(df.columns)

                df = pd.merge(df, self.__feature_dfs[dataset_split][cols_to_merge], on=[self.__index_col_name], copy=False)

                df.dropna(inplace=True)
                self.__feature_dfs[dataset_split] = df



    #TODO: Pass CUSUM events to this method
    #ANOTHER GIT TEST (TODO: delete this comment)
    def optimize_technicals(self, open_col='price_open', high_col='price_high', low_col='price_low', close_col='price_close', volume_col='volume_traded'):
        #1) Tune features on train set
        #2) Apply optimal feature values to train/dev/test

        train_df = self.__raw_dfs[DatasetSplit.TRAIN].copy(deep=True)

        ta_opt = TechnicalsOptimizer(train_df)
        ta_opt.tune_all()
        best_params = ta_opt.optimal_params

        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                df = self.__raw_dfs[dataset_split].copy(deep=True)

                open = df[open_col].values
                high = df[close_col].values
                low = df[low_col].values
                close = df[close_col].values
                volume = df[volume_col].values

                #Optuna optimized TA features
                df['SMA_' + str(best_params['SMA'])] = talib.SMA(close, timeperiod=best_params['SMA'])
                df['EMA_' + str(best_params['EMA'])] = talib.EMA(close, timeperiod=best_params['EMA'])
                df['MOM_' + str(best_params['MOM'])] = talib.MOM(close, timeperiod=best_params['MOM'])
                df['RSI_' + str(best_params['RSI'])] = talib.RSI(close, timeperiod=best_params['RSI'])
                df['ADX_' + str(best_params['ADX'])] = talib.ADX(high, low, close, timeperiod=best_params['ADX'])
                df['ATR_' + str(best_params['ATR'])] = talib.ATR(high, low, close, timeperiod=best_params['ATR'])
                df['CCI_' + str(best_params['CCI'])] = talib.CCI(high, low, close, timeperiod=best_params['CCI'])
                df['SAR_' + str(best_params['SAR'])] = talib.SAR(high, low, acceleration=best_params['SAR'])
                df['AROONOSC_' + str(best_params['AROONOSC'])] = talib.AROONOSC(high, low, timeperiod=best_params['AROONOSC'])
                df['MFI_' + str(best_params['MFI'])] = talib.MFI(high, low, close, volume, timeperiod=best_params['MFI'])
                df['DX_' + str(best_params['DX'])] = talib.DX(high, low, close, timeperiod=best_params['DX'])
                df['WILLR_' + str(best_params['WILLR'])] = talib.WILLR(high, low, close, timeperiod=best_params['WILLR'])
                df['WMA_' + str(best_params['WMA'])] = talib.WMA(close, timeperiod=best_params['WMA'])

                #Fixed parameter TA features
                df['OBV'] = talib.OBV(close, volume)
                df['BOP'] = talib.BOP(open, high, low, close)
                df['WCLPRICE'] = talib.WCLPRICE(high, low, close)
                df['AD'] = talib.AD(high, low, close, volume)


                df.dropna(inplace=True)
                self.__feature_dfs[dataset_split] = df

        return best_params

    def compute_ma_diffs (self, ma_windows=[2,3,6], ignore_cols=[]):
        for dataset_split, split_perc in self.__dataset_splits.items():
            if split_perc > 0:
                df = self.__feature_dfs[dataset_split].copy(deep=True)

                for f in df.columns.values:
                    if f not in ignore_cols:
                        for ma_window in ma_windows:
                            #MA
                            df[str(f) + '_MA_' + str(ma_window)] = df[f].rolling(ma_window).mean()

                            # diff(MA)     Should be T - (T-1) --> No data snooping
                            df[str(f) + '_DIFF(MA)_' + str(ma_window)] = df[f + '_MA_' + str(ma_window)] - df[f + '_MA_' + str(ma_window)].shift(1)

                            # MA(diff(MA))
                            df[str(f) + '_MA(DIFF(MA))_' + str(ma_window)] = df[f + '_DIFF(MA)_' + str(ma_window)].rolling(ma_window).mean()

                cols_to_merge = self.__feature_dfs[dataset_split].columns.difference(df.columns)
                df = pd.merge(self.__feature_dfs[dataset_split][cols_to_merge], df, on=[self.__index_col_name], copy=False)

                df.dropna(inplace=True)
                self.__feature_dfs[dataset_split] = df

        # TODO: Replace this with tsfresh
        def compute_tsfresh_features(self, ma_windows=[2, 3, 6], ignore_cols=[]):
            for dataset_split, split_perc in self.__dataset_splits.items():
                if split_perc > 0:
                    df = self.__feature_dfs[dataset_split].copy(deep=True)

                    #TODO: Implement tsfresh here
                    '''
                    for f in df.columns.values:
                        if f not in ignore_cols:
                            for ma_window in ma_windows:
                                # MA
                                df[str(f) + '_MA_' + str(ma_window)] = df[f].rolling(ma_window).mean()

                                # diff(MA)     Should be T - (T-1) --> No data snooping
                                df[str(f) + '_DIFF(MA)_' + str(ma_window)] = df[f + '_MA_' + str(ma_window)] - df[
                                    f + '_MA_' + str(ma_window)].shift(1)

                                # MA(diff(MA))2
                                df[str(f) + '_MA(DIFF(MA))_' + str(ma_window)] = df[
                                    f + '_DIFF(MA)_' + str(ma_window)].rolling(ma_window).mean()
                    '''


                    cols_to_merge = self.__feature_dfs[dataset_split].columns.difference(df.columns)
                    df = pd.merge(self.__feature_dfs[dataset_split][cols_to_merge], df, on=[self.__index_col_name],
                                  copy=False)



                    df.dropna(inplace=True)
                    self.__feature_dfs[dataset_split] = df
