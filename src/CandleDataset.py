from src.TargetLabeler import TargetLabeler
from src.enum_classes import *
import hashlib

#NOTE: Currently only supports time-sampled candles
class CandleDataset:
    DEFAULT_NEUTRAL_PERC_THRESHOLD = 0.003

    def __init__(self, raw_data_filepath, exchange, asset_pair, raw_df, sampling_type, sampling_period, bucket_name_gs, col_to_label='', labeled_filepaths=''):
        self.__sampling_period = sampling_period
        self.__sampling_type = sampling_type

        self.__index_col_name = 'time_period_start'

        self.__prediction_horizon_periods = 1 #NOTE: This is hard coded for now since we will always be working with t+1 predictions
        self.__col_to_label = col_to_label
        self.__filenames_gs = dict() # Dict of target_type --> filenames
        self.__bucket_name_gs = bucket_name_gs

        self.__labeled_dfs = dict() # Dict of target_type --> DF
        self.__labeled_filepaths = labeled_filepaths # Dict of target_type --> DF

        self.__raw_df = raw_df
        self.__raw_data_filepath = raw_data_filepath

        self.__raw_df_start_date = str(min(self.__raw_df.index.values))
        self.__raw_df_end_date = str(max(self.__raw_df.index.values))

        self.__exchange = exchange
        self.__asset_pair = asset_pair

        self.__dataset_hash = '' # Dict of target_type --> hash

        self.neutral_perc_threshold = 0
        self.__label_value_counts = dict() # Dict of target_type --> value counts (only applies to classification)
        self.__target_type = ''


    def label_dataset (self, target_type=TargetType.PERC_RETURN, neutral_perc_threshold=DEFAULT_NEUTRAL_PERC_THRESHOLD):
        self.__target_type = target_type
        self.create_hash()

        if self.__target_type == TargetType.PERC_RETURN:
            print ('LABELING PERCENT RETURN')
            return self.label_perc_return(self.__labeled_filepaths[target_type])

        elif self.__target_type == TargetType.BINARY_MOMENTUM:
            print ('LABELING BINARY')
            return self.label_binary_momentum(self.__labeled_filepaths[target_type])

        elif self.__target_type == TargetType.TERNARY_MOMENTUM:
            print ('LABELING TERNARY')
            self.neutral_perc_threshold = neutral_perc_threshold
            return self.label_ternary_momentum(self.__labeled_filepaths[target_type], neutral_perc_threshold)

        else:
            print ('UNKNOWN Target Type to label!')
            raise Exception ('Unknown target type in CandleDataset.label_dataset():  ' + str(target_type))
            exit()

    def get_raw_data (self):
        return self.__raw_df.copy(deep=True)

    def save_to_csv (self, filepath, target_type=None):
        if not target_type:
            self.__raw_df.to_csv(filepath)
        else:
            self.__labeled_dfs[target_type].to_csv(filepath)


    def save_to_google_storage (self, gs_obj, target_type, current_filepath, destination_filepath):
        res = gs_obj.upload_blob_from_file(current_filepath, destination_filepath)
        self.__filenames_gs[target_type] = destination_filepath
        print (current_filepath)

    def label_binary_momentum(self, destination):
        target_labeler = TargetLabeler(self.__raw_data_filepath, self.__asset_pair, self.__exchange, self.__sampling_period, self.__prediction_horizon_periods, self.__col_to_label, self.neutral_perc_threshold)
        labeled_file, df, value_counts = target_labeler.label_binary_momentum(destination)
        self.__labeled_dfs[TargetType.BINARY_MOMENTUM] = df
        self.__label_value_counts[TargetType.BINARY_MOMENTUM] = value_counts

        return labeled_file

    def label_ternary_momentum(self, destination, neutral_perc_threshold):
        target_labeler = TargetLabeler(self.__raw_data_filepath, self.__asset_pair, self.__exchange, self.__sampling_period, self.__prediction_horizon_periods, self.__col_to_label, self.neutral_perc_threshold)
        labeled_file, df, value_counts = target_labeler.label_ternary_momentum(destination)
        self.__labeled_dfs[TargetType.TERNARY_MOMENTUM] = df
        self.__label_value_counts[TargetType.TERNARY_MOMENTUM] = value_counts

        return labeled_file

    def label_perc_return(self, destination):
        target_labeler = TargetLabeler(self.__raw_data_filepath, self.__asset_pair, self.__exchange, self.__sampling_period, self.__prediction_horizon_periods, self.__col_to_label, self.neutral_perc_threshold)
        labeled_file, df = target_labeler.label_perc_return(destination)
        self.__labeled_dfs[TargetType.PERC_RETURN] = df

        return labeled_file

        #TODO: Have this loop through all target types
    def save_to_sql(self, db_manager, target_type):
        num_up_labels = self.__label_value_counts[target_type][1]
        num_down_labels = self.__label_value_counts[target_type][-1]

        #Make this SQL statement cleaner?
        sql = "INSERT INTO datasets (sampling_type, sampling_period, target_type, index_col_name, target_col_name, prediction_horizon_periods, asset_pair, exchange, start_datetime, end_datetime, filename_gs, dataset_hash, bucket_name_gs, neutral_perc_threshold, num_up_labels, num_down_labels) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (str(self.__sampling_type.value), str(self.__sampling_period), str(self.__target_type.value), str(self.__index_col_name), str(TargetType.target_type_to_target_col_name(self.__target_type) + '_' + str(self.__prediction_horizon_periods)), str(self.__prediction_horizon_periods), str(self.__asset_pair), str(self.__exchange), str(self.__raw_df_start_date), str(self.__raw_df_end_date), str(self.__filenames_gs[self.__target_type]), str(self.__dataset_hash), str(self.__bucket_name_gs), str(self.neutral_perc_threshold), str(num_up_labels), str(num_down_labels))
        if self.__target_type == TargetType.TERNARY_MOMENTUM:
            num_neutral_labels = self.__label_value_counts[target_type][0.0]
            sql = "INSERT INTO datasets (sampling_type, sampling_period, target_type, index_col_name, target_col_name, prediction_horizon_periods, asset_pair, exchange, start_datetime, end_datetime, filename_gs, dataset_hash, bucket_name_gs, neutral_perc_threshold, num_up_labels, num_down_labels, num_neutral_labels) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (str(self.__sampling_type.value), str(self.__sampling_period), str(self.__target_type.value), str(self.__index_col_name), str(TargetType.target_type_to_target_col_name(self.__target_type) + '_' + str(self.__prediction_horizon_periods)), str(self.__prediction_horizon_periods), str(self.__asset_pair), str(self.__exchange), str(self.__raw_df_start_date), str(self.__raw_df_end_date), str(self.__filenames_gs[self.__target_type]), str(self.__dataset_hash), str(self.__bucket_name_gs), str(self.neutral_perc_threshold), str(num_up_labels), str(num_down_labels), str(num_neutral_labels))
        res = db_manager.update_database(sql)
        return res

    def create_hash (self):
        fields_dict = dict()

        fields_dict['raw_df_start_date'] = self.__raw_df_start_date
        fields_dict['raw_df_end_date'] = self.__raw_df_end_date
        fields_dict['sampling_type'] = self.__sampling_type.value
        fields_dict['sampling_period'] = self.__sampling_period
        fields_dict['prediction_horizon_periods'] = self.__prediction_horizon_periods
        fields_dict['target_col_name'] = TargetType.target_type_to_target_col_name(self.__target_type) + '_' + str(self.__prediction_horizon_periods)
        fields_dict['exchange'] = self.__exchange
        fields_dict['asset_pair'] = self.__asset_pair
        fields_dict['target_type'] = self.__target_type.value
        fields_dict['neutral_perc_threshold'] = self.neutral_perc_threshold

        m = hashlib.md5()
        m.update(str(fields_dict).encode('utf-8'))
        hash = m.hexdigest()
        self.__dataset_hash = hash
