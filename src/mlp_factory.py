from sklearn.metrics import f1_score
from sklearn.metrics import r2_score
from sklearn.metrics import make_scorer

from sklearn.neural_network import MLPClassifier
from sklearn.neural_network import MLPRegressor

import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import EarlyStopping
from keras import optimizers
from keras.backend.tensorflow_backend import set_session
import os

class MLPFactory():

    def __init__(self):

        #with tf.device('/device:GPU:%s' % str(gpu_index)):
        #    pass
        # config = tf.ConfigProto(device_count = {'GPU': gpu_index})
        #config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
        #config.log_device_placement = True  # to log device placement (on which device the operation ran)
                                    # (nothing gets printed in Jupyter, only if you run it standalone)
        #sess = tf.Session(config=tf.ConfigProto())
        #set_session(sess)



        #os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_index)


        pass
    ############################################################################
    #NOTE: This only works for classication problems
    def create_sequential_mlp(self, num_features, num_classes, params, loss='categorical_crossentropy', eval_metrics=['accuracy']):

        optimizers_dict = {
        'adam' : optimizers.Adam(lr=params['learning_rate'])
        }

        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation_function = params['activation_function']
        optimizer_algo = optimizers_dict[params['optimizer_algo']]
        dropout_perc = params['dropout_perc']

        model = Sequential()

        for i in range(num_layers):
            # First layer needs to have the same dimensions as the features vector
            if i == 0:
                model.add(Dense(num_neurons, activation=activation_function, input_dim=num_features))
            else:
                model.add(Dense(num_neurons, activation=activation_function))

            model.add(Dropout(dropout_perc, seed=1))

        # Output layer.
        model.add(Dense(num_classes, activation='softmax'))
        #Create Model
        model.compile(loss='categorical_crossentropy', optimizer=optimizer_algo,
                      metrics=['categorical_crossentropy'])

        return model
    ############################################################################
    def get_score(self, scorer_name, y_true, y_pred):
        if scorer_name == 'f1_score':
            return self.get_f1_score(y_true, y_pred)

        if scorer_name == 'r2_score':
            return self.get_r2_score(y_true, y_pred)

    ############################################################################
    def get_f1_score (self, y_true, y_pred):
        #TODO: Hard code F1 score for Keras
        '''
        class Metrics(keras.callbacks.Callback):
            def on_epoch_end(self, batch, logs={}):
                predict = np.asarray(self.model.predict(self.validation_data[0]))
                targ = self.validation_data[1]
                self.f1s=f1(targ, predict)
                return
        metrics = Metrics()
        model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, validation_data=[X_test,y_test],
               verbose=1, callbacks=[metrics])
        '''

        return f1_score(y_true, y_pred, average='micro')

    ############################################################################
    def get_r2_score (self, y_true, y_pred):
        return r2_score(y_true, y_pred)
