'''market data db manager
   @author: Fadi Hariri
   Last Edited: May 22, 2018
'''

import pymysql
import sys
import time
from src.ConsiliumLogger import *



class DatabaseManager:
    def __init__(self, kwargs, process, use_dict_cursor=False):
        '''
            instantiate a db connection
        '''
        self._kwargs=kwargs
        self._process = process
        self._logger=ConsiliumLogger('MySQL.Info.log')
        self._use_dict_cursor = use_dict_cursor
        self.connect_database()


    def connect_database (self):
        try:
            self._db_connection=pymysql.connect(**self._kwargs)
            if self._use_dict_cursor:
                print ('Using dict_cursor')
                self._cursor=self._db_connection.cursor(pymysql.cursors.DictCursor)
            else:
                self._cursor=self._db_connection.cursor()
            print("connected to mysql db")
            self._logger.logger("Process "+self._process+" Connected to database "+str(self._kwargs['database'])+" @ "+str(self._kwargs['host']), "info")
        except Exception as e:
            self._logger.logger("Process "+self._process+"    Error Connecting to database..."+str(e), "error")
            print ('Error in DatabaseManager --> Exiting')
            sys.exit(1)

    def update_database (self, mysql_query):
        i=5
        while i >= 0:
            i-=1
            try:
                # Execute the SQL command
                self._cursor.execute(mysql_query)
                # Commit your changes in the database
                self._db_connection.commit()
                return  True
            except Exception as e:
                if str(e).upper().find("DUPLICATE") == -1:
                    self._logger.logger ("Process "+self._process+": DB UPDATE error caused by "+str(e), "warning")
                    self._logger.logger (mysql_query, "warning")
                    # Rollback in case there is any error
                    try:
                        self._db_connection.rollback()
                    except Exception as e2:
                        self._logger.logger("Process "+self._process+": Failed to Rollback caused by "+str(e2), "error")
                    time.sleep(1+i)
                    self.close_connection()
                    self.connect_database()

    def fetch_mysql_result (self, mysql_query):
        '''
            return a list of rows
            data is stored as row [colName] foreach row in rows
        '''
        i=5
        while i >= 0:
            i-=1
            try:
                #self._cursor.execute("LOCK TABLE arbitrageorderbookdata READ;")
                self._cursor.execute(mysql_query)
                #self._cursor.execute("UNLOCK TABLES;")
                res= self._cursor.fetchall()
                self._db_connection.commit()#release any lock that might have been held
                return res
            except Exception as e:
                self._logger.logger("Process "+self._process+": DB Error Fetching records! Caused by "+str(e), "warning")
                #self._cursor.execute("UNLOCK TABLES;")
                time.sleep(1+i)
                self.close_connection()
                self.connect_database()

    def close_connection (self):
        try:
            self._db_connection.close()
            self._logger.logger ("Process "+self._process+": Closed database connection @ "+ str(self._kwargs['host']), "info")
        except Exception as e:
            time.sleep(1)
            self._logger.logger ("Error closing connection @ "+str(self._kwargs['host'])+" caused by: "+str(e), "error")


if __name__ == '__main__':
    kwargs = {'host': '35.193.233.156', 'user': 'root', 'password': 'consilium_access_log_api_2019!!',
              'database': 'marketdata_api',
              'ssl': {'ca': '/Users/fadihariri/Downloads/server-ca.pem',
                      'key': '/Users/fadihariri/Downloads/client-key.pem',
                      'cert': '/Users/fadihariri/Downloads/client-cert.pem', 'check_hostname': False},
              'port': 3306}
    dm=DatabaseManager (kwargs, 'test')
    dm.close_connection()
