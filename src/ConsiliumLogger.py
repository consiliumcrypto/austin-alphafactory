import logging

class ConsiliumLogger:

    def __init__(self, log_file, level = logging.DEBUG):
        self.filename = log_file
        log_setup = logging.getLogger(self.filename)
        formatter = logging.Formatter('%(levelname)s: %(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
        fileHandler = logging.FileHandler(self.filename, mode='a')
        fileHandler.setFormatter(formatter)
        log_setup.setLevel(level)
        log_setup.addHandler(fileHandler)
         
    def logger(self, msg, level):
        log = logging.getLogger(self.filename)
        if level == "info"    : log.info(msg)
        if level == "warning" : log.warning(msg)
        if level == "error"   : log.error(msg)


