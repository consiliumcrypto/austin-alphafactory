import pandas as pd
import numpy as np

#TODO: Add "if file exists" statements to each labeling function to skip that process and move to the next shift_period
#TODO: Rename to "candle data labeler"? --> Create new classes for trade data labeler and order book data labeler
class TargetLabeler():

    """
    Add target labels and create various shifted timeframes
    """
    def __init__(self, input_file, asset_name, exchange_name, sample_period, shift_period, col_to_label, neutral_perc_threshold, bar_type='time', index_col_name='time_period_start', drop_cols=[]):

        self._binary_momentum_base_name = 'target_binary_momentum'
        self._ternary_momentum_base_name = 'target_ternary_momentum'
        self._price_base_name = 'target_price'
        self._abs_return_base_name = 'target_abs_return'
        self._perc_return_base_name = 'target_perc_return'
        self._volatility_base_name = 'target_volatility'

        self.asset_name = asset_name
        self.exchange_name = exchange_name
        self.bar_type = bar_type

        #Check to make sure sample period is formatted correctly
        if sample_period[-1:] in ['S', 'T', 'H', 'M']:
            self.sample_period = sample_period
        else:
            raise ValueError ('Sample period formatted incorrectly. Please use S, T, H, M for second, minute, hour, month (respectively).')

        self.shift_period = shift_period
        self.index_col_name = index_col_name
        self.col_to_label = col_to_label
        self.raw_data = pd.read_csv(input_file, header=0, parse_dates=[0], index_col=self.index_col_name)
        self.raw_data.sort_index(inplace=True)
        self.raw_data.drop(drop_cols, axis=1, inplace=True)

        self.NEUTRAL_PERC_THRESHOLD = neutral_perc_threshold



    #NOTE: This is not needed when labeling price data for regression, only momentum
    def label_up_down_neutral(self, row, col_name='temp_diff_perc'):
        if row[col_name] > self.NEUTRAL_PERC_THRESHOLD:
            return 1
        elif row[col_name] < -self.NEUTRAL_PERC_THRESHOLD:
            return -1
        elif row[col_name] <= self.NEUTRAL_PERC_THRESHOLD and row[col_name] >= -self.NEUTRAL_PERC_THRESHOLD:
            return 0


    #NOTE: This is not needed when labeling price data for regression, only momentum
    def label_up_down(self, row, col_name='temp_diff_perc'):
        if row[col_name] >= 0:
            return 1
        else:
            return -1


    def label_ternary_momentum (self, destination_folder):
        #CREATE NEW TARGET COLUMNS

        raw_data =  self.raw_data.copy()

        target_type = self._ternary_momentum_base_name
        target_col = target_type + '_' + str(self.shift_period)
        raw_data['temp_diff'] =  (raw_data[self.col_to_label].shift(-self.shift_period)) - (raw_data[self.col_to_label])
        raw_data['temp_diff_perc'] = raw_data['temp_diff'] / (raw_data[self.col_to_label])
        raw_data[target_col] = raw_data.apply(self.label_up_down_neutral, axis=1)
        value_counts = raw_data[target_col].value_counts()

        raw_data = raw_data.dropna()
        raw_data.drop('temp_diff', axis=1, inplace=True)
        raw_data.drop('temp_diff_perc', axis=1, inplace=True)
        filename = destination_folder + '/' + self.asset_name + '_' + self.exchange_name + '_' + self.sample_period + '_' + self.bar_type + '_' + str(self.shift_period) + 'P_' + target_type + '_' + str(self.NEUTRAL_PERC_THRESHOLD) +'.csv'
        labeled_file = {'filename': filename, 'target_col': target_col, 'shift_period': self.shift_period, 'value_counts': value_counts, 'asset_name': self.asset_name, 'exchange_name': self.exchange_name, 'sample_period': self.sample_period}
        raw_data.to_csv(filename)
        raw_data.drop(target_col, inplace=True, axis=1)

        #metadata_dict = self.create_metadata_dict(destination_folder, target_type, labeled_files)

        return filename, raw_data, value_counts




    def label_binary_momentum (self, destination_folder):
        #CREATE NEW TARGET COLUMNS
        raw_data =  self.raw_data.copy()

        target_type = self._binary_momentum_base_name
        target_col = target_type + '_' + str(self.shift_period)
        raw_data['temp_diff'] =  (raw_data[self.col_to_label].shift(-self.shift_period)) - (raw_data[self.col_to_label])
        raw_data['temp_diff_perc'] = raw_data['temp_diff'] / (raw_data[self.col_to_label])
        raw_data[target_col] = raw_data.apply(self.label_up_down, axis=1)
        value_counts = raw_data[target_col].value_counts()

        raw_data = raw_data.dropna()
        raw_data.drop('temp_diff', axis=1, inplace=True)
        raw_data.drop('temp_diff_perc', axis=1, inplace=True)
        filename = destination_folder + '/' + self.asset_name + '_' + self.exchange_name + '_' + self.sample_period + '_' + self.bar_type + '_' + str(self.shift_period) + 'P_' + target_type + '.csv'
        labeled_file = {'filename': filename, 'target_col': target_col, 'shift_period': self.shift_period, 'value_counts': value_counts, 'asset_name': self.asset_name, 'exchange_name': self.exchange_name, 'sample_period': self.sample_period}
        raw_data.to_csv(filename)
        raw_data.drop(target_col, inplace=True, axis=1)

        #metadata_dict = self.create_metadata_dict(destination_folder, target_type, labeled_files)

        return filename, raw_data, value_counts





    def label_price (self, destination_folder):
        #CREATE NEW TARGET COLUMNS
        raw_data =  self.raw_data.copy()
        #labeled_files = list()

        target_type = self._price_base_name
        target_col = target_type + '_' + str(self.shift_period)
        raw_data[target_col] = (raw_data[self.col_to_label].shift(-self.shift_period))

        value_counts = raw_data[target_col].value_counts()
        temp_list = []

        for k, v in value_counts.items():
            temp_list.append([k,v])

        value_counts = temp_list
        print(raw_data[target_col].value_counts())

        raw_data = raw_data.dropna()

        filename = destination_folder + '/' + self.asset_name + '_' + self.exchange_name + '_' + self.sample_period + '_' + self.bar_type + '_' + str(self.shift_period) + 'P_' + target_type + '.csv'
        #labeled_files.append({'filename': filename, 'target_col': target_col, 'shift_period': self.shift_period, 'value_counts': value_counts})
        labeled_file = {'filename': filename, 'target_col': target_col, 'shift_period': self.shift_period, 'value_counts': value_counts, 'asset_name': self.asset_name, 'exchange_name': self.exchange_name, 'sample_period': self.sample_period}
        raw_data.to_csv(filename)
        raw_data.drop(target_col, inplace=True, axis=1)

        #metadata_dict = self.create_metadata_dict(destination_folder, target_type, labeled_files)

        return labeled_file



    def create_metadata_dict(self, destination_folder, target_type, labeled_files):
        metadata = dict()
        metadata['destination_folder'] = destination_folder
        metadata['shift_periods'] = self.shift_period
        metadata['perc_threshold'] = 0.005
        metadata['asset_name'] = self.asset_name
        metadata['exchange_name'] = self.exchange_name
        metadata['sample_period'] = self.sample_period
        metadata['bar_type'] = self.bar_type
        metadata['target_type'] = target_type
        metadata['index_col_name'] = self.index_col_name
        metadata['labeled_files'] = labeled_files

        return metadata

    def label_abs_return (self, destination_folder):
        #CREATE NEW TARGET COLUMNS
        raw_data =  self.raw_data.copy()

        raw_data[self._abs_return_base_name + '_' + str(self.shift_period)] = np.abs((raw_data[self.col_to_label].shift(-self.shift_period)) - (raw_data[self.col_to_label]))
        raw_data = raw_data.dropna()

        filename = destination_folder + '/' + self.asset_name + '_' + self.exchange_name + '_' + self.sample_period + '_' + self.bar_type + '_' + str(self.shift_period) + 'P_' + self._abs_return_base_name + '.csv'

        raw_data.to_csv(filename)
        return


    def label_perc_return (self, destination_folder):
        #CREATE NEW TARGET COLUMNS
        raw_data_df =  self.raw_data.copy()
        target_col_name = self._perc_return_base_name + '_' + str(self.shift_period)
        raw_data_df[target_col_name] = ((raw_data_df[self.col_to_label].shift(-self.shift_period)) - (raw_data_df[self.col_to_label])) / raw_data_df[self.col_to_label]
        raw_data_df = raw_data_df.dropna()

        filename = destination_folder + '/' + self.asset_name + '_' + self.exchange_name + '_' + self.sample_period + '_' + self.bar_type + '_' + str(self.shift_period) + 'P_' + self._perc_return_base_name + '.csv'

        raw_data_df.to_csv(filename)
        print ('saving percent return filename ' + filename)
        return filename, raw_data_df



    #TODO: High - Low
    def label_price_range (self, destination_folder):
        pass
