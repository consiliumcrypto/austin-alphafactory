import talib
import random
import numpy as np
import datetime
import dateutil.parser as dp
import math
import requests as rq
import pandas as pd
import time

import sklearn.metrics as sk_metrics
import keras
from twilio.rest import Client

import plotly
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import json

import os
import platform

from src.api_utils import *


class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


def train_dev_test_split (df, dev_perc=0.1, test_perc=0.1):
    num_train_examples = int(np.floor((len(df) * (1 - test_perc - dev_perc))))
    num_dev_examples = int(np.floor((len(df) * (1 - test_perc)))) - num_train_examples
    num_test_examples = len(df) - num_train_examples - num_dev_examples

    train = df.iloc[:-(num_dev_examples + num_test_examples)]
    dev = df.iloc[-(num_dev_examples + num_test_examples):-num_test_examples]
    test = df.iloc[-num_test_examples:]

    return train, dev, test

def plot_results_dict (results_dict, filename):
    shift_periods = results_dict['metadata']['target_shift_periods']
    models_run = results_dict['metadata']['models_run']
    model_results = dict()
    model_results_pca = dict()
    model_names = []

    for m in models_run:
        model_names.append(m['model_name'])

    #Initialize lists to hold results values for each model at each target shift period
    for m in model_names:
        model_results[m] = []
        model_results_pca[m] = []

    #Get list of best results for each model at each target shift period

    for i in results_dict['results']:
        shift_period = i['shift_period']
        model_name = i['model_name']
        final_max_fitness = i['final_max_fitness']
        final_max_fitness_pca = i['pca_final_max_fitness']
        model_results[model_name].append(final_max_fitness)
        model_results_pca[model_name].append(final_max_fitness_pca)

    ########## TESTING DATA ##########
    traces = []

    for k, v in model_results.items():
        scores = []
        for i in v:
            scores.append(i['score'])
        trace = go.Scatter(
            x = shift_periods,
            y = scores,
            name = k + ' Results'
        )

        traces.append(trace)

    for k, v in model_results_pca.items():
        scores = []
        for i in v:
            scores.append(i['score'])
        trace = go.Scatter(
            x = shift_periods,
            y = scores,
            name = k + ' Results (PCA)'
        )

        traces.append(trace)


    layout = go.Layout(
            yaxis=dict(
                title='Model Performance'
            ),
            xaxis=dict(
                title='Target variable shifted periods',
            )
    )
    ##################################


    fig=dict(data=traces, layout=layout)
    plotly.offline.plot(fig, auto_open=False, filename=filename)


def save_model(model, fname):
    with open(fname, 'wb') as f:
        pickle.dump(model, f)

def load_model (pickled_model):
    with open(pickled_model, 'rb') as fid:
        model = pickle.load(fid)
    return model


def send_text_msg (rec_num, msg):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    account_sid = 'ACe2dea3bce2410eb638ca13d555a9b65c'
    auth_token = 'b77220422cae0553bffadf9f9913d48f'
    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                         body=msg,
                         from_='+14387950253',
                         to='+1' + str(rec_num)
                     )
    return





def shutdown_server ():
    #Detect if this is running in the cloud
    if platform.system() == 'Linux':
        os.system("sudo shutdown now -h")
    else:
        print ('Not running in the cloud, not shutting down.')

def restart_server ():
    #Detect if this is running in the cloud
    if platform.system() == 'Linux':
        os.system("sudo reboot")
    else:
        print ('Not running in the cloud, not restarting.')

#Takes a dataframe with 'y' (actual) and 'y_pred' (predicted) columns
def calc_regressor_performance (df):
    #NOTE: Keys of this dict must match the column names in the regressor_performance table
    metrics = dict()

    #All predictions and actuals
    all_y = df['y'].tolist()
    all_preds = df['y_pred'].tolist()
    df['sign_preds'] = df['y_pred'].apply(np.sign)
    df['sign_y'] = df['y'].apply(np.sign)

    df['sign_correct'] = 0
    df.loc[df.sign_preds * df.sign_y > 0, 'sign_correct'] = 1

    df['sign_incorrect'] = 0
    df.loc[df.sign_preds * df.sign_y < 0, 'sign_incorrect'] = 1

    df['is_predicted'] = df['sign_correct'] + df['sign_incorrect']

    df['result'] = df.sign_preds * df['y']


    metrics['accuracy'] = df['sign_correct'].sum()*1.0 / (df['is_predicted'].sum() * 1.0) * 100.0
    metrics['edge'] = df['result'].mean()
    #metrics['edge_std_dev'] = metrics['edge'].std()
    metrics['noise'] = df['y_pred'].diff().abs().mean()

    metrics['y_chg'] = df['y'].abs().mean()
    metrics['y_pred_chg'] = df['y_pred'].abs().mean()
    metrics['prediction_calibration'] = metrics['y_pred_chg'] / metrics['y_chg']
    metrics['capture_ratio'] = metrics['edge'] / (metrics['y_chg'] * 100)

    metrics['edge_up'] = df[df['sign_preds'] == 1].result.mean() - df['y'].mean()
    metrics['edge_down'] = df[df['sign_preds'] == -1].result.mean() - df['y'].mean()

    metrics['edge_win'] = df[df['sign_correct'] == 1].result.mean() - df['y'].mean()
    metrics['edge_lose'] = df[df['sign_incorrect'] == 1].result.mean() - df['y'].mean()

    #Up move predictions and actuals
    up_preds = df[df['y_pred'] > 0]
    up_y = up_preds['y'].tolist()
    up_preds = up_preds['y_pred'].tolist()

    #Down move predictions and actuals
    down_preds = df[df['y_pred'] < 0]
    print (down_preds.head())
    down_y = down_preds['y'].tolist()
    down_preds = down_preds['y_pred'].tolist()

    #Metrics on total dataset
    metrics['mean_squared_error_score'] = sk_metrics.mean_squared_error(all_y, all_preds)
    metrics['r2_score'] = sk_metrics.r2_score(all_y, all_preds)
    metrics['explained_variance_score'] = sk_metrics.explained_variance_score(all_y, all_preds)
    metrics['mean_absolute_error_score'] = sk_metrics.mean_absolute_error(all_y, all_preds)
    metrics['median_absolute_error_score'] = sk_metrics.median_absolute_error(all_y, all_preds)

    #Metrics on "up" predictions
    if (len(down_y) > 1) and (len(down_preds) > 1):
        metrics['up_mean_squared_error_score'] = sk_metrics.mean_squared_error(up_y, up_preds)
        metrics['up_r2_score'] = sk_metrics.r2_score(up_y, up_preds)
        metrics['up_explained_variance_score'] = sk_metrics.explained_variance_score(up_y, up_preds)
        metrics['up_mean_absolute_error_score'] = sk_metrics.mean_absolute_error(up_y, up_preds)
        metrics['up_median_absolute_error_score'] = sk_metrics.median_absolute_error(up_y, up_preds)

    #Metrics on "down" predictions
    if (len(down_y) > 1) and (len(down_preds) > 1):
        metrics['down_mean_squared_error_score'] = sk_metrics.mean_squared_error(down_y, down_preds)
        metrics['down_r2_score'] = sk_metrics.r2_score(down_y, down_preds)
        metrics['down_explained_variance_score'] = sk_metrics.explained_variance_score(down_y, down_preds)
        metrics['down_mean_absolute_error_score'] = sk_metrics.mean_absolute_error(down_y, down_preds)
        metrics['down_median_absolute_error_score'] = sk_metrics.median_absolute_error(down_y, down_preds)

    #Replace any 'nans' with 0
    for k, v in metrics.items():
        if math.isnan(v):
            metrics[k]=0

    return metrics

#TODO:
def calc_classifier_performance (y_ypred_df):
    #NOTE: Keys of this dict must match the column names in the regressor_performance table
    metrics = dict()

    up_preds = y_ypred_df[y_ypred_df['y_pred'] == 1]
    up_actuals = y_ypred_df[y_ypred_df['y'] == 1]
    num_up_preds = len(up_preds)
    num_up_actuals = len(up_actuals)
    correct_up_preds = up_preds[up_preds['y'] == 1]
    num_correct_up_preds = len(correct_up_preds)


    metrics['accuracy'] = sk_metrics.balanced_accuracy_score(y_ypred_df['y'], y_ypred_df['y_pred'])

    if (num_up_preds > 0) and (num_up_actuals > 0):
        metrics['up_tp_ratio'] = num_correct_up_preds / num_up_preds
        metrics['up_capture_ratio'] = num_correct_up_preds / num_up_actuals
    else:
        print('CANNOT COMPUTE UP ACCURACY')
        metrics['up_tp_ratio'] = 0
        metrics['up_capture_ratio'] = 0


    down_preds = y_ypred_df[y_ypred_df['y_pred'] == -1]
    down_actuals = y_ypred_df[y_ypred_df['y'] == -1]
    num_down_preds = len(down_preds)
    num_down_actuals = len(down_actuals)
    correct_down_preds = down_preds[down_preds['y'] == -1]
    num_correct_down_preds = len(correct_down_preds)

    if (num_down_preds > 0) and (num_down_actuals > 0):
        metrics['down_tp_ratio'] = num_correct_down_preds / num_down_preds
        metrics['down_capture_ratio'] = num_correct_down_preds / num_down_actuals
    else:
        print('CANNOT COMPUTE DOWN ACCURACY')
        metrics['down_tp_ratio'] = 0
        metrics['down_capture_ratio'] = 0


    neutral_preds = y_ypred_df[y_ypred_df['y_pred'] == 0]
    neutral_actuals = y_ypred_df[y_ypred_df['y'] == 0]
    num_neutral_preds = len(neutral_preds)
    num_neutral_actuals = len(neutral_actuals)
    correct_neutral_preds = neutral_preds[neutral_preds['y'] == 0]
    num_correct_neutral_preds = len(correct_neutral_preds)

    if (num_neutral_preds > 0) and (num_neutral_actuals > 0):
        metrics['neutral_tp_ratio'] = num_correct_neutral_preds / num_neutral_preds
        metrics['neutral_capture_ratio'] = num_correct_neutral_preds / num_neutral_actuals
    else:
        print('CANNOT COMPUTE NEUTRAL ACCURACY')
        metrics['neutral_tp_ratio'] = 0
        metrics['neutral_capture_ratio'] = 0

    y = y_ypred_df['y']
    y_pred = y_ypred_df['y_pred']

    metrics['f1_weighted'] = sk_metrics.f1_score(y, y_pred, average='weighted')
    f1_scores = sk_metrics.f1_score(y, y_pred, average=None)

    print('F1 SCORE:')
    print(f1_scores)

    if len(f1_scores) == 3:
        metrics['up_f1'] = f1_scores[2]
        metrics['down_f1'] = f1_scores[0]
        metrics['neutral_f1'] = f1_scores[1]
    elif len(f1_scores) == 2:
        metrics['up_f1'] = f1_scores[1]
        metrics['down_f1'] = f1_scores[0]


    y_pred_prob = y_ypred_df.drop(['y', 'y_pred'], axis=1)

    if len(y_pred_prob.columns.values) > 2:
        roc_auc = sk_metrics.roc_auc_score(y, y_pred_prob.values, multi_class='ovr', average='weighted')
    elif len(y_pred_prob.columns.values) == 2:
        roc_auc = sk_metrics.roc_auc_score(y, y_pred_prob.values[:, 1], average='weighted')

    metrics['roc_auc'] = roc_auc

    return metrics



class MinMaxNormalizer:
    """Normalize a single tensor using min-max"""

    def __init__(self, ub: float = 1.0, lb: float = 0.0):
        self.min = None
        self.max = None
        self.ub = ub
        self.lb = lb

    def normalize(self, tensor: np.ndarray, axis: tuple = (0,)):
        self._calc_min_max(tensor, axis)
        return self._normalize(tensor)

    def _calc_min_max(self, tensor: np.ndarray, axis: tuple):
        self.min = tensor.min(axis=axis, keepdims=True)
        self.max = tensor.max(axis=axis, keepdims=True) + 1e-8

    def _normalize(self, tensor: np.ndarray):
        return (tensor - self.min) / (self.max - self.min) * (
            self.ub - self.lb) + self.lb
