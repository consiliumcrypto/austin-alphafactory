from src.enum_classes import *
import pandas as pd
import numpy as np
import talib
from src.cc_ts_utils import MinMaxNormalizer
import os
import csv
from keras.utils.np_utils import to_categorical

#NOTE: Currently only supports time-sampled candles
class LSTMFeatureGenerator:

    def __init__(self, index_col_name, raw_df, batch_size, dev_set_perc, test_set_perc, random_seed, target_col_name, num_steps):
        self.__batch_size = batch_size
        self.__num_steps = num_steps
        self.__test_set_perc = test_set_perc
        self.__dev_set_perc = dev_set_perc
        self.__random_seed = random_seed
        self.__index_col_name = index_col_name
        self.__target_col_name = target_col_name

        self.__raw_dfs = dict() #Dictionary of DatasetSplit --> raw df
        self.__feature_dfs = dict() #Dictionary of DatasetSplit --> feature df
        self.__feature_numpy = dict() #Dictionary of DatasetSplit --> feature numpy array

        self.__target_dfs = dict() #Dictionary of DatasetSplit --> target df
        self.__target_numpy = dict() #Dictionary of DatasetSplit --> target numpy array

        self.__index_col_numpy = dict()  #Dictionary of DatasetSplit --> index column numpy array
        self.__dataset_splits = [DatasetSplit.TRAIN, DatasetSplit.DEV, DatasetSplit.TEST]

        self._raw_df_cols = raw_df.columns.values
        self.__num_features = ''

        self.__train_dev_test_split(raw_df)

    def generate_all_features (self):
        self.compute_technicals()
        #self.compute_new_features()
        #self.compute_ma_diffs(ignore_cols=[self.__index_col_name, self.__target_col_name])
        self.__create_lagged_features(ignore_col=self.__target_col_name)
        self.__separate_features_target()
        self.__calc_class_imbalances()
        self.__targets_to_categorical()
        self.__convert_dfs_to_numpy()
        self.__reshape_normalize_features()
        self.__cleanup_data_dimensions()

    def get_num_features (self):
        return self.__num_features

    def get_dataset (self, dataset_split):
        X = self.__feature_numpy[dataset_split]
        y = self.__target_numpy[dataset_split]
        index = self.__index_col_numpy[dataset_split]

        return X, y, index


    def save_processed_inputs (self, model_id, dataset_id, dirname, google_storage_obj, folder_path='processed_model_inputs/'):
        for dataset_split in self.__dataset_splits:
            features_data = self.__feature_numpy[dataset_split]
            features_data_filename =  folder_path + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + dataset_split.value + '_X.npy'
            features_data_filename_full = os.path.join(dirname, features_data_filename)

            target_data = self.__target_numpy[dataset_split]
            target_data_filename =  folder_path + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + dataset_split.value + '_y.npy'
            target_data_filename_full = os.path.join(dirname, target_data_filename)

            index_data = self.__index_col_numpy[dataset_split]
            index_data_filename =  folder_path + 'model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + dataset_split.value + '_index.csv'
            index_data_filename_full = os.path.join(dirname, index_data_filename)

            #Save features data (npy) to cloud
            #NOTE: "features_data" is 3 dimensional (LSTM input), so cannot be saved as a CSV
            np.save(features_data_filename_full, features_data)
            google_storage_obj.upload_blob_from_file(features_data_filename_full, features_data_filename)
            os.remove(features_data_filename_full)

            #Save target data (CSV) to cloud
            np.save(target_data_filename_full, target_data)
            google_storage_obj.upload_blob_from_file(target_data_filename_full, target_data_filename)
            os.remove(target_data_filename_full)

            #Save index data (CSV) to cloud
            with open(index_data_filename_full, "w") as f:
                writer = csv.writer(f)
                for i in range(0, len(index_data)):
                    writer.writerow([index_data[i]])
            google_storage_obj.upload_blob_from_file(index_data_filename_full, index_data_filename)
            os.remove(index_data_filename_full)


    def __separate_features_target (self):
        for dataset_split in self.__dataset_splits:
            self.__target_dfs[dataset_split] = self.__feature_dfs[dataset_split][self.__target_col_name]
            self.__feature_dfs[dataset_split].drop(self.__target_col_name, axis=1, inplace=True)

    def __create_lagged_features (self, ignore_col=''):
        for dataset_split in self.__dataset_splits:
            df = self.__feature_dfs[dataset_split]
            #Loop through each feature column, creating lagged features
            for f in df.columns.values:
                if f != ignore_col:
                    for step in range(0, self.__num_steps):
                        df[f + '_lag_' + str(step)] = df[f].shift(step)
                    df.drop(f, inplace=True, axis=1)

            df.dropna(inplace=True)
            self.__feature_dfs[dataset_split] = df


    def __reshape_normalize_features (self):
        for dataset_split in self.__dataset_splits:
            feature_numpy = self.__feature_numpy[dataset_split]
            #NOTE: "num_features" must be the original number of features (before lags)
            num_features = int(len(self.__feature_dfs[dataset_split].columns.values) / self.__num_steps)
            self.__num_features = num_features
            feature_numpy = feature_numpy.reshape((feature_numpy.shape[0], self.__num_steps, num_features), order='F')
            scaler = MinMaxNormalizer()
            feature_numpy = scaler.normalize(tensor=feature_numpy, axis=(1, ))
            self.__feature_numpy[dataset_split] = feature_numpy

    def __cleanup_data_dimensions (self):
        for dataset_split in self.__dataset_splits:
            feature_numpy = self.__feature_numpy[dataset_split]
            target_numpy = self.__target_numpy[dataset_split]
            index_col_numpy = self.__index_col_numpy[dataset_split]

            self.__feature_numpy[dataset_split][(self.__num_steps-1):, :]
            self.__target_numpy[dataset_split][(self.__num_steps-1):]
            self.__index_col_numpy[dataset_split][(self.__num_steps-1):]

            num_rows_to_drop = (len(feature_numpy)) % self.__batch_size
            print (len(feature_numpy))
            print ('Num rows to drop: '  + str(num_rows_to_drop))
            if num_rows_to_drop > 0:
                self.__feature_numpy[dataset_split] = self.__feature_numpy[dataset_split][:-(num_rows_to_drop), :]
                self.__target_numpy[dataset_split] = self.__target_numpy[dataset_split][:-(num_rows_to_drop)]
                self.__index_col_numpy[dataset_split] = self.__index_col_numpy[dataset_split][:-(num_rows_to_drop)]

            print (len(self.__feature_numpy[dataset_split]))


    def __calc_class_imbalances (self):
        class_dists = {
            0: 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[0] / len(self.__target_dfs[DatasetSplit.TRAIN])),
            1: 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[1] / len(self.__target_dfs[DatasetSplit.TRAIN])),
            2: 1. / (self.__target_dfs[DatasetSplit.TRAIN].value_counts()[-1] / len(self.__target_dfs[DatasetSplit.TRAIN])),
        }

        sums = sum(class_dists.values())

        self.train_class_weights = {
            0: class_dists[0] / sums,
            1: class_dists[1] / sums,
            2: class_dists[2] / sums,
        }


    def __targets_to_categorical (self):
        for split in self.__dataset_splits:
            self.__target_numpy[split] = to_categorical(self.__target_dfs[split], 3)


    def __convert_dfs_to_numpy(self):
        for dataset_split in self.__dataset_splits:
            self.__feature_numpy[dataset_split] = self.__feature_dfs[dataset_split].values
            #self.__target_numpy[dataset_split] = self.__target_dfs[dataset_split].values
            self.__index_col_numpy[dataset_split] = self.__target_dfs[dataset_split].index.values



    def __train_dev_test_split (self, raw_df):
        df = raw_df.copy(deep=True)
        dev_perc = self.__dev_set_perc
        test_perc = self.__test_set_perc

        num_train_examples = int(np.floor((len(df) * (1 - test_perc - dev_perc))))
        num_dev_examples = int(np.floor((len(df) * (1 - test_perc)))) - num_train_examples
        num_test_examples = len(df) - num_train_examples - num_dev_examples

        train = df.iloc[:-(num_dev_examples + num_test_examples)]
        dev = df.iloc[-(num_dev_examples + num_test_examples):-num_test_examples]
        test = df.iloc[-num_test_examples:]

        self.__raw_dfs[DatasetSplit.TRAIN] = train
        self.__raw_dfs[DatasetSplit.DEV] = dev
        self.__raw_dfs[DatasetSplit.TEST] = test


    def compute_new_features(self):
        for dataset_split in self.__dataset_splits:
            df = self.__raw_dfs[dataset_split].copy(deep=True)

            open = df['price_open']
            high = df['price_high']
            low = df['price_low']
            close = df['price_close']
            total_volume = df['volume_traded']

            count_buys = df['count_buys']
            count_sells = df['count_sells']
            volume_buys = df['volume_buys']
            volume_sells = df['volume_sells']
            total_trade_count = df['count_trades']

            #TODO: Handle divisions by zero and replace with a meaningful value (not 'inf')
            df['buy_trades_to_sell_trades'] = count_buys / count_sells
            df['buy_volume_to_sell_volume'] = volume_buys / volume_sells

            #Interbar strength -->  (close-low) / (high-low)
            #TODO: Replace screwed up values with 0
            df['interbar_strength'] = (close - low) / (high - low)
            df['interbar_strength'].replace(np.nan, 0, inplace=True) #For cases where high-low = 0 AND close-low = 0

            df.replace(np.inf, 1, inplace=True)
            cols_to_merge = self.__feature_dfs[dataset_split].columns.difference(df.columns)

            df = pd.merge(df, self.__feature_dfs[dataset_split][cols_to_merge], on=[self.__index_col_name], copy=False)

            if 'time_period_start.1' in df.columns.values:
                df.drop('time_period_start.1', inplace=True, axis=1)

            df.dropna(inplace=True)
            self.__feature_dfs[dataset_split] = df



    def compute_technicals(self, high_col='price_high', low_col='price_low', close_col='price_close', volume_col='volume_traded', periods=None):
        for dataset_split in self.__dataset_splits:
            df = self.__raw_dfs[dataset_split].copy(deep=True)

            high = df[close_col].values
            low = df[low_col].values
            close = df[close_col].values
            volume = df[volume_col].values

            #Default periods for each indicator
            if periods == None:
                SMA_PERIODS = [5, 20, 50]
                MOM_PERIODS = [10]
                RSI_PERIODS = [14]
                ADX_PERIODS = [14]
                EMA_PERIODS = [5, 20, 50]

            else:
                SMA_PERIODS = periods['sma']
                MOM_PERIODS = periods['mom']
                RSI_PERIODS = periods['rsi']
                ADX_PERIODS = periods['adx']
                EMA_PERIODS = periods['ema']

            # Compute SMA with various periods
            for i in SMA_PERIODS:
                df['SMA_' + str(i)] = talib.SMA(close, timeperiod=i)

            #Compute Momentum with various periods
            for i in MOM_PERIODS:
                df['MOM_' + str(i)] = talib.MOM(close, timeperiod=i)

            #Compute RSI with various periods
            for i in RSI_PERIODS:
                df['RSI_' + str(i)] = talib.RSI(close, timeperiod=i)

            #Compute Average Directional Index with various periods
            for i in ADX_PERIODS:
                df['ADX_' + str(i)] = talib.ADX(high, low, close, timeperiod=i)

            #Compute Average Directional Index with various periods
            for i in EMA_PERIODS:
                df['EMA_' + str(i)] = talib.EMA(close, timeperiod=i)

            df.dropna(inplace=True)
            self.__feature_dfs[dataset_split] = df


    def compute_ma_diffs (self, ma_windows=[2,3,6], ignore_cols=[]):
        for dataset_split in self.__dataset_splits:
            df = self.__feature_dfs[dataset_split].copy(deep=True)
            for f in df.columns.values:
                if f not in ignore_cols:
                    for ma_window in ma_windows:
                        #MA
                        #df[f + '_MA_' + str(ma_window)] = df[f].rolling(ma_window).mean()
                        df[str(f) + '_MA_' + str(ma_window)] = df[f].rolling(ma_window).mean()

                        # diff(MA)     Should be T - (T-1) --> No data snooping
                        df[str(f) + '_DIFF(MA)_' + str(ma_window)] = df[f + '_MA_' + str(ma_window)] - df[f + '_MA_' + str(ma_window)].shift(1)

                        # MA(diff(MA))
                        df[str(f) + '_MA(DIFF(MA))_' + str(ma_window)] = df[f + '_DIFF(MA)_' + str(ma_window)].rolling(ma_window).mean()

            cols_to_merge = self.__feature_dfs[dataset_split].columns.difference(df.columns)
            df = pd.merge(self.__feature_dfs[dataset_split][cols_to_merge], df, on=[self.__index_col_name], copy=False)

            if 'time_period_start.1' in df.columns.values:
                df.drop('time_period_start.1', inplace=True, axis=1)

            df.dropna(inplace=True)
            self.__feature_dfs[dataset_split] = df
