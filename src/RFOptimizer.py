import optuna
from sklearn.ensemble import RandomForestClassifier
import sklearn.metrics as sk_metrics
from src.enum_classes import *

#NOTE: Currently only supports time-sampled candles
class RFOptimizer:

    MAX_FEATURES = ['sqrt', 'log2']
    MAX_DEPTHS = [2, 50]
    MAX_SAMPLES = [0, 1]
    NUM_ESTIMATORS = [100, 500, 1000]
    SPLIT_CRITERIA = ['gini', 'entropy']

    def __init__(self, train_X, train_y, dev_X, dev_y, train_class_weights, random_seed, objective_metric):
        self.__train_X = train_X
        self.__train_y = train_y
        self.__dev_X = dev_X
        self.__dev_y = dev_y
        self.__train_class_weights = train_class_weights
        self.__random_seed = random_seed
        self.__objective_metric = objective_metric

        self.optimal_params = None
        self.optuna_trials = None


    def _rf_objective (self, trial):
        max_features = trial.suggest_categorical('rf_max_features', RFOptimizer.MAX_FEATURES)
        max_depth = trial.suggest_int('rf_max_depth', RFOptimizer.MAX_DEPTHS[0], RFOptimizer.MAX_DEPTHS[1])
        max_samples = trial.suggest_float('rf_max_samples', RFOptimizer.MAX_SAMPLES[0], RFOptimizer.MAX_SAMPLES[1])
        num_estimators = trial.suggest_categorical('rf_num_estimators', RFOptimizer.NUM_ESTIMATORS)
        split_criterion = trial.suggest_categorical('rf_split_criterion', RFOptimizer.SPLIT_CRITERIA)

        rf_clf = RandomForestClassifier(max_features=max_features, random_state=self.__random_seed, class_weight=self.__train_class_weights, max_depth=max_depth, n_estimators=num_estimators, criterion=split_criterion, max_samples=max_samples, n_jobs=-1)
        print(rf_clf.get_params())
        rf_clf = rf_clf.fit(self.__train_X, self.__train_y)

        y_pred_dev = rf_clf.predict(self.__dev_X)
        y_pred_prob_dev = rf_clf.predict_proba(self.__dev_X)

        if self.__objective_metric == OptunaModelMetric.BALANCED_ACCURACY:
            metric = sk_metrics.balanced_accuracy_score(self.__dev_y, y_pred_dev)

        elif self.__objective_metric == OptunaModelMetric.ROC_AUC:
            if len(y_pred_prob_dev[0]) > 2:
                metric = sk_metrics.roc_auc_score(self.__dev_y.values, y_pred_prob_dev, multi_class='ovr', average='weighted')
            elif len(y_pred_prob_dev[0]) == 2:
                metric = sk_metrics.roc_auc_score(self.__dev_y.values, y_pred_prob_dev[:, 1], average='weighted')
        return metric


    def tune_rf (self, num_trials=1000):
        study = optuna.create_study(direction='maximize')
        study.optimize(self._rf_objective, n_trials=num_trials)
        all_trials = study.get_trials()

        #TODO parse trials in a meaningful way
        '''
        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['SMA'])
            values.append(trial.value)

        self.optuna_trials['SMA'] = dict()
        self.optuna_trials['SMA']['params'] = params
        self.optuna_trials['SMA']['values'] = values
        '''
        self.optimal_params = study.best_params
