'''
    @author: Fadi 'CTO | Maestro | gifs troll' Hariri
    @date: 2019-08-19
    @description: api function calls -> pandas df covering:
            1) trade data
            2) candles (bars)
            3) candles (Extended)
            4) bulk download from google storage (Admin access required !)
'''

import datetime, requests as rq
try:
    from yapic import json
except Exception as e:
    print("required dependency:     sudo pip3 install yapic.json ")

try:
    from ciso8601 import parse_datetime as parse
except Exception as e:
    print("required dependency:     sudo pip3 install ciso8601 ")

import gzip, math, pandas as pd, warnings
from google.cloud import storage
import traceback, typing

try:
    from tqdm import tqdm
except Exception as e:
    print("required dependency:     sudo pip3 install tqdm ")

api_base_url="https://us-central1-consiliumcrypto-master.cloudfunctions.net/api"
ERRORS=[]

def map_globalasset_localasset(exchange:str, globalasset:str)->object:
    try:
        client = storage.Client()
        bucket=client.get_bucket("consilium-exchanges-metadata")
        blob=bucket.blob("consilium-asset-standard.json")
        res = json.loads(gzip.decompress(blob.download_as_string()).decode("utf-8"))
        return res[globalasset.upper()][exchange.upper()]
    except Exception as e:
        print("%s error has occured caused by: %s" % (e.__class__.__name__, str(e)))
        return None

def get_global_asset_list (api_key:str)->dict:
    i=5
    while i>=0:
        try:
            i-=1
            response = rq.get("https://us-central1-consiliumcrypto-master.cloudfunctions.net/api?api-key=%s&type=assets" % api_key)
            status_code, result = response.status_code, response.json()
            if status_code != 200:
                continue
            return result
        except Exception as e:
            pass
    raise Exception("Error occured whilst retrieving global_asset_list. PLease contact Fadi 'CTO | Maestro | gifs troll' Hariri' for remedies")



def get_tradedata(exchange:str, asset:str, date_from:str, date_to:str, path_to_creds:str=None)->pd.DataFrame:
    """
    Will return tradedata using query parameters as pandas dataframe
    :param exchange: valid exchange
    :param asset: valid asset name (global asset),
    :param date_from: string date 'Y-m-dTHH:MM:SS'
    :param date_to: string date 'Y-m-dTHH:MM:SS'
    :param path_to_creds: path json credentials for google service account
    :return: pandas dataframe for trades
    """

    data=[]
    if date_to < date_from:
        raise Exception ("Date_to < Date_from")
    if path_to_creds is not None:
        assert type(path_to_creds) is str and path_to_creds != ""

    #gs=storage.Client() if path_to_creds is None else storage.Client().from_service_account_json(path_to_creds)
    gs = storage.Client() if path_to_creds is None else storage.Client.from_service_account_json(path_to_creds)
    bucket=gs.get_bucket("consilium-marketdata-%s" % exchange.lower())
    #asset=map_globalasset_localasset(exchange, asset)

    if not bucket.exists():
        raise RuntimeError("requested exchange is not supported by consilium")
    #if asset is None:
    #    raise RuntimeError("global mapping not found for requesed pair")

    try:
        date_from=date_from.replace(" ", "T").split(":")[0]
        date_to = date_to.replace(" ", "T").split(":")[0]

        start=datetime.datetime.strptime(datetime.datetime.strftime(parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0], '%Y-%m-%dT%H')
        end=datetime.datetime.strptime(datetime.datetime.strftime(parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0], '%Y-%m-%dT%H')

        dstart=datetime.datetime.strptime(date_from.split("T")[0], '%Y-%m-%d')
        dend = datetime.datetime.strptime(date_to.split("T")[0], '%Y-%m-%d')

        rng=int(math.ceil((dend-dstart).days+1))
        interval=[start+datetime.timedelta(days=i) for i in range(0, rng)]
    except Exception as e:
        raise Exception("Error in date format...please follow the instructions properly and do not be fancy... date format: YYYY-MM-DD HH:MM:SS")

    try:
        for i in tqdm(interval):
            day = str(i).replace(" ", "T").split("T")[0].replace("-", "_")
            blob_name = "%s/%s.json" % (day.replace("-", "_"), asset)
            blob = bucket.blob(blob_name)
            if not blob.exists(): warnings.warn("skipping %s/%s ... blob doesnt exist" % (bucket.name, blob_name))
            result = json.loads(gzip.decompress(blob.download_as_string()).decode("utf-8"), parse_date=False)[asset]

            if 'warning' in result:
                warnings.warn(result['warning'])

            for k, trades in result.items():
                data.extend(trades)
    except Exception as e:
        raise Exception("Error parsing request caused by: %s" % e.__class__.__name__)

    try:
        keys = ['asset_pair', 'trade_id', 'amount', 'side', 'price', 'buyorder_id', 'sellorder_id',
                'exchange_timestamp', 'newyork_adjusted_timestamp']
        d = [(row['asset_pair'], row['trade_id'], row['amount'], row['side'], row['price'],row['buyorder_id'], row['sellorder_id'],
              parse(str(row['exchange_timestamp'])).replace(tzinfo=None), parse(str(row['newyork_adjusted_timestamp'])).replace(tzinfo=None)) for row in data]
        df = pd.DataFrame(d, columns=keys)
        df.set_index('newyork_adjusted_timestamp', inplace=True)
        pd.to_datetime(df.index)
        df.sort_index(inplace=True)
        df['newyork_adjusted_timestamp']=df.index
        df['price'] = list(map(float, df['price']))
        df['amount'] = list(map(float, df['amount']))

        df = df.loc[df.index >= start]
        df = df.loc[df.index < end]

    except Exception as e:
        raise Exception("Failed to create dataframe caused by: %s" % e.__class__.__name__)

    return df

def __get_trade_candles (exchange:str, asset:str, date_from:str, date_to:str, candle_type='bars', path_to_creds:str=None)->pd.DataFrame:
    data = []
    if date_to < date_from:
        raise Exception("Date_to < Date_from")
    if path_to_creds is None:
        gs=storage.Client()
    else:
        print("using creds")
        #gs=storage.Client().from_service_account_json(path_to_creds)
        gs = storage.Client.from_service_account_json(path_to_creds)
    bucket=gs.get_bucket("consilium-candles-trades-%s" % exchange.lower())
    #asset=map_globalasset_localasset(exchange, asset)
    #if asset is None:
    #    raise Exception("global mapping not found for requesed pair")
    try:
        dfrom = datetime.datetime.strftime(parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split(":")[0]
        dto = datetime.datetime.strftime(parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0]

        start = datetime.datetime.strptime(datetime.datetime.strftime(parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split("T")[0], '%Y-%m-%d')
        end = datetime.datetime.strptime(datetime.datetime.strftime(parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split("T")[0], '%Y-%m-%d')

        rng = int(math.ceil(((end - start).total_seconds() / (3600 * 24)) + 1))
        interval = [start + datetime.timedelta(days=i) for i in range(0, rng)]
    except Exception as e:
        raise Exception(
            "Error in date format...please follow the instructions properly and do not be fancy... date format: YYYY-MM-DD HH:MM:SS")
    try:
        for i in tqdm(interval):
            day = str(i).replace(" ", "T").split("T")[0].replace("-", "_")
            blob=bucket.blob("%s/1min-bars%s/%s.json" % (day,'-extended' if candle_type == 'bars-extended' else '',asset.upper()))
            if not blob.exists():
                warnings.warn("No data available for %s on %s" % (asset, str(i)))
                continue
            res=json.loads(gzip.decompress(blob.download_as_string()).decode("utf-8"), parse_date=False)

            pair = list(res.keys())[0]
            for time, item in res[pair].items():
                item['time_period_start'] = time
                data.append(item)

    except Exception as e:
        raise Exception("Error parsing request caused by: %s" % e.__class__.__name__)

    try:
        if candle_type == 'bars':
            keys = ['open', 'high', 'low', 'close', 'volume', 'time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'],
                  parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            # df = df.loc[~df.index.duplicated(keep='first')]
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df.columns = ['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded', 'time_period_start']

        elif candle_type =='bars-extended':
            keys = ['open', 'high', 'low', 'close', 'volume',
                    'count_trades', 'count_buys', 'count_sells', 'volume_buys', 'volume_sells','time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'],
                  row['count_trades'], row['count_buys'], row['count_sells'], row['volume_buys'], row['volume_sells'],
                  parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df['count_trades'] = list(map(float, df['count_trades']))
            df['count_buys'] = list(map(float, df['count_buys']))
            df['count_sells'] = list(map(float, df['count_sells']))
            df['volume_buys'] = list(map(float, df['volume_buys']))
            df['volume_sells'] = list(map(float, df['volume_sells']))
            df.columns = ['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded','count_trades', 'count_buys',
                          'count_sells', 'volume_buys', 'volume_sells', 'time_period_start']

        df = df.loc[df.index >= datetime.datetime.strptime(dfrom, '%Y-%m-%dT%H')]
        df = df.loc[df.index < datetime.datetime.strptime(dto, '%Y-%m-%dT%H')]

    except Exception as e:
        raise Exception("Failed to create dataframe caused by: %s" % e.__class__.__name__)

    return df

def resample(cds:pd.DataFrame, resolution:str, candle_type:str="bars", offset:datetime.timedelta=None)->pd.DataFrame:
    if candle_type == 'bars':
        df = cds.resample(resolution, loffset=offset).agg({'price_open': 'first', 'price_close': 'last',
                                                           'price_high': 'max', 'price_low': 'min',
                                                           'volume_traded': 'sum'}).ffill()  # fill values from last row
    elif candle_type == 'extended':
        df = cds.resample(resolution, loffset=offset).agg({'price_open': 'first', 'price_close': 'last',
                                                           'price_high': 'max', 'price_low': 'min',
                                                           'volume_traded': 'sum', 'count_trades': 'sum', 'count_buys': 'sum',
                                                           'count_sells': 'sum', 'volume_buys': 'sum',
                                                           'volume_sells': 'sum'}).ffill()  # fill values from last row
    #df['time_period_start'] = df.index
    return df

def get_trade_candles(exchange:str, asset:str, date_from:str, date_to:str, candle_type='bars', resolution='1min',
                            path_to_creds:str=None, offset:datetime.timedelta=None)->pd.DataFrame:
    if path_to_creds is not None:
        assert type(path_to_creds) is str and path_to_creds != ""
    cds=__get_trade_candles(exchange, asset, date_from, date_to, candle_type, path_to_creds)
    if resolution == '1min':
        if offset is None:
            return cds
        else:
            return resample(cds, resolution, candle_type, offset)
    else:
        # resample
        return resample(cds, resolution, candle_type, offset)



def generate_extra_candles_params (df:pd.DataFrame, bars='1min') -> pd.DataFrame:
    # load data in pandas df
    df['amount'] = list(map(float, df['amount']))
    df['price'] = list(map(float, df['price']))

    tempcd = df.copy()
    tempcd = tempcd.drop(['asset_pair', 'trade_id', 'buyorder_id', 'sellorder_id',
                          'exchange_timestamp', 'newyork_adjusted_timestamp'], axis=1)

    tempcd = tempcd.resample('1min').agg({'price': 'ohlc', 'amount': 'sum'}).ffill()  # fill values from last row
    tempcd.columns = ['open', 'high', 'low', 'close', 'volume']
    tempcd.index.names=['time']

    # add time and continue with extended attributes
    df['time'] = list(
        map(lambda x: datetime.datetime.strftime(x, '%Y-%m-%d %H:%M:00'), df['newyork_adjusted_timestamp']))
    df = df.drop(['asset_pair', 'trade_id', 'buyorder_id', 'sellorder_id',
                  'exchange_timestamp'], axis=1)

    # count total trades
    tt = df.copy()
    tt['pv'] = tt['price'] * tt['amount']
    tt=tt.groupby(['time']).agg({'amount': 'count', 'pv' : 'sum'})

    # count buys/sells
    df2 = df.copy().groupby(['time', 'side']).agg({'price': 'count', 'amount': 'sum'}).fillna(0).unstack(-1)


    # merge
    tt['count_trades'] = tt['amount']
    tt['count_buys'] = df2['price']['B'] if 'B' in df2['price'] else [0] * len(df2.index)
    tt['count_sells'] = df2['price']['S'] if 'S' in df2['price'] else [0] * len(df2.index)
    tt['volume_buys'] = df2['amount']['B'] if 'B' in df2['amount'] else [0] * len(df2.index)
    tt['volume_sells'] = df2['amount']['S'] if 'S' in df2['amount'] else [0] * len(df2.index)
    tt = tt.drop(['amount'], axis=1).fillna(0)

    cd=tempcd.join(tt)
    cd['vwap']=cd['pv']/cd['volume']
    cd=cd.drop(['pv'], axis=1).fillna(0)
    if bars != '1min':
        cd = cd.resample(bars).agg({'open': 'first', 'close': 'last','high': 'max', 'low': 'min',
                                    'volume': 'sum', 'count_trades': 'sum', 'count_buys': 'sum',
                                    'count_sells': 'sum', 'volume_buys': 'sum', 'volume_sells': 'sum', 'vwap':'sum'}).ffill()
    return cd


def read_data_file (path: str, exchange:str, asset:str, resolution:str)->typing.Optional[pd.DataFrame]:
    try:
        return pd.read_pickle("%s/apidata/%s_%s_%s.pkl" % (path, exchange.upper(), asset.upper(), resolution))
    except Exception as e:
        raise Exception("Failed to read file caused by: %s -- %s" % (e.__class__.__name__, str(e)))


def get_lending_data(key: str, exchange:str, currency: str, day: datetime.datetime, lending_type: str)->typing.Union[pd.DataFrame, typing.Dict]:
    url = "https://us-central1-consiliumcrypto-master.cloudfunctions.net/lending?key=%s&type=%s&exchange=%s&currency=%s&day=%s" % \
          (key, lending_type,exchange,currency, str(day))
    try:
        res=rq.get(url)
        if res.status_code != 200:
            return json.loads(res.json())
        else:
            message=json.loads(gzip.decompress(res.content).decode(), parse_date=False)[currency]
            cols={'loans' :['currency', 'funding_id', 'side', 'funding_class', 'amount', 'rate', 'duration','timestamp_utc', 'timestamp_est'] ,
                  'averages' :['currency', 'rate', 'amount_lent', 'amount_used','timestamp_utc', 'timestamp_est']}
            keys = cols[lending_type]

            d = [[row[k] for k in keys] for row in message]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('timestamp_utc', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            return df
    except Exception as e:
        raise ConnectionError("%s" % str(e))



if __name__ == '__main__':
    MY_API_KEY = 'cqzsjksnF1uBXfghhUZKhkwehRMrPU3DL2QAJ9KXaus'  # THIS IS YOUR ASSIGNED KEY
    path_to_creds="/Users/austin/Documents/consilium/bitbucket/austin-alphafactory/credentials/cc-master-storage-restricted-readonly.json" # <--- put the path to the json key here

    if MY_API_KEY is None or MY_API_KEY == '':
        raise Exception("Missing API Key, Please fill this field and try again")
    if path_to_creds is not None and path_to_creds == "":
        raise Exception("path_to_creds is either None or a valid string, Please fill this field and try again")

    # example candles with offset
    SAMPLING_PERIOD = '1H'
    datetime_start = '2020-10-01 00:00:00'
    datetime_end = '2020-11-01 00:00:00'
    exchange="binance"
    pair="ETH_BTC"
    offset=datetime.timedelta(minutes=15) # leave None if no offset needed


    print("Testing candles")
    df = get_trade_candles(exchange=exchange, asset=pair, date_from=datetime_start, date_to=datetime_end,
                        candle_type='bars', resolution=SAMPLING_PERIOD, path_to_creds=path_to_creds, offset=offset)

    print(df.head())


    #print("Testing trades")
    #df=get_tradedata(exchange=exchange, asset=pair, date_from=datetime_start, date_to=datetime_end)
    #print(df.head())
