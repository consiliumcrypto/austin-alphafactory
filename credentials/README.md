There should be a file called "sql_db_creds.txt" in this directory with the following format:

host
user
password
port
ssl-ca filepath
ssl-key filepath
ssl-cert filepath


There should also be a file called "consilium_api_key.txt" in this directory with the following format:

api_key
