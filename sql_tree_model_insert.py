from src.DatabaseManager import *
from src.cc_ts_utils import *

# Establish connection to database
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
'database': sql_db_creds[7],
'ssl': {'ca': sql_db_creds[4],
'key': sql_db_creds[5],
'cert': sql_db_creds[6], 'check_hostname': False},
'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'sql_model_insert.py', use_dict_cursor=True)

sql = "SELECT bucket_name_gs FROM datasets WHERE TRUE LIMIT 1"
BUCKET_NAME_GS = db_manager.fetch_mysql_result(sql)[0]['bucket_name_gs']

TESTING = False

if TESTING:
    DATASET_IDS = range(1,6)
    MODEL_TYPE = 'CLASSIFIER'
    RANDOM_SEEDS = [1,2,3]
    NORMALIZATIONS = ['MIN_MAX']
    MAX_DEPTHS = [10]
    MAX_FEATURES = ['sqrt']
    MAX_SAMPLES = [0.2]
    NUM_ESTIMATORS = [100]
    PCA_VARIANCE_PERC = [0]
    SPLIT_CRITERIA = ['gini', 'entropy']
    NUM_OPTUNA_MODEL_TRIALS = [10]
    OPTUNA_RF_OBJECTIVES = ['ROC_AUC']

else:
    '''
    DATASET_IDS = range(1,17)
    MODEL_TYPE = 'CLASSIFIER'
    RANDOM_SEEDS = [1,2,3]
    NORMALIZATIONS = ['MIN_MAX', 'Z_SCORE']
    MAX_DEPTHS = [2, 3, 5, 10, 20]
    MAX_FEATURES = ['sqrt', 'log2']
    MAX_SAMPLES = [0.2, 0.5, 1]
    NUM_ESTIMATORS = [100, 1000, 5000]
    PCA_VARIANCE_PERC = [0, 0.8, 0.9, 0.95]
    SPLIT_CRITERIA = ['gini', 'entropy']
    '''

    DATASET_IDS = [1,2,3,4,5,6,7,8,9,10,11,12,15]
    MODEL_TYPE = 'CLASSIFIER'
    RANDOM_SEEDS = [1, 2, 3]
    NORMALIZATIONS = ['MIN_MAX', 'Z_SCORE']
    MAX_DEPTHS = ['OPTUNA']
    MAX_FEATURES = ['OPTUNA']
    MAX_SAMPLES = ['OPTUNA']
    NUM_ESTIMATORS = ['OPTUNA']
    PCA_VARIANCE_PERC = [0, 0.8, 0.9, 0.95]
    SPLIT_CRITERIA = ['OPTUNA']
    NUM_OPTUNA_MODEL_TRIALS = [50]
    OPTUNA_RF_OBJECTIVES = ['ROC_AUC']



description = 'Random forests - Feb 9, 2021'

print ('inserting SQL MODEL')

for id in DATASET_IDS:
    for max_depth in MAX_DEPTHS:
        for max_features in MAX_FEATURES:
            for max_samples in MAX_SAMPLES:
                for num_estimators in NUM_ESTIMATORS:
                    for normalization_type in NORMALIZATIONS:
                        for pca_variance_perc in PCA_VARIANCE_PERC:
                            for random_seed in RANDOM_SEEDS:
                                for split_criterion in SPLIT_CRITERIA:
                                    for num_optuna_model_trials in NUM_OPTUNA_MODEL_TRIALS:
                                        for optuna_rf_obj in OPTUNA_RF_OBJECTIVES:
                                            sql = "INSERT INTO random_forest_models (description, dataset_id, model_status, normalization_type, max_depth, max_features, max_samples, num_estimators, random_seed, bucket_name_gs, model_type, pca_variance_perc, split_criterion, num_optuna_model_trials, optuna_model_objective) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (description, id, 'WAITING', normalization_type, max_depth, max_features, max_samples, num_estimators, random_seed, BUCKET_NAME_GS, MODEL_TYPE, pca_variance_perc, split_criterion, num_optuna_model_trials, optuna_rf_obj)
                                            res = db_manager.update_database(sql)
                                            print(res)