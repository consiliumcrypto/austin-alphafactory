from src.DatabaseManager import *
from src.cc_ts_utils import *


# Establish connection to database
DATABASE_NAME = 'austin_march26_2020'

with open('credentials/sql_db_creds.txt') as f:
    sql_db_creds = f.readlines()
    sql_db_creds = [x.strip() for x in sql_db_creds]

    kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
    'database': DATABASE_NAME,
    'ssl': {'ca': sql_db_creds[4],
    'key': sql_db_creds[5],
    'cert': sql_db_creds[6], 'check_hostname': False},
    'port': int(sql_db_creds[3])}

    db_manager=DatabaseManager (kwargs, 'sql_model_insert.py', use_dict_cursor=True )


    TESTING = False

    if TESTING:
        DATASET_IDS = [5]
        MODEL_TYPE = 'CLASSIFIER'
        NUM_STEPS = [5, 15]
        BATCH_SIZES = [32]
        MAX_EPOCHS = 400
        EARLY_STOP_PATIENCE = 75
        EARLY_STOP_DELTA = 0
        RANDOM_SEEDS = [1,2,3]
        LOSS_TYPE = 'categorical_crossentropy'
        OPTIMIZER = 'adam'
        BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'



    else:
        #TODO: Ping database for all dataset IDs
        #DATASET_IDS = [x for x in range(1,9)]
        DATASET_IDS = [1]
        MODEL_TYPE = 'CLASSIFIER'
        NUM_STEPS = [5, 15, 60, 100]
        BATCH_SIZES = [32]
        MAX_EPOCHS = 400
        EARLY_STOP_PATIENCE = 75
        EARLY_STOP_DELTA = 0
        RANDOM_SEEDS = [7]
        LOSS_TYPE = 'categorical_crossentropy'
        OPTIMIZER = 'adam'
        BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'

    description = '50/100/50 Bidirectional LSTM, 20% dropout, 0.003 threshold momentum labeling'

    print ('inserting SQL MODEL')

    for id in DATASET_IDS:
        for steps in NUM_STEPS:
            for batch_size in BATCH_SIZES:
                for random_seed in RANDOM_SEEDS:
                    sql = "INSERT INTO models (description, loss_type, optimizer, max_epochs, batch_size, dataset_id, model_status, num_steps, random_seed, early_stop_patience, early_stop_delta, bucket_name_gs, model_type) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (description, LOSS_TYPE, OPTIMIZER, MAX_EPOCHS, batch_size, id, 'WAITING', steps, random_seed, EARLY_STOP_PATIENCE, EARLY_STOP_DELTA, BUCKET_NAME_GS, MODEL_TYPE)
                    print(db_manager.update_database(sql))
