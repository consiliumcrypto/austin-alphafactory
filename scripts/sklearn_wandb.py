import sklearn.metrics as sk_metrics
import wandb
from sklearn.datasets import load_breast_cancer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from wandb.sklearn import plot_feature_importances, plot_learning_curve, plot_confusion_matrix, plot_summary_metrics, plot_class_proportions, plot_roc, plot_precision_recall

# load data
dataset = load_breast_cancer()
X = dataset.data
y = dataset.target
labels = dataset.target_names

# shuffle data
X, y = shuffle(X, y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5)

# create and fit the model for each hyperparameter configuration
for n_estimators in [100, 200]:
    for min_samples_split in [2, 3]:
        
        # create a config associated with each run
        config = {
            'n_estimators': n_estimators,
            'min_samples_split': min_samples_split,
        }

        # initialize a wandb run
        wandb.init(
            project='sklearn',
            config=config,
            reinit=True,
        )

        # fit the model
        model = RandomForestClassifier(**config)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        y_probas = model.predict_proba(X_test)

        # add sklearn metrics
        metrics = {
            "val/accuracy": sk_metrics.accuracy_score(y_test, y_pred),
            "val/prec": sk_metrics.precision_score(y_test, y_pred, average='weighted', zero_division=0),
            "val/rec": sk_metrics.recall_score(y_test, y_pred, average='weighted', zero_division=0),
            "val/f1": sk_metrics.f1_score(y_test, y_pred, average='weighted', zero_division=0),
        }
        for i, label in enumerate(labels):
            metrics.update({
                f"val/{label}/accuracy": sk_metrics.accuracy_score(y_test[y_test == i], y_pred[y_test == i]),
                f"val/{label}/prec": sk_metrics.precision_score(y_test, y_pred, average='weighted', labels=[i], zero_division=0),
                f"val/{label}/rec": sk_metrics.recall_score(y_test, y_pred, average='weighted', labels=[i], zero_division=0),
                f"val/{label}/f1": sk_metrics.f1_score(y_test, y_pred, average='weighted', labels=[i], zero_division=0),
            })
        wandb.log(metrics)

        # add sklearn summary plots
        plot_feature_importances(model, list(dataset.feature_names))
        print("Logged feature importances.")
        plot_learning_curve(model, X_train, y_train)
        print("Logged learning curve.")
        plot_confusion_matrix(y_test, y_pred, labels)
        print("Logged confusion matrix.")
        plot_summary_metrics(model, X=X_train, y=y_train, X_test=X_test, y_test=y_test)
        print("Logged summary metrics.")
        plot_class_proportions(y_train, y_test, labels)
        print("Logged class proportions.")
        plot_roc(y_test, y_probas, labels)
        print("Logged roc curve.")
        plot_precision_recall(y_test, y_probas, labels)
        print("Logged precision recall curve.")




'''
# Model training here

# Log classifier visualizations
wandb.sklearn.plot_classifier(clf, X_train, X_test, y_train, y_test, y_pred, y_probas, labels, model_name='SVC', feature_names=None)

# Log regression visualizations
wandb.sklearn.plot_regressor(reg, X_train, X_test, y_train, y_test,  model_name='Ridge')

# Log clustering visualizations
wandb.sklearn.plot_clusterer(kmeans, X_train, cluster_labels, labels=None, model_name='KMeans')
  

'''