from src.cc_ts_utils import *
from src.GoogleStorageClient import *
from src.DatabaseManager import *


'''
*** 1. Get list of all model files in GS ***
2. Loop through list, extracting model_id
3. Select 'model_file_gs' field of that model in sql
4. Check if model filename from GS matches 'model_filename_gs' in SQL
5. If not, set 'model_filename_gs' to the model filename from GS

'''


BUCKET_NAME = 'consilium-austin-data'


google_storage_obj = GoogleStorageClient(BUCKET_NAME)

#res = list_blobs_with_prefix(BUCKET_NAME, 'models')

all_model_filenames = google_storage_obj.list_blobs(prefix='predictions')
all_model_filenames = all_model_filenames[1:] #Drop first element in list (folder name, not a file)

#model_id_256_dataset_id_13.h5

for m_filename in all_model_filenames:
    print ('Deleting  ' + m_filename)
    google_storage_obj.delete_blob(m_filename)
