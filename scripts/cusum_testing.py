import pandas as pd
import plotly
import plotly.graph_objects as go

from mlfinlab.filters import cusum_filter

CUSUM_THRESHOLDS = [0.01, 0.02, 0.05]


df = pd.read_csv('../data/alpha-factory-vlad-140529794743744.csv', parse_dates=True, index_col=0, header=0)

target = df['target']
features_df = df.drop(['target'], axis=1)

fig = go.Figure()

price_trace = go.Scatter(
    x = features_df.index,
    y = features_df['price_close'],
    name = 'Close Price'
)

fig.add_trace(price_trace)

for cusum_thresh in CUSUM_THRESHOLDS:
    cusum_events = cusum_filter(features_df['price_close'], threshold=cusum_thresh)

    filtered_events = features_df.loc[cusum_events]
    print(cusum_thresh)
    print(len(filtered_events))
    print('#########')
    cusum_trace = go.Scatter(
        x=filtered_events.index,
        y=filtered_events['price_close'],
        mode='markers',
        name='CUSUM Events - ' + str(cusum_thresh)
    )

    fig.add_trace(cusum_trace)


plotly.offline.plot(fig, auto_open=False, filename='cusum-test-new.html')