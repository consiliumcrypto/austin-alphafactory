from enum_classes import *
import pandas as pd
import numpy as np
import talib
from cc_ts_utils import MinMaxNormalizer
import os
import csv
import optuna
from sklearn.feature_selection import mutual_info_classif

#NOTE: Currently only supports time-sampled candles
class TechnicalsOptimizer:
    SMA_RANGE = [2, 150]
    EMA_RANGE = [2, 150]
    MOM_RANGE = [2, 150]
    RSI_RANGE = [2, 150]
    ADX_RANGE = [2, 150]
    ATR_RANGE = [2, 150]
    CCI_RANGE = [2, 150]
    SAR_RANGE = [0.01, 0.4]
    AROONOSC_RANGE = [2, 150]
    MFI_RANGE = [2, 150]
    DX_RANGE = [2, 150]
    WILLR_RANGE = [2, 150]
    WMA_RANGE = [2, 150]

    def __init__(self, df,high_col='price_high', low_col='price_low', close_col='price_close', volume_col='volume_traded'):
        self.__raw_df = df.copy(deep=True)
        self.__target_df = self.__raw_df[self.__raw_df.columns.values[-1]]

        self.__high = self.__raw_df[close_col].values
        self.__low = self.__raw_df[low_col].values
        self.__close = self.__raw_df[close_col].values
        self.__volume = self.__raw_df[volume_col].values

        self.optimal_params = dict()
        self.optuna_trials = dict()

    def tune_all (self):
        self.tune_sma()
        self.tune_ema()
        self.tune_mom()
        self.tune_rsi()
        self.tune_adx()
        self.tune_atr()
        self.tune_cci()
        self.tune_sar()
        self.tune_aroonosc()
        self.tune_mfi()
        self.tune_dx()
        self.tune_willr()
        self.tune_wma()


    def _sma_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('SMA', TechnicalsOptimizer.SMA_RANGE[0], TechnicalsOptimizer.SMA_RANGE[1])
        temp_df['SMA_' + str(int(x))] = talib.SMA(self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['SMA_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _ema_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('EMA', TechnicalsOptimizer.EMA_RANGE[0], TechnicalsOptimizer.EMA_RANGE[1])
        temp_df['EMA_' + str(int(x))] = talib.EMA(self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['EMA_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _mom_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('MOM', TechnicalsOptimizer.MOM_RANGE[0], TechnicalsOptimizer.MOM_RANGE[1])
        temp_df['MOM_' + str(int(x))] = talib.MOM(self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['MOM_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _rsi_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('RSI', TechnicalsOptimizer.RSI_RANGE[0], TechnicalsOptimizer.RSI_RANGE[1])
        temp_df['RSI_' + str(int(x))] = talib.RSI(self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['RSI_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _adx_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('ADX', TechnicalsOptimizer.ADX_RANGE[0], TechnicalsOptimizer.ADX_RANGE[1])
        temp_df['ADX_' + str(int(x))] = talib.ADX(self.__high, self.__low, self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['ADX_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi

    def _atr_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('ATR', TechnicalsOptimizer.ATR_RANGE[0], TechnicalsOptimizer.ATR_RANGE[1])
        temp_df['ATR_' + str(int(x))] = talib.ATR(self.__high, self.__low, self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['ATR_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _cci_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('CCI', TechnicalsOptimizer.CCI_RANGE[0], TechnicalsOptimizer.CCI_RANGE[1])
        temp_df['CCI_' + str(int(x))] = talib.CCI(self.__high, self.__low, self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['CCI_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi

    #PSAR
    def _sar_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_float('SAR', TechnicalsOptimizer.SAR_RANGE[0], TechnicalsOptimizer.SAR_RANGE[1])
        temp_df['SAR_' + str(x)] = talib.SAR(self.__high, self.__low, acceleration=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['SAR_' + str(x)].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _aroonosc_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('AROONOSC', TechnicalsOptimizer.AROONOSC_RANGE[0], TechnicalsOptimizer.AROONOSC_RANGE[1])
        temp_df['AROONOSC_' + str(int(x))] = talib.AROONOSC(self.__high, self.__low, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['AROONOSC_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def _mfi_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('MFI', TechnicalsOptimizer.MFI_RANGE[0], TechnicalsOptimizer.MFI_RANGE[1])
        temp_df['MFI_' + str(int(x))] = talib.MFI(self.__high, self.__low, self.__close, self.__volume, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['MFI_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi

    #Directional Movement Index
    def _dx_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('DX', TechnicalsOptimizer.DX_RANGE[0], TechnicalsOptimizer.DX_RANGE[1])
        temp_df['DX_' + str(int(x))] = talib.DX(self.__high, self.__low, self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['DX_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi

    def _willr_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('WILLR', TechnicalsOptimizer.WILLR_RANGE[0], TechnicalsOptimizer.WILLR_RANGE[1])
        temp_df['WILLR_' + str(int(x))] = talib.WILLR(self.__high, self.__low, self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['WILLR_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi

    def _wma_objective (self, trial):
        temp_df = self.__raw_df.copy()
        x = trial.suggest_int('WMA', TechnicalsOptimizer.WMA_RANGE[0], TechnicalsOptimizer.WMA_RANGE[1])
        temp_df['WMA_' + str(int(x))] = talib.WMA(self.__close, timeperiod=x)
        temp_df['target'] = self.__target_df
        temp_df.dropna(inplace=True)
        mi = mutual_info_classif(temp_df['WMA_' + str(int(x))].values.reshape(-1, 1), temp_df['target'])

        return mi


    def tune_sma (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._sma_objective, n_trials=(int(TechnicalsOptimizer.SMA_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['SMA'])
            values.append(trial.value)

        self.optuna_trials['SMA'] = dict()
        self.optuna_trials['SMA']['params'] = params
        self.optuna_trials['SMA']['values'] = values

        self.optimal_params['SMA'] = int(study.best_params['SMA'])


    def tune_ema (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._ema_objective, n_trials=(int(TechnicalsOptimizer.EMA_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['EMA'])
            values.append(trial.value)

        self.optuna_trials['EMA'] = dict()
        self.optuna_trials['EMA']['params'] = params
        self.optuna_trials['EMA']['values'] = values

        self.optimal_params['EMA'] = int(study.best_params['EMA'])


    def tune_mom (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._mom_objective, n_trials=(int(TechnicalsOptimizer.MOM_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['MOM'])
            values.append(trial.value)

        self.optuna_trials['MOM'] = dict()
        self.optuna_trials['MOM']['params'] = params
        self.optuna_trials['MOM']['values'] = values

        self.optimal_params['MOM'] = int(study.best_params['MOM'])

    def tune_rsi (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._rsi_objective, n_trials=(int(TechnicalsOptimizer.RSI_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['RSI'])
            values.append(trial.value)

        self.optuna_trials['RSI'] = dict()
        self.optuna_trials['RSI']['params'] = params
        self.optuna_trials['RSI']['values'] = values

        self.optimal_params['RSI'] = int(study.best_params['RSI'])

    def tune_adx (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._adx_objective, n_trials=(int(TechnicalsOptimizer.ADX_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['ADX'])
            values.append(trial.value)

        self.optuna_trials['ADX'] = dict()
        self.optuna_trials['ADX']['params'] = params
        self.optuna_trials['ADX']['values'] = values

        self.optimal_params['ADX'] = int(study.best_params['ADX'])

    def tune_atr (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._atr_objective, n_trials=(int(TechnicalsOptimizer.ATR_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['ATR'])
            values.append(trial.value)

        self.optuna_trials['ATR'] = dict()
        self.optuna_trials['ATR']['params'] = params
        self.optuna_trials['ATR']['values'] = values

        self.optimal_params['ATR'] = int(study.best_params['ATR'])


    def tune_cci (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._cci_objective, n_trials=(int(TechnicalsOptimizer.CCI_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['CCI'])
            values.append(trial.value)

        self.optuna_trials['CCI'] = dict()
        self.optuna_trials['CCI']['params'] = params
        self.optuna_trials['CCI']['values'] = values

        self.optimal_params['CCI'] = int(study.best_params['CCI'])


    def tune_sar (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._sar_objective, n_trials=(int(TechnicalsOptimizer.SAR_RANGE[1]/TechnicalsOptimizer.SAR_RANGE[0])*2))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['SAR'])
            values.append(trial.value)

        self.optuna_trials['SAR'] = dict()
        self.optuna_trials['SAR']['params'] = params
        self.optuna_trials['SAR']['values'] = values

        self.optimal_params['SAR'] = study.best_params['SAR']


    def tune_aroonosc (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._aroonosc_objective, n_trials=(int(TechnicalsOptimizer.AROONOSC_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['AROONOSC'])
            values.append(trial.value)

        self.optuna_trials['AROONOSC'] = dict()
        self.optuna_trials['AROONOSC']['params'] = params
        self.optuna_trials['AROONOSC']['values'] = values

        self.optimal_params['AROONOSC'] = int(study.best_params['AROONOSC'])


    def tune_mfi (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._mfi_objective, n_trials=(int(TechnicalsOptimizer.MFI_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['MFI'])
            values.append(trial.value)

        self.optuna_trials['MFI'] = dict()
        self.optuna_trials['MFI']['params'] = params
        self.optuna_trials['MFI']['values'] = values

        self.optimal_params['MFI'] = int(study.best_params['MFI'])


    def tune_dx (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._dx_objective, n_trials=(int(TechnicalsOptimizer.DX_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['DX'])
            values.append(trial.value)

        self.optuna_trials['DX'] = dict()
        self.optuna_trials['DX']['params'] = params
        self.optuna_trials['DX']['values'] = values

        self.optimal_params['DX'] = int(study.best_params['DX'])

    def tune_willr (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._willr_objective, n_trials=(int(TechnicalsOptimizer.DX_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['WILLR'])
            values.append(trial.value)

        self.optuna_trials['WILLR'] = dict()
        self.optuna_trials['WILLR']['params'] = params
        self.optuna_trials['WILLR']['values'] = values

        self.optimal_params['WILLR'] = int(study.best_params['WILLR'])

    def tune_wma (self):
        study = optuna.create_study(direction='maximize')
        optimizer_results = study.optimize(self._wma_objective, n_trials=(int(TechnicalsOptimizer.WMA_RANGE[1]/2)))
        all_trials = study.get_trials()

        params = []
        values = []

        for trial in all_trials:
            params.append(trial.params['WMA'])
            values.append(trial.value)

        self.optuna_trials['WMA'] = dict()
        self.optuna_trials['WMA']['params'] = params
        self.optuna_trials['WMA']['values'] = values

        self.optimal_params['WMA'] = int(study.best_params['WMA'])
