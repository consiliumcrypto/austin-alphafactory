from enum import Enum

class TargetType (Enum):
    PRICE = 'PRICE'
    VOLUME = 'VOLUME'
    ABS_RETURN = 'ABSOLUTE_RETURN'
    PERC_RETURN = 'PERCENT_RETURN'
    VOLATILITY = 'VOLATILITY'
    BINARY_MOMENTUM = 'BINARY_MOMENTUM'
    TERNARY_MOMENTUM = 'TERNARY_MOMENTUM'

    @classmethod
    def target_type_to_target_col_name(cls, target_type):
        #Mapping of "target_type" to "target_col_name"
        target_type_to_target_col_name_dict = dict()
        target_type_to_target_col_name_dict[cls.PRICE] = 'target_price'
        target_type_to_target_col_name_dict[cls.ABS_RETURN] = 'target_abs_return'
        target_type_to_target_col_name_dict[cls.PERC_RETURN] = 'target_perc_return'
        target_type_to_target_col_name_dict[cls.VOLATILITY] = 'target_volatility'
        target_type_to_target_col_name_dict[cls.BINARY_MOMENTUM] = 'target_binary_momentum'
        target_type_to_target_col_name_dict[cls.TERNARY_MOMENTUM] = 'target_ternary_momentum'

        return target_type_to_target_col_name_dict[target_type]

class TargetColName (Enum):
    PRICE = 'target_price'
    ABS_RETURN = 'target_abs_return'
    PERC_RETURN = 'target_perc_return'
    VOLATILITY = 'target_volatility'
    BINARY_MOMENTUM = 'target_binary_momentum'
    TERNARY_MOMENTUM = 'target_ternary_momentum'


class SampleType (Enum):
    TIME = 'TIME_BARS'
    VOLUME = 'VOLUME_BARS'
    TICK = 'TICK BARS'
    DOLLAR = 'DOLLAR_BARS'
    VOLUME_IMBALANCE = 'VOLUME_IMBALANCE_BARS'
    TICK_IMBALANCE = 'TICK_IMBALANCE_BARS'
    DOLLAR_IMBALANCE = 'DOLLAR_IMBALANCE_BARS'


class Status (Enum):
    COMPLETE = ''
    RUNNING = ''
    WAITING = ''
    ERROR = ''

class ModelType (Enum):
    REGRESSOR = ''
    CLASSIFIER = ''
    AGENT = ''

class NormalizationType (Enum):
    BATCH_NORM = ''
    SHORT_ORDER = ''

class DatasetSplit (Enum):
    TRAIN = 'TRAIN'
    DEV = 'DEV'
    TEST = 'TEST'
