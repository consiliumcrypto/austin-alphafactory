'''
    @author: Fadi 'CTO | Maestro | gifs troll' Hariri
    @date: 2019-08-19
    @description: api function calls -> pandas df covering:
            1) trade data
            2) candles (bars)
            3) candles (Extended)
            4) bulk download from google storage (Admin access required !)
'''

import datetime, dateutil.parser as dp, requests as rq, json, gzip, math, pandas as pd, warnings
from google.cloud import storage
import typing

api_base_url="https://us-central1-consiliumcrypto-master.cloudfunctions.net/api"


def map_globalasset_localasset(exchange:str, globalasset:str)->object:
    try:
        client = storage.Client()
        bucket=client.get_bucket("consilium-exchanges-metadata")
        blob=bucket.blob("consilium-asset-standard.json")
        res = json.loads(gzip.decompress(blob.download_as_string()).decode("utf-8"))
        return res[globalasset.upper()][exchange.upper()]
    except Exception as e:
        print("%s error has occured caused by: %s" % (e.__class__.__name__, str(e)))
        return None

def get_global_asset_list (api_key:str)->dict:
    i=5
    while i>=0:
        try:
            i-=1
            response = rq.get("https://us-central1-consiliumcrypto-master.cloudfunctions.net/api?api-key=%s&type=assets" % api_key)
            status_code, result = response.status_code, response.json()
            if status_code != 200:
                continue
            return result
        except Exception as e:
            pass
    raise Exception("Error occured whilst retrieving global_asset_list. PLease contact Fadi 'CTO | Maestro | gifs troll' Hariri' for remedies")


def get_tradedata(exchange:str, asset:str, date_from:str, date_to:str, api_key:str)->pd.DataFrame:
    """
    Will return tradedata using query parameters as pandas dataframe
    :param exchange: valid exchange
    :param asset: valid asset name (global asset),
    :param date_from: string date 'Y-m-dTHH:MM:SS'
    :param date_to: string date 'Y-m-dTHH:MM:SS'
    :param api_key: consilium provided api key
    :return: pandas dataframe for trades
    """

    data=[]
    if date_to < date_from:
        raise Exception ("Date_to < Date_from")
    try:
        date_from=date_from.replace(" ", "T").split(":")[0]
        date_to = date_to.replace(" ", "T").split(":")[0]

        start=datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0], '%Y-%m-%dT%H')
        end=datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0], '%Y-%m-%dT%H')

        dstart=datetime.datetime.strptime(date_from.split("T")[0], '%Y-%m-%d')
        dend = datetime.datetime.strptime(date_to.split("T")[0], '%Y-%m-%d')

        rng=int(math.ceil((dend-dstart).days+1))
        interval=[start+datetime.timedelta(days=i) for i in range(0, rng)]
    except Exception as e:
        raise Exception("Error in date format...please follow the instructions properly and do not be fancy... date format: YYYY-MM-DD HH:MM:SS")

    try:
        for i in interval:
            day=str(i).replace(" ", "T").split("T")[0]
            url="%s/trades?api-key=%s&type=marketdata&exchange=%s&asset=%s&date=%s&compressed=true" % (api_base_url, api_key, exchange.lower(), asset.upper(), day)
            res=rq.get(url)
            status_code = res.status_code
            if res.status_code != 200:
                result = res.json()
                warnings.warn("Did not include data for %s. Status code: %s. Message: %s" % (
                str(i), str(status_code), str(result['status'])))
                continue
            else:
                result = json.loads(gzip.decompress(res.content).decode("utf-8"))

            if 'warning' in result:
                warnings.warn(result['warning'])

            for k, trades in result['trades'].items():
                data.extend(trades)
    except Exception as e:
        raise Exception("Error parsing request caused by: %s" % e.__class__.__name__)

    try:
        keys = ['asset_pair', 'trade_id', 'amount', 'side', 'price', 'buyorder_id', 'sellorder_id',
                'exchange_timestamp', 'newyork_adjusted_timestamp']
        d = [(row['asset_pair'], row['trade_id'], row['amount'], row['side'], row['price'],row['buyorder_id'], row['sellorder_id'],
              dp.parse(str(row['exchange_timestamp'])).replace(tzinfo=None), dp.parse(str(row['newyork_adjusted_timestamp'])).replace(tzinfo=None)) for row in data]
        df = pd.DataFrame(d, columns=keys)
        df.set_index('newyork_adjusted_timestamp', inplace=True)
        pd.to_datetime(df.index)
        df.sort_index(inplace=True)
        df['newyork_adjusted_timestamp']=df.index
        df['price'] = list(map(float, df['price']))
        df['amount'] = list(map(float, df['amount']))

        df = df.loc[df.index >= start]
        df = df.loc[df.index < end]

    except Exception as e:
        raise Exception("Failed to create dataframe caused by: %s" % e.__class__.__name__)

    return df


def get_trade_candles(exchange:str, asset:str, date_from:str, date_to:str, api_key:str, candle_type='bars')->pd.DataFrame:
    """
    Will return trade candles using query parameters as pandas dataframe
    :param exchange: valid exchange
    :param asset: valid asset name (global asset),
    :param date_from: string date 'Y-m-dTHH:MM:SS'
    :param date_to: string date 'Y-m-dTHH:MM:SS'
    :param api_key: consilium provided api key
    :param candle_type: bars, ticks, volume
    :return: pandas dataframe for trades
    """


    data = []
    if date_to < date_from:
        raise Exception("Date_to < Date_from")
    try:
        dfrom = datetime.datetime.strftime(dp.parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split(":")[0]
        dto = datetime.datetime.strftime(dp.parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split(":")[0]

        start = datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split( "T")[0], '%Y-%m-%d')
        end = datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split("T")[0], '%Y-%m-%d')

        rng = int(math.ceil(((end - start).total_seconds() / (3600*24)) + 1))
        interval = [start + datetime.timedelta(days=i) for i in range(0, rng)]
    except Exception as e:
        raise Exception(
            "Error in date format...please follow the instructions properly and do not be fancy... date format: YYYY-MM-DD HH:MM:SS")
    try:
        for i in interval:
            day = str(i).replace(" ", "T").split("T")[0]
            url = "%s/candles?api-key=%s&type=trade-candles&exchange=%s&asset=%s&candle=%s&date=%s" % (
            api_base_url, api_key, exchange.lower(), asset.upper(),candle_type, day)
            res = rq.get(url)
            status_code, result = res.status_code, res.json()
            if res.status_code != 200:
                warnings.warn("Did not include data for %s on %s. Status code: %s. Message: %s" % (
                asset, str(i), str(status_code), str(result['status'])))
                continue

            if 'warning' in result:
                warnings.warn("WARNING FOR %s on %s: %s" % (asset, str(i), result['warning']))

            if 'status' in result and result['status'] == 'INVALID_REQUEST':
                warnings.warn("No data available for %s on %s" % (asset, str(i)))
                continue

            pair=list(result['candles'].keys())[0]
            for time, item in result['candles'][pair].items():
                item['time_period_start']=time
                data.append(item)

    except Exception as e:
        raise Exception("Error parsing request caused by: %s" % e.__class__.__name__)

    try:
        if candle_type == 'bars':
            keys = ['open', 'high', 'low', 'close', 'volume', 'time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'], dp.parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df.columns=['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded', 'time_period_start']

        elif candle_type =='bars-extended':
            keys = ['open', 'high', 'low', 'close', 'volume',
                    'count_trades', 'count_buys', 'count_sells', 'volume_buys', 'volume_sells','time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'],
                  row['count_trades'], row['count_buys'], row['count_sells'], row['volume_buys'], row['volume_sells'],
                  dp.parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df['count_trades'] = list(map(float, df['count_trades']))
            df['count_buys'] = list(map(float, df['count_buys']))
            df['count_sells'] = list(map(float, df['count_sells']))
            df['volume_buys'] = list(map(float, df['volume_buys']))
            df['volume_sells'] = list(map(float, df['volume_sells']))
            df.columns = ['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded','count_trades', 'count_buys',
                          'count_sells', 'volume_buys', 'volume_sells', 'time_period_start']


        df = df.loc[df.index >= datetime.datetime.strptime(dfrom, '%Y-%m-%dT%H')]
        df = df.loc[df.index < datetime.datetime.strptime(dto, '%Y-%m-%dT%H')]

    except Exception as e:
        raise Exception("Failed to create dataframe caused by: %s" % e.__class__.__name__)

    return df

def get_trade_candles_google_storage(exchange:str, asset:str, date_from:str, date_to:str, candle_type='bars')->pd.DataFrame:
    data = []
    if date_to < date_from:
        raise Exception("Date_to < Date_from")
    gs=storage.Client()
    bucket=gs.get_bucket("consilium-candles-trades-%s" % exchange.lower())
    asset=map_globalasset_localasset(exchange, asset)
    if asset is None:
        raise Exception("global mapping not found for requesed pair")
    try:
        dfrom = datetime.datetime.strftime(dp.parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split(":")[0]
        dto = datetime.datetime.strftime(dp.parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ", "T").split(":")[0]

        start = datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_from).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split("T")[0], '%Y-%m-%d')
        end = datetime.datetime.strptime(datetime.datetime.strftime(dp.parse(date_to).replace(tzinfo=None), '%Y-%m-%dT%H:00:00').replace(" ","T").split("T")[0], '%Y-%m-%d')

        rng = int(math.ceil(((end - start).total_seconds() / (3600 * 24)) + 1))
        interval = [start + datetime.timedelta(days=i) for i in range(0, rng)]
    except Exception as e:
        raise Exception(
            "Error in date format...please follow the instructions properly and do not be fancy... date format: YYYY-MM-DD HH:MM:SS")
    try:
        for i in interval:
            day = str(i).replace(" ", "T").split("T")[0].replace("-", "_")
            blob=bucket.blob("%s/1min-bars%s/%s.json" % (day,'-extended' if candle_type == 'bars-extended' else '',asset.upper()))
            if not blob.exists():
                continue
            res=json.loads(gzip.decompress(blob.download_as_string()).decode("utf-8"))

            pair = list(res.keys())[0]
            for time, item in res[pair].items():
                item['time_period_start'] = time
                data.append(item)

    except Exception as e:
        raise Exception("Error parsing request caused by: %s" % e.__class__.__name__)

    try:
        if candle_type == 'bars':
            keys = ['open', 'high', 'low', 'close', 'volume', 'time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'],
                  dp.parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            # df = df.loc[~df.index.duplicated(keep='first')]
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df.columns = ['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded', 'time_period_start']

        elif candle_type =='bars-extended':
            keys = ['open', 'high', 'low', 'close', 'volume',
                    'count_trades', 'count_buys', 'count_sells', 'volume_buys', 'volume_sells','time_period_start']
            d = [(row['open'], row['high'], row['low'], row['close'], row['volume'],
                  row['count_trades'], row['count_buys'], row['count_sells'], row['volume_buys'], row['volume_sells'],
                  dp.parse(str(row['time_period_start'])).replace(tzinfo=None)) for row in data]
            df = pd.DataFrame(d, columns=keys)
            df.set_index('time_period_start', inplace=True)
            pd.to_datetime(df.index)
            df.sort_index(inplace=True)
            df['time_period_start'] = df.index
            df['open'] = list(map(float, df['open']))
            df['high'] = list(map(float, df['high']))
            df['low'] = list(map(float, df['low']))
            df['close'] = list(map(float, df['close']))
            df['volume'] = list(map(float, df['volume']))
            df['count_trades'] = list(map(float, df['count_trades']))
            df['count_buys'] = list(map(float, df['count_buys']))
            df['count_sells'] = list(map(float, df['count_sells']))
            df['volume_buys'] = list(map(float, df['volume_buys']))
            df['volume_sells'] = list(map(float, df['volume_sells']))
            df.columns = ['price_open', 'price_high', 'price_low', 'price_close', 'volume_traded','count_trades', 'count_buys',
                          'count_sells', 'volume_buys', 'volume_sells', 'time_period_start']

        df = df.loc[df.index >= datetime.datetime.strptime(dfrom, '%Y-%m-%dT%H')]
        df = df.loc[df.index < datetime.datetime.strptime(dto, '%Y-%m-%dT%H')]

    except Exception as e:
        raise Exception("Failed to create dataframe caused by: %s" % e.__class__.__name__)

    return df

def get_bar_candles_storage(exchange:str, asset:str, date_from:str, date_to:str, candle_type='bars', resolution='1min')->pd.DataFrame:
    cds=get_trade_candles_google_storage(exchange, asset, date_from, date_to, candle_type)
    if resolution == '1min':
        return cds
    else:
        if candle_type == 'bars':
            df = cds.resample(resolution).agg({'price_open': 'first','price_close' : 'last',
                                               'price_high' : 'max', 'price_low':'min',
                                               'volume_traded' : 'sum'}).ffill()  # fill values from last row
        elif candle_type == 'bars-extended':
            df = cds.resample(resolution).agg({'price_open': 'first', 'price_close': 'last',
                                               'price_high': 'max', 'price_low': 'min',
                                               'volume_traded': 'sum','count_trades':'sum', 'count_buys':'sum',
                                                'count_sells' :'sum', 'volume_buys':'sum', 'volume_sells' :'sum'}).ffill()  # fill values from last row

        df['time_period_start']=df.index
        return df

def get_bar_candles(exchange:str, asset:str, date_from:str, date_to:str, api_key:str, candle_type='bars', resolution='1min')->pd.DataFrame:
    cds=get_trade_candles(exchange, asset, date_from, date_to, api_key, candle_type)
    if resolution == '1min':
        return cds
    else:
        # resample
        if candle_type == 'bars':
            df = cds.resample(resolution).agg({'price_open': 'first','price_close' : 'last',
                                               'price_high' : 'max', 'price_low':'min',
                                               'volume_traded' : 'sum'}).ffill()  # fill values from last row
        elif candle_type == 'bars-extended':
            df = cds.resample(resolution).agg({'price_open': 'first', 'price_close': 'last',
                                               'price_high': 'max', 'price_low': 'min',
                                               'volume_traded': 'sum','count_trades':'sum', 'count_buys':'sum',
                                                'count_sells' :'sum', 'volume_buys':'sum', 'volume_sells' :'sum'}).ffill()  # fill values from last row


        print (df.columns.values)
        return df

def generate_extra_candles_params (df:pd.DataFrame, bars='1min') -> pd.DataFrame:
    # load data in pandas df
    df['amount'] = list(map(float, df['amount']))
    df['price'] = list(map(float, df['price']))

    tempcd = df.copy()
    tempcd = tempcd.drop(['asset_pair', 'trade_id', 'buyorder_id', 'sellorder_id',
                          'exchange_timestamp', 'newyork_adjusted_timestamp'], axis=1)

    tempcd = tempcd.resample('1min').agg({'price': 'ohlc', 'amount': 'sum'}).ffill()  # fill values from last row
    tempcd.columns = ['open', 'high', 'low', 'close', 'volume']
    tempcd.index.names=['time']

    # add time and continue with extended attributes
    df['time'] = list(
        map(lambda x: datetime.datetime.strftime(x, '%Y-%m-%d %H:%M:00'), df['newyork_adjusted_timestamp']))
    df = df.drop(['asset_pair', 'trade_id', 'buyorder_id', 'sellorder_id',
                  'exchange_timestamp'], axis=1)

    # count total trades
    tt = df.copy().groupby(['time']).agg({'amount': 'count'})

    # count buys/sells
    df2 = df.copy().groupby(['time', 'side']).agg({'price': 'count', 'amount': 'sum'}).fillna(0).unstack(-1)

    # merge
    tt['count_trades'] = tt['amount']
    tt['count_buys'] = df2['price']['B'] if 'B' in df2['price'] else [0] * len(df2.index)
    tt['count_sells'] = df2['price']['S'] if 'S' in df2['price'] else [0] * len(df2.index)
    tt['volume_buys'] = df2['amount']['B'] if 'B' in df2['amount'] else [0] * len(df2.index)
    tt['volume_sells'] = df2['amount']['S'] if 'S' in df2['amount'] else [0] * len(df2.index)

    tt = tt.drop(['amount'], axis=1).fillna(0)

    cd=tempcd.join(tt)
    if bars != '1min':
        cd = cd.resample(bars).agg({'open': 'first', 'close': 'last','high': 'max', 'low': 'min',
                                    'volume': 'sum', 'count_trades': 'sum', 'count_buys': 'sum',
                                    'count_sells': 'sum', 'volume_buys': 'sum', 'volume_sells': 'sum'}).ffill()
    return cd


def read_data_file (homepath: str, exchange:str, asset:str, resolution:str)->typing.Optional[pd.DataFrame]:
    try:
        return pd.read_pickle("%s/apidata/%s_%s_%s.pkl" % (homepath, exchange.upper(), asset.upper(), resolution))
    except Exception as e:
        raise Exception("Failed to read file caused by: %s -- %s" % (e.__class__.__name__, str(e)))

if __name__ == '__main__':
    MY_API_KEY = 'cqzsjksnF1uBXfghhUZKhkwehRMrPU3DL2QAJ9KXaus'  # THIS IS YOUR ASSIGNED KEY
    if MY_API_KEY is None or MY_API_KEY == '':
        raise Exception("Missing API Key, Please fill this field and try again")

    # let us explore the json api with a sample request to retrieve all global assets available at Consilium
    print("Testing get_global_asset_list")
    ga=get_global_asset_list(MY_API_KEY)
    print(ga)

    # test candles
    print("Testing get_bar_candles with 1min-bars")
    date_from = '2018-06-30'
    date_to = '2018-07-02'
    df=get_bar_candles('gdax', 'BTC_USD', date_from, date_to, MY_API_KEY, 'bars', '1min')
    print(df.head())

    print("Testing get_bar_candles with 1min-bars extended edition")
    date_from = '2018-06-30'
    date_to = '2018-07-02'
    df = get_bar_candles('gdax', 'BTC_USD', date_from, date_to, MY_API_KEY, 'bars-extended', '1min')
    print(df.head())


    # df=get_tradedata('gdax', 'BTC_USD', date_from, date_to, MY_API_KEY)
    # print(df.head())
