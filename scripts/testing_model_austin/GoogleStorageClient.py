# Copyright 2016 Google, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""This application demonstrates how to perform basic operations on blobs
(objects) in a Google Cloud Storage bucket.
For more information, see the README.md under /storage  and the documentation
at https://cloud.google.com/storage/docs.
"""

# import argparse
import datetime
from ConsiliumLogger import ConsiliumLogger
from google.cloud import storage
import json
import gzip
import time


class GoogleStorageClient:
    def __init__(self, bucket_name):
        self.bucket_name=bucket_name
        self.logger = ConsiliumLogger("StorageClient.Log")
        self.storage_client = storage.Client()
        self.init_bucket(bucket_name)


    def init_bucket(self, bucket_name):
        try:
            # is bucket is available
            if bucket_name not in self.list_buckets():
                self.create_bucket(bucket_name)

            self.bucket = self.storage_client.get_bucket(bucket_name)

        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during init_bucket caused by: "+str(e), "error")



    def list_buckets (self):
        try:
            return [b.name for b in self.storage_client.list_buckets()]
        except Exception as e:
            return []

    def create_bucket(self,bucket_name):
        """Creates a new bucket."""
        self.storage_client.create_bucket(bucket_name)

    def delete_bucket(self, bucket_name):
        """Deletes a bucket. The bucket must be empty."""
        try:
            bucket = self.storage_client.get_bucket(bucket_name)
            bucket.delete(force=True)
        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during delete_bucket caused by: "+str(e), "error")



    def enable_default_kms_key(self, kms_key_name):
        # [START storage_set_bucket_default_kms_key]
        """Sets a bucket's default KMS key."""
        try:
            self.bucket.default_kms_key_name = kms_key_name
            self.bucket.patch()
        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error encountered during enable_default_kms_key caused by: "+str(e), "error")

        # [END storage_set_bucket_default_kms_key]


    def get_bucket_labels(self):
        """Prints out a bucket's labels."""
        try:
            labels = self.bucket.labels
        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during get_bucket_labels caused by: "+str(e), "error")



    def add_bucket_label(self,label_key, label):
        """Add a label to a bucket."""
        try:
            labels = self.bucket.labels
            labels[label_key] = label
            self.bucket.labels = labels
            self.bucket.patch()
        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during add_bucket_label caused by: "+str(e), "error")



    def remove_bucket_label(self, label_key):
        """Remove a label from a bucket."""
        try:
            labels = self.bucket.labels

            if label_key in labels:
                del labels[label_key]

            self.bucket.labels = labels
            self.bucket.patch()

        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during remove_bucket_label caused by: "+str(e), "error")



    def list_blobs(self, prefix=None):
        """Lists all the blobs in the bucket."""
        try:
            i=10
            while i >= 0:
                i -= 1
                try:
                    blobs = self.bucket.list_blobs(prefix=prefix)
                    return [blob.name for blob in blobs]
                except Exception as le:
                    time.sleep(0.5)
            raise Exception("fail to list_blobs")

        except Exception as e:
            self.logger.logger(e.__class__.__name__+" error has occured during list_blobs caused by: "+str(e), "error")
            raise Exception (e.__class__.__name__+" error has occured during list_blobs caused by: "+str(e))

    def blob_exists(self, blob):
        '''validate if blob is available in bucket'''
        try:
            return self.bucket.blob(blob).exists()
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during blob_exists caused by: " + str(e),
                               "error")
            # return False
            raise Exception (e.__class__.__name__ + " error has occured during blob_exists caused by: " + str(e))


    # [START storage_upload_file]
    def upload_blob_from_file(self, source_file_name, destination_blob_name):
        """Uploads a file to the bucket."""
        try:
            blob = self.bucket.blob(destination_blob_name)
            blob.upload_from_filename(source_file_name)
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during upload_blob_from_file caused by: " + str(e),
                               "error")

    # [END storage_upload_file]

    def download_blob_to_dest(self, source_blob_name, destination_file_name):
        """Downloads a blob from the bucket."""
        try:
            blob = self.bucket.blob(source_blob_name)
            blob.download_to_filename(destination_file_name)
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during download_blob_to_dest caused by: " + str(e),
                               "error")
            raise Exception (e.__class__.__name__ + " error has occured during download_blob_to_dest caused by: " + str(e))
    # [START storage_upload_file]

    def upload_json_blob(self, packet, destination_blob_name):
        """Uploads a json packet to the bucket."""
        try:
            i=10
            while i >= 0:
                i -= 1
                try:
                    blob = self.bucket.blob(destination_blob_name)
                    data = bytes(json.dumps(packet), 'utf-8')
                    s_out = gzip.compress(data)
                    blob.upload_from_string(s_out)
                    return True
                except Exception as eu:
                    time.sleep(0.5)

            raise Exception("fail to upload json")
        except Exception as e:
            self.logger.logger("%s error has occured during upload_json_blob for %s caused by: %s" % (e.__class__.__name__, destination_blob_name, str(e)),
                               "error")
            raise Exception("%s error has occured during upload_json_blob for %s caused by: %s" % (e.__class__.__name__, destination_blob_name, str(e)))

    # [END storage_upload_file]
    def download_json_blob(self, source_blob_name):
        """Downloads a json blob from the bucket."""
        try:
            i = 10
            while i >= 0:
                i -= 1
                try:
                    blob = self.bucket.blob(source_blob_name)
                    message = gzip.decompress(blob.download_as_string()).decode('utf-8')
                    return json.loads(message)
                except Exception as de:
                    time.sleep(0.5)
            raise Exception("fail to download json")
        except Exception as e:
            self.logger.logger("%s error has occured during download_json_blob for %s caused by: %s" % (e.__class__.__name__, source_blob_name,str(e)),
                               "error")
            raise Exception ("%s error has occured during download_json_blob for %s caused by: %s" % (e.__class__.__name__, source_blob_name,str(e)))

    def upload_blob(self, packet, destination_blob_name):
        """Uploads a packet to the bucket."""
        try:
            blob = self.bucket.blob(destination_blob_name)
            data = bytes(str(packet), 'utf-8')
            s_out = gzip.compress(data)
            blob.upload_from_string(s_out)
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during upload_blob caused by: " + str(e),
                               "error")

    # [END storage_upload_file]
    def download_blob(self, source_blob_name):
        """Downloads a blob from the bucket."""
        try:
            blob = self.bucket.blob(source_blob_name)
            message = gzip.decompress(blob.download_as_string()).decode('utf-8')
            return str(message)
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during download_blob caused by: " + str(e),
                               "error")


    def delete_blob(self, blob_name):
        """Deletes a blob from the bucket."""
        try:
            blob = self.bucket.blob(blob_name)
            blob.delete()
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during delete_blob caused by: " + str(e),
                               "error")

    def rename_blob(self, blob_name, new_name):
        """Renames a blob."""
        try:
            blob = self.bucket.blob(blob_name)
            new_blob = self.bucket.rename_blob(blob, new_name)
        except Exception as e:
            self.logger.logger(e.__class__.__name__ + " error has occured during rename_blob caused by: " + str(e),
                               "error")




def list_blobs_with_prefix(bucket_name, prefix, delimiter=None):
    """Lists all the blobs in the bucket that begin with the prefix.
    This can be used to list all blobs in a "folder", e.g. "public/".
    The delimiter argument can be used to restrict the results to only the
    "files" in the given "folder". Without the delimiter, the entire tree under
    the prefix is returned. For example, given these blobs:
        /a/1.txt
        /a/b/2.txt
    If you just specify prefix = '/a', you'll get back:
        /a/1.txt
        /a/b/2.txt
    However, if you specify prefix='/a' and delimiter='/', you'll get back:
        /a/1.txt
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)

    blobs = bucket.list_blobs(prefix=prefix, delimiter=delimiter)

    print('Blobs:')
    for blob in blobs:
        print(blob.name)

    if delimiter:
        print('Prefixes:')
        for prefix in blobs.prefixes:
            print(prefix)


def upload_blob_with_kms(bucket_name, source_file_name, destination_blob_name,
                         kms_key_name):
    # [START storage_upload_with_kms_key]
    """Uploads a file to the bucket, encrypting it with the given KMS key."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name, kms_key_name=kms_key_name)
    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {} with encryption key {}.'.format(
        source_file_name,
        destination_blob_name,
        kms_key_name))
    # [END storage_upload_with_kms_key]




def blob_metadata(bucket_name, blob_name):
    """Prints out a blob's metadata."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(blob_name)

    print('Blob: {}'.format(blob.name))
    print('Bucket: {}'.format(blob.bucket.name))
    print('Storage class: {}'.format(blob.storage_class))
    print('ID: {}'.format(blob.id))
    print('Size: {} bytes'.format(blob.size))
    print('Updated: {}'.format(blob.updated))
    print('Generation: {}'.format(blob.generation))
    print('Metageneration: {}'.format(blob.metageneration))
    print('Etag: {}'.format(blob.etag))
    print('Owner: {}'.format(blob.owner))
    print('Component count: {}'.format(blob.component_count))
    print('Crc32c: {}'.format(blob.crc32c))
    print('md5_hash: {}'.format(blob.md5_hash))
    print('Cache-control: {}'.format(blob.cache_control))
    print('Content-type: {}'.format(blob.content_type))
    print('Content-disposition: {}'.format(blob.content_disposition))
    print('Content-encoding: {}'.format(blob.content_encoding))
    print('Content-language: {}'.format(blob.content_language))
    print('Metadata: {}'.format(blob.metadata))
    print("Temporary hold: ",
          'enabled' if blob.temporary_hold else 'disabled')
    print("Event based hold: ",
          'enabled' if blob.event_based_hold else 'disabled')
    if blob.retention_expiration_time:
        print("retentionExpirationTime: {}"
              .format(blob.retention_expiration_time))


def make_blob_public(bucket_name, blob_name):
    """Makes a blob publicly accessible."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)

    blob.make_public()

    print('Blob {} is publicly accessible at {}'.format(
        blob.name, blob.public_url))


def generate_signed_url(bucket_name, blob_name):
    """Generates a signed URL for a blob.
    Note that this method requires a service account key file. You can not use
    this if you are using Application Default Credentials from Google Compute
    Engine or from the Google Cloud SDK.
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)

    url = blob.generate_signed_url(
        # This URL is valid for 1 hour
        expiration=datetime.timedelta(hours=1),
        # Allow GET requests using this URL.
        method='GET')

    print('The signed url for {} is {}'.format(blob.name, url))




def copy_blob(bucket_name, blob_name, new_bucket_name, new_blob_name):
    """Copies a blob from one bucket to another with a new name."""
    storage_client = storage.Client()
    source_bucket = storage_client.get_bucket(bucket_name)
    source_blob = source_bucket.blob(blob_name)
    destination_bucket = storage_client.get_bucket(new_bucket_name)

    new_blob = source_bucket.copy_blob(
        source_blob, destination_bucket, new_blob_name)

    print('Blob {} in bucket {} copied to blob {} in bucket {}.'.format(
        source_blob.name, source_bucket.name, new_blob.name,
        destination_bucket.name))


if __name__ == '__main__':

    c= GoogleStorageClient("")
    data={"test" : 1}
    # c.upload_json_blob(data, "2019-01-03/data.json")
    # print(c.list_buckets())
    # print(c.list_blobs())
    #c.upload_json_blob([{"AssetPair": "BTC_USD", "ReferenceRate": "6,481.79", "RetrievalTime": "2018-11-09 06:00:00-5:00"}],"ReferenceRate-BTC-2018-11-09T05:00:00.json.gzip" )
    print(c.download_json_blob('2018_11_16/ATL_BTC.json'))
    '''
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('bucket_name', help='Your cloud storage bucket.')

    subparsers = parser.add_subparsers(dest='command')
    subparsers.add_parser('create-bucket', help=create_bucket.__doc__)
    subparsers.add_parser('delete-bucket', help=delete_bucket.__doc__)
    subparsers.add_parser('get-bucket-labels', help=get_bucket_labels.__doc__)
    subparsers.add_parser('add-bucket-label', help=add_bucket_label.__doc__)
    subparsers.add_parser(
        'remove-bucket-label', help=remove_bucket_label.__doc__)
    subparsers.add_parser('list', help=list_blobs.__doc__)

    list_with_prefix_parser = subparsers.add_parser(
        'list-with-prefix', help=list_blobs_with_prefix.__doc__)
    list_with_prefix_parser.add_argument('prefix')
    list_with_prefix_parser.add_argument('--delimiter', default=None)

    upload_parser = subparsers.add_parser('upload', help=upload_blob.__doc__)
    upload_parser.add_argument('source_file_name')
    upload_parser.add_argument('destination_blob_name')

    enable_default_kms_parser = subparsers.add_parser(
        'enable-default-kms-key', help=enable_default_kms_key.__doc__)
    enable_default_kms_parser.add_argument('kms_key_name')

    upload_kms_parser = subparsers.add_parser(
        'upload-with-kms-key', help=upload_blob_with_kms.__doc__)
    upload_kms_parser.add_argument('source_file_name')
    upload_kms_parser.add_argument('destination_blob_name')
    upload_kms_parser.add_argument('kms_key_name')

    download_parser = subparsers.add_parser(
        'download', help=download_blob.__doc__)
    download_parser.add_argument('source_blob_name')
    download_parser.add_argument('destination_file_name')

    delete_parser = subparsers.add_parser('delete', help=delete_blob.__doc__)
    delete_parser.add_argument('blob_name')

    metadata_parser = subparsers.add_parser(
        'metadata', help=blob_metadata.__doc__)
    metadata_parser.add_argument('blob_name')

    make_public_parser = subparsers.add_parser(
        'make-public', help=make_blob_public.__doc__)
    make_public_parser.add_argument('blob_name')

    signed_url_parser = subparsers.add_parser(
        'signed-url', help=generate_signed_url.__doc__)
    signed_url_parser.add_argument('blob_name')

    rename_parser = subparsers.add_parser('rename', help=rename_blob.__doc__)
    rename_parser.add_argument('blob_name')
    rename_parser.add_argument('new_name')

    copy_parser = subparsers.add_parser('copy', help=rename_blob.__doc__)
    copy_parser.add_argument('blob_name')
    copy_parser.add_argument('new_bucket_name')
    copy_parser.add_argument('new_blob_name')

    args = parser.parse_args()

    if args.command == 'create-bucket':
        create_bucket(args.bucket_name)
    if args.command == 'enable-default-kms-key':
        enable_default_kms_key(args.bucket_name, args.kms_key_name)
    elif args.command == 'delete-bucket':
        delete_bucket(args.bucket_name)
    if args.command == 'get-bucket-labels':
        get_bucket_labels(args.bucket_name)
    if args.command == 'add-bucket-label':
        add_bucket_label(args.bucket_name)
    if args.command == 'remove-bucket-label':
        remove_bucket_label(args.bucket_name)
    elif args.command == 'list':
        list_blobs(args.bucket_name)
    elif args.command == 'list-with-prefix':
        list_blobs_with_prefix(args.bucket_name, args.prefix, args.delimiter)
    elif args.command == 'upload':
        upload_blob(
            args.bucket_name,
            args.source_file_name,
            args.destination_blob_name)
    elif args.command == 'upload-with-kms-key':
        upload_blob_with_kms(
            args.bucket_name,
            args.source_file_name,
            args.destination_blob_name,
            args.kms_key_name)
    elif args.command == 'download':
        download_blob(
            args.bucket_name,
            args.source_blob_name,
            args.destination_file_name)
    elif args.command == 'delete':
        delete_blob(args.bucket_name, args.blob_name)
    elif args.command == 'metadata':
        blob_metadata(args.bucket_name, args.blob_name)
    elif args.command == 'make-public':
        make_blob_public(args.bucket_name, args.blob_name)
    elif args.command == 'signed-url':
        generate_signed_url(args.bucket_name, args.blob_name)
    elif args.command == 'rename':
        rename_blob(args.bucket_name, args.blob_name, args.new_name)
    elif args.command == 'copy':
        copy_blob(
            args.bucket_name,
            args.blob_name,
            args.new_bucket_name,
            args.new_blob_name)
    '''
