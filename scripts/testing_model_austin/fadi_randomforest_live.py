from pathlib import Path

'''
from kashta.testing_model_austin.DatabaseManager import *
from kashta.testing_model_austin.RandomForestFeatureGenerator import RandomForestFeatureGenerator
from kashta.testing_model_austin.enum_classes import *
'''

from DatabaseManager import *
from RandomForestFeatureGenerator import RandomForestFeatureGenerator
from enum_classes import *
from GoogleStorageClient import GoogleStorageClient

from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import pandas as pd
import joblib
import numpy
import time
import os
from typing import *
import csv


# paths and initializers
dirname = os.path.dirname(__file__)

MODEL_ID = 220


'''
LOCAL_DATA_FILE = str(Path(__file__).parent)+'/data/time_bars/target_ternary_momentum/BTC_USD_gdax_1H_time_1P_target_ternary_momentum_0.005.csv'


MODEL_FILE_DIRECTORY = str(Path(__file__).parent)+'/models/'
print(LOCAL_DATA_FILE)
print(MODEL_FILE_DIRECTORY)
'''


def init_money_maker()->Dict:
    ################################################################################
    # Establish connection to database
    ################################################################################
    with open(str(Path(__file__).parent)+'/credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

    print('CREDS!')
    print(sql_db_creds)

    #path_to_creds=str(Path(__file__).parent)+"/credentials/"
    #print(path_to_creds)
    kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
          'database': sql_db_creds[7],
          'ssl': {'ca': sql_db_creds[4],
                  'key': sql_db_creds[5],
                  'cert': sql_db_creds[6], 'check_hostname': False},
          'port': int(sql_db_creds[3])}

    ###FADI: Left this here for you if you need it for prod
    '''
    kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
          'database': sql_db_creds[7],
          'ssl': {'ca': path_to_creds+sql_db_creds[4],
                  'key': path_to_creds+sql_db_creds[5],
                  'cert': path_to_creds+sql_db_creds[6], 'check_hostname': False},
          'port': int(sql_db_creds[3])}
    '''

    db_manager=DatabaseManager (kwargs, 'fadi_randomforest_live.py', use_dict_cursor=True )

    ################################################################################
    # Get all metadata for this model from "models table"
    ################################################################################
    sql = "SELECT * FROM random_forest_models WHERE id='" + str(MODEL_ID) + "'"
    model_to_run = db_manager.fetch_mysql_result(sql)[0]

    model_id = int(model_to_run['id'])

    if model_id != MODEL_ID:
        print('Something is very broken!')
        exit()


    dataset_id = model_to_run['dataset_id']
    bucket_name_gs = model_to_run['bucket_name_gs']
    normalization_type = model_to_run['normalization_type']
    max_depth = int(model_to_run['max_depth'])
    max_features = model_to_run['max_features']
    max_samples = float(model_to_run['max_samples'])
    num_estimators = int(model_to_run['num_estimators'])
    pca_variance_perc = float(model_to_run['pca_variance_perc'])
    test_set_perc = float(model_to_run['test_set_perc'])
    dev_set_perc = float(model_to_run['dev_set_perc'])
    random_seed = int(model_to_run['random_seed'])
    split_criterion = model_to_run['split_criterion']
    pca_params_dict = eval(model_to_run['pca_params_dict'])
    optuna_ta_dict = eval(model_to_run['optuna_ta_dict'])
    model_file_gs = model_to_run['model_file_gs']
    model_file_local = str(Path(__file__).parent)+'/'+model_file_gs
    dev_predictions_file_gs = model_to_run['dev_predictions_file_gs']
    dev_predictions_file_local = str(Path(__file__).parent)+'/'+dev_predictions_file_gs

    ################################################################################
    # Initialize google storage bucket
    ################################################################################
    try:
        google_storage_obj = GoogleStorageClient(bucket_name_gs)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        exit()

    ################################################################################
    # Get all relevant dataset parameters from datasets table
    ################################################################################
    sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
    dataset = db_manager.fetch_mysql_result(sql)[0]

    target_type = dataset['target_type']
    index_col_name = dataset['index_col_name']
    target_col_name = dataset['target_col_name']
    dataset_file_gs = dataset['filename_gs']
    dataset_file_local = str(Path(__file__).parent)+'/data/'+dataset_file_gs

    '''
    ################################################################################
    # Fetch model file from google storage
    ################################################################################
    try:
        google_storage_obj.download_blob_to_dest(model_file_gs, model_file_local)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        exit()

    ################################################################################
    # Fetch raw data file from google storage
    ################################################################################
    try:
        google_storage_obj.download_blob_to_dest(dataset_file_gs, dataset_file_local)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        exit()

    ################################################################################
    # Fetch dev set predictions file from google storage
    ################################################################################
    try:
        google_storage_obj.download_blob_to_dest(dev_predictions_file_gs, dev_predictions_file_local)
    except Exception as e:
        print('Google storage exception!')
        print (e)
        exit()


    print('Done loading files!')
    exit()
    '''

    ################################################################################
    # Load raw data into pandas DF
    ################################################################################
    numpy.random.seed(random_seed)
    raw_ohlcv_df = pd.read_csv(dataset_file_local, header=0, parse_dates=[0], index_col=index_col_name)
    target_col_df = raw_ohlcv_df[target_col_name]

    ################################################################################
    # Generate all features from raw data for train set, calculate class imbalances
    ################################################################################
    feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc, random_seed, target_col_name)
    feature_generator.features_from_dict(optuna_ta_dict)
    train_X, train_y, train_index = feature_generator.get_df_dataset(DatasetSplit.TRAIN)
    dev_X, dev_y, dev_index = feature_generator.get_df_dataset(DatasetSplit.DEV)

    train_class_weights = feature_generator.train_class_weights
    ################################################################################
    # Fit scaler and PCA to train set, transform train set
    ################################################################################
    if normalization_type == 'MIN_MAX':
        scaler = MinMaxScaler()

    elif normalization_type == 'Z_SCORE':
        scaler = StandardScaler()

    scaler = scaler.fit(train_X)
    train_X = scaler.transform(train_X)
    dev_X = scaler.transform(dev_X)

    if pca_variance_perc > 0:
        pca = PCA()
        pca = pca.set_params(**pca_params_dict)
        pca = pca.fit(train_X)
        train_X = pca.transform(train_X)
        dev_X = pca.transform(dev_X)
    else:
        pca = None


    ################################################################################
    # Train classifier on preprocessed data
    ################################################################################
    trained_rf_clf = RandomForestClassifier(max_features=max_features, random_state=random_seed, class_weight=train_class_weights, max_depth=max_depth, n_estimators=num_estimators, criterion=split_criterion, max_samples=max_samples, n_jobs=-1)
    trained_rf_clf = trained_rf_clf.fit(train_X, train_y)
    trained_model_params = trained_rf_clf.get_params()
    trained_model_dev_preds = trained_rf_clf.predict(dev_X)

    ################################################################################
    # Load classifier from google storage
    ################################################################################
    loaded_rf_clf = joblib.load(model_file_local)
    loaded_model_params = loaded_rf_clf.get_params()

    loaded_model_dev_preds = loaded_rf_clf.predict(dev_X)

    ################################################################################
    # Ensure model loaded from google storage has params expected from DB entry
    ################################################################################
    if trained_model_params != loaded_model_params:
        print('Something is wrong with model params!')
        exit()

    ################################################################################
    # Ensure model loaded from google storage makes same predictions as trained model
    ################################################################################
    if list(loaded_model_dev_preds) != list(trained_model_dev_preds):
        print('Predictions dont match!')
        exit()

    ################################################################################
    # Dev set predictions from google storage
    ################################################################################
    with open(dev_predictions_file_local, newline='') as f:
        reader = csv.reader(f)
        gs_dev_preds = list(reader)

    del gs_dev_preds[0] #get rid of header column
    gs_dev_preds = [float(x[2]) for x in gs_dev_preds] #Only select predictions, cast strings to floats

    ################################################################################
    # Ensure model loaded from google storage makes same predictions as google storage preds
    ################################################################################
    if list(loaded_model_dev_preds) != list(gs_dev_preds):
        print('Predictions dont match!')
        exit()



    print('Done with training process!')
    return {"index_col_name":index_col_name, "random_seed":random_seed, "target_col_name":target_col_name,
            "scaler":scaler,"pca_variance_perc":pca_variance_perc, "pca_obj":pca, "rf_clf":loaded_rf_clf, "optuna_ta_dict": optuna_ta_dict}



def predict(df:pd.DataFrame, random_seed:float, index_col_name:str, target_col_name:str, scaler:MinMaxScaler,
            pca_variance_perc:float, pca_obj:PCA, rf_clf:RandomForestClassifier, optuna_ta_dict: dict):
    ################################################################################################################################################################
    #                                                           LIVE PREDICTION CODE
    ################################################################################################################################################################
    raw_ohlcv_df=df.drop(columns=['time_period_start'], axis=1)
    ################################################################################
    # Generate all features from raw data for train set
    ################################################################################
    # Use entire input data to generate features, no train/dev/test when running live
    dev_set_perc = 0
    test_set_perc = 0

    feature_generator = RandomForestFeatureGenerator(index_col_name, raw_ohlcv_df, dev_set_perc, test_set_perc,
                                                     random_seed, target_col_name)

    feature_generator.features_from_dict(optuna_ta_dict)
    input_X, _, _ = feature_generator.get_df_dataset(DatasetSplit.TRAIN)

    ################################################################################
    # Transform input data using pre-fitted scaler and PCA
    ################################################################################
    input_X = scaler.transform(input_X)

    if pca_variance_perc > 0:
        input_X = pca_obj.transform(input_X)

    y_pred = rf_clf.predict(input_X)
    return y_pred


if __name__ == "__main__":
    init_money_maker()