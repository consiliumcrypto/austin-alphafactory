from pathlib import Path

'''
from kashta.testing_model_austin.DatabaseManager import *
from kashta.testing_model_austin.RandomForestFeatureGenerator import RandomForestFeatureGenerator
from kashta.testing_model_austin.enum_classes import *
'''

from DatabaseManager import *
from RandomForestFeatureGenerator import RandomForestFeatureGenerator
from enum_classes import *
from GoogleStorageClient import GoogleStorageClient

from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import pandas as pd
import joblib
import numpy
import time
import os
from typing import *
import csv
import traceback


# paths and initializers
dirname = os.path.dirname(__file__)

MODEL_ID = 220


class LiveModelManager:
    def __init__(self, model_id):
        self._model_id = model_id
        self._db_manager = None
        self.model_metadata = None
        self.dataset_metadata = None
        self._google_storage_obj = None

        self._train_X = None
        self._train_y = None
        self._dev_X = None
        self._dev_y = None

        self._gs_model = None
        self._train_class_weights = None
        self._pca_obj = None
        self._scaler = None

        self._init_db()
        self._fetch_model_metadata()
        numpy.random.seed(self.model_metadata['random_seed'])
        self.model_metadata['pca_params_dict'] = eval(self.model_metadata['pca_params_dict'])
        self.model_metadata['optuna_ta_dict'] = eval(self.model_metadata['optuna_ta_dict'])

        self._fetch_dataset_metadata()
        self._init_gs()
        self._fetch_gs_model_file()
        self._fetch_gs_raw_data_file()
        self._raw_data_df = pd.read_csv(str(Path(__file__).parent) + '/data/'+self.dataset_metadata['filename_gs'], header=0, parse_dates=[0], index_col=self.dataset_metadata['index_col_name'])
        self._fetch_gs_dev_preds()
        self._generate_features()
        self._apply_scaler()
        self._apply_pca()
        self._load_gs_model()
        self._check_model_state()


    def _init_db (self, creds_file='/credentials/server_sql_db_creds.txt'):
        # Establish connection to database

        with open(str(Path(__file__).parent) + creds_file) as f:
            sql_db_creds = f.readlines()
            sql_db_creds = [x.strip() for x in sql_db_creds]

        kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
                  'database': sql_db_creds[7],
                  'ssl': {'ca': sql_db_creds[4],
                          'key': sql_db_creds[5],
                          'cert': sql_db_creds[6], 'check_hostname': False},
                  'port': int(sql_db_creds[3])}

        db_manager = DatabaseManager(kwargs, 'LiveModelManager.py', use_dict_cursor=True)
        self._db_manager = db_manager
        print('Conected to DB!')


    def _fetch_model_metadata (self):
        sql = "SELECT * FROM random_forest_models WHERE id='" + str(self._model_id) + "'"
        model_metadata = self._db_manager.fetch_mysql_result(sql)[0]

        model_id = int(model_metadata['id'])

        if model_id != MODEL_ID:
            print('Database is corrupted, model ids do not match!')
            exit()

        self.model_metadata = model_metadata
        print(self.model_metadata)

    def _init_gs (self):
        try:
            self._google_storage_obj = GoogleStorageClient(self.model_metadata['bucket_name_gs'])
        except Exception as e:
            print('Google storage exception!')
            print(e)
            exit()


    def _fetch_dataset_metadata (self):
        sql = "SELECT * FROM datasets WHERE id='" + str(self.model_metadata['dataset_id']) + "'"
        self.dataset_metadata = self._db_manager.fetch_mysql_result(sql)[0]

    def _fetch_gs_model_file (self):
        try:
            print('fetching_gs_model')
            print(self.model_metadata['model_file_gs'])
            self._google_storage_obj.download_blob_to_dest(self.model_metadata['model_file_gs'], str(Path(__file__).parent)+'/'+self.model_metadata['model_file_gs'])
        except Exception as e:
            print('Google storage exception!')
            print(e)
            exit()



    def _fetch_gs_raw_data_file (self):
        try:
            print(self.dataset_metadata['filename_gs'])
            self._google_storage_obj.download_blob_to_dest(self.dataset_metadata['filename_gs'], str(Path(__file__).parent)+'/data/'+self.dataset_metadata['filename_gs'])
        except Exception as e:
            print('Google storage exception!')
            print(str(traceback.format_exc()))
            exit()



    def _fetch_gs_dev_preds (self):
        try:
            self._google_storage_obj.download_blob_to_dest(self.model_metadata['dev_predictions_file_gs'], str(Path(__file__).parent)+'/'+self.model_metadata['dev_predictions_file_gs'])
        except Exception as e:
            print('Google storage exception!')
            print(e)
            exit()



    def _generate_features (self):
        feature_generator = RandomForestFeatureGenerator(self.dataset_metadata['index_col_name'], self._raw_data_df, self.model_metadata['dev_set_perc'], self.model_metadata['test_set_perc'],
                                                         self.model_metadata['random_seed'], self.dataset_metadata['target_col_name'])
        feature_generator.features_from_dict(self.model_metadata['optuna_ta_dict'])
        self._train_X, self._train_y, _ = feature_generator.get_df_dataset(DatasetSplit.TRAIN)
        self._dev_X, self._dev_y, _ = feature_generator.get_df_dataset(DatasetSplit.DEV)

        self._train_class_weights = feature_generator.train_class_weights

        print(self._train_X.head())

    def _apply_scaler (self):
        if self.model_metadata['normalization_type'] == 'MIN_MAX':
            scaler = MinMaxScaler()

        elif self.model_metadata['normalization_type'] == 'Z_SCORE':
            scaler = StandardScaler()

        else:
            print('No recognized scaler provided:  ' + self.model_metadata['normalization_type'])
            exit()

        self._scaler = scaler.fit(self._train_X)
        self._train_X = self._scaler.transform(self._train_X)
        self._dev_X = self._scaler.transform(self._dev_X)


    def _apply_pca (self):
        if self.model_metadata['pca_variance_perc'] > 0:
            self._pca_obj = PCA()
            self._pca_obj = self._pca_obj.set_params(**self.model_metadata['pca_params_dict'])
            self._pca_obj = self._pca_obj.fit(self._train_X)
            self._train_X = self._pca_obj.transform(self._train_X)
            self._dev_X = self._pca_obj.transform(self._dev_X)
        else:
            self._pca_obj = None


    def _load_gs_model (self):
        self._gs_model = joblib.load(str(Path(__file__).parent)+'/'+self.model_metadata['model_file_gs'])


    def _check_model_state (self):
        # Train classifier on preprocessed data
        trained_rf_clf = RandomForestClassifier(max_features=self.model_metadata['max_features'], random_state=self.model_metadata['random_seed'],
                                                class_weight=self._train_class_weights, max_depth=int(self.model_metadata['max_depth']),
                                                n_estimators=int(self.model_metadata['num_estimators']), criterion=self.model_metadata['split_criterion'],
                                                max_samples=float(self.model_metadata['max_samples']), n_jobs=-1)
        trained_rf_clf = trained_rf_clf.fit(self._train_X, self._train_y)
        trained_model_params = trained_rf_clf.get_params()
        trained_model_dev_preds = trained_rf_clf.predict(self._dev_X)


        loaded_model_params = self._gs_model.get_params()
        loaded_model_dev_preds = self._gs_model.predict(self._dev_X)

        # Ensure model loaded from google storage has params expected from DB entry#
        if trained_model_params != loaded_model_params:
            print('Something is wrong with model params!')
            exit()

        # Ensure model loaded from google storage makes same predictions as trained model
        if list(loaded_model_dev_preds) != list(trained_model_dev_preds):
            print('Predictions dont match!')
            exit()

        # Ensure Dev set predictions from google storage match loaded model predictions
        with open(str(Path(__file__).parent)+'/'+self.model_metadata['dev_predictions_file_gs'], newline='') as f:
            reader = csv.reader(f)
            gs_dev_preds = list(reader)

        del gs_dev_preds[0]  # get rid of header column
        gs_dev_preds = [float(x[2]) for x in gs_dev_preds]  # Only select predictions, cast strings to floats


        # Ensure model loaded from google storage makes same predictions as google storage preds
        if list(loaded_model_dev_preds) != list(gs_dev_preds):
            print('Predictions dont match!')
            exit()

    def model_predict(self, raw_df, period:str=None):
        print(raw_df.columns.values)
        if 'time_period_start' in raw_df.columns.values:
            raw_df = raw_df.drop(columns=['time_period_start'], axis=1)

        # Generate all features from raw data for train set
        # Use entire input data to generate features, no train/dev/test when running live
        dev_set_perc = 0
        test_set_perc = 0

        feature_generator = RandomForestFeatureGenerator(self.dataset_metadata['index_col_name'], raw_df, dev_set_perc, test_set_perc,
                                                         self.model_metadata['random_seed'], self.dataset_metadata['target_col_name'])

        feature_generator.features_from_dict(self.model_metadata['optuna_ta_dict'])
        input_X, _, _ = feature_generator.get_df_dataset(DatasetSplit.TRAIN)

        # Transform input data using pre-fitted scaler and PCA
        input_X = self._scaler.transform(input_X)

        if self.model_metadata['pca_variance_perc'] > 0:
            input_X = self._pca_obj.transform(input_X)

        y_pred = self._gs_model.predict(input_X)
        y_prob = self._gs_model.predict_proba(input_X)

        return y_pred, y_prob
