import talib
import random
import numpy as np
import datetime
import dateutil.parser as dp
import math
import requests as rq
import pandas as pd
import time

import sklearn.metrics as sk_metrics
import keras
from twilio.rest import Client

import plotly
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import json

import os
import platform

from api_utils import *


class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


def train_dev_test_split (df, dev_perc=0.1, test_perc=0.1):
    num_train_examples = int(np.floor((len(df) * (1 - test_perc - dev_perc))))
    num_dev_examples = int(np.floor((len(df) * (1 - test_perc)))) - num_train_examples
    num_test_examples = len(df) - num_train_examples - num_dev_examples

    train = df.iloc[:-(num_dev_examples + num_test_examples)]
    dev = df.iloc[-(num_dev_examples + num_test_examples):-num_test_examples]
    test = df.iloc[-num_test_examples:]

    return train, dev, test

def plot_results_dict (results_dict, filename):
    shift_periods = results_dict['metadata']['target_shift_periods']
    models_run = results_dict['metadata']['models_run']
    model_results = dict()
    model_results_pca = dict()
    model_names = []

    for m in models_run:
        model_names.append(m['model_name'])

    #Initialize lists to hold results values for each model at each target shift period
    for m in model_names:
        model_results[m] = []
        model_results_pca[m] = []

    #Get list of best results for each model at each target shift period

    for i in results_dict['results']:
        shift_period = i['shift_period']
        model_name = i['model_name']
        final_max_fitness = i['final_max_fitness']
        final_max_fitness_pca = i['pca_final_max_fitness']
        model_results[model_name].append(final_max_fitness)
        model_results_pca[model_name].append(final_max_fitness_pca)

    ########## TESTING DATA ##########
    traces = []

    for k, v in model_results.items():
        scores = []
        for i in v:
            scores.append(i['score'])
        trace = go.Scatter(
            x = shift_periods,
            y = scores,
            name = k + ' Results'
        )

        traces.append(trace)

    for k, v in model_results_pca.items():
        scores = []
        for i in v:
            scores.append(i['score'])
        trace = go.Scatter(
            x = shift_periods,
            y = scores,
            name = k + ' Results (PCA)'
        )

        traces.append(trace)


    layout = go.Layout(
            yaxis=dict(
                title='Model Performance'
            ),
            xaxis=dict(
                title='Target variable shifted periods',
            )
    )
    ##################################


    fig=dict(data=traces, layout=layout)
    plotly.offline.plot(fig, auto_open=False, filename=filename)


def save_model(model, fname):
    with open(fname, 'wb') as f:
        pickle.dump(model, f)

def load_model (pickled_model):
    with open(pickled_model, 'rb') as fid:
        model = pickle.load(fid)
    return model


def send_text_msg (rec_num, msg):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    account_sid = 'ACe2dea3bce2410eb638ca13d555a9b65c'
    auth_token = 'b77220422cae0553bffadf9f9913d48f'
    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                         body=msg,
                         from_='+14387950253',
                         to='+1' + str(rec_num)
                     )
    return

def compute_all_technicals(df_in, high='price_high', low='price_low', close='price_close', volume='volume_traded', periods=None):
    df = df_in.copy()

    high = df[high].values
    low = df[low].values
    close = df[close].values
    volume = df[volume].values

    #Default periods for each indicator
    if periods == None:
        '''
        SMA_PERIODS = [3]
        WILLR_PERIODS = [3]
        MOM_PERIODS = [3]
        RSI_PERIODS = [5]
        ADX_PERIODS = [5]
        ADXR_PERIODS = [5]
        TRIX_PERIODS = [5]
        ATR_PERIODS = [5]
        MFI_PERIODS = [5]
        PROC_PERIODS = [5]
        CCI_PERIODS = [5]
        '''

        SMA_PERIODS = [3,5,10,20,30]
        WILLR_PERIODS = range(5,10)
        MOM_PERIODS = range(3,10)
        RSI_PERIODS = [5,10,15,20]
        ADX_PERIODS = [5,10,15,20]
        ADXR_PERIODS = [5,10,15,20]
        TRIX_PERIODS = [5,10,15,20]
        ATR_PERIODS = [5,10,15,20]
        MFI_PERIODS = [5,10,15,20]
        PROC_PERIODS = [5,10,15,20]
        CCI_PERIODS = [5,10,15,20]


    else:
        SMA_PERIODS = periods['sma']
        WILLR_PERIODS = periods['willr']
        MOM_PERIODS = periods['mom']
        RSI_PERIODS = periods['rsi']
        ADX_PERIODS = periods['adx']
        ADXR_PERIODS = periods['adxr']
        TRIX_PERIODS = periods['trix']
        ATR_PERIODS = periods['atr']
        MFI_PERIODS = periods['mfi']
        PROC_PERIODS = periods['proc']
        CCI_PERIODS = periods['cci']

    # Compute SMA with various periods
    for i in SMA_PERIODS:
        df['SMA_' + str(i)] = talib.SMA(close, timeperiod=i)

    #Compute Stochastic (slowk, slowd)
    df['STOCH_SLOWK_3_5'], df['STOCH_SLOWD_3_5'] = talib.STOCH(high, low, close, fastk_period=5, slowk_period=3, slowk_matype=0, slowd_period=3, slowd_matype=0)

    #Compute percentage price oscillator
    df['PPO_5_10'] = talib.PPO(close, fastperiod=5, slowperiod=10, matype=0)

    #Compute Williams %R with various periods
    for i in WILLR_PERIODS:
        df['WILLR_' + str(i)]= talib.WILLR(high, low, close, timeperiod=i)

    #Compute Momentum with various periods
    for i in MOM_PERIODS:
        df['MOM_' + str(i)] = talib.MOM(close, timeperiod=i)

    #Compute RSI with various periods
    for i in RSI_PERIODS:
        df['RSI_' + str(i)] = talib.RSI(close, timeperiod=i)

    #Compute Average Directional Index with various periods
    for i in ADX_PERIODS:
        df['ADX_' + str(i)] = talib.ADX(high, low, close, timeperiod=i)

    #Compute Average Directional Index Rating with various periods
    for i in ADXR_PERIODS:
        df['ADXR_' + str(i)] = talib.ADXR(high, low, close, timeperiod=i)

    #Compute Triple Exponential Moving Average with various periods
    for i in TRIX_PERIODS:
        df['TRIX_' + str(i)] = talib.TRIX(close, timeperiod=i)

    #Compute On Balance Volume with various periods
    df['OBV'] = talib.OBV(close, volume)

    #Compute Average True Range with various periods
    for i in ATR_PERIODS:
        df['ATR_' + str(i)] = talib.ATR(high, low, close, timeperiod=i)

    #Compute Money Flow Index with various periods
    for i in MFI_PERIODS:
        df['MFI_' + str(i)] = talib.MFI(high, low, close, volume, timeperiod=i)

    #Compute Price Rate of Change (PROC) with various periods
    for i in PROC_PERIODS:
        df['PROC_' + str(i)] = talib.ROC(close, timeperiod=i)

    #Compute Weighted Close Price (WCP)
    df['WCP'] = talib.WCLPRICE(high, low, close)

    #TODO Compute Williams Accumulation Distribution Line

    #Compute Accumulation/Distribution Oscillator
    df['ADOSC_3_10'] =  talib.ADOSC(high, low, close, volume, fastperiod=3, slowperiod=10)

    #Compute Moving Average Convergence Divergence
    #TODO: MACD Periods
    df['MACD'], df['MACDSIG'], df['MACDHIST'] = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)

    #Compute Commodity Channel Index with various periods
    for i in CCI_PERIODS:
        df['CCI_' + str(i)] = talib.CCI(high, low, close, timeperiod=i)

    #Compute Bollinger Bands
    df['BBANDS_UP'], df['BBANDS_MID'], df['BBANDS_LOW'] = talib.BBANDS(close, timeperiod=5, nbdevup=2, nbdevdn=2, matype=0)

    return df



def compute_new_features(df_in):
    df = df_in.copy()

    open = df['price_open']
    high = df['price_high']
    low = df['price_low']
    close = df['price_close']
    total_volume = df['volume_traded']

    count_buys = df['count_buys']
    count_sells = df['count_sells']
    volume_buys = df['volume_buys']
    volume_sells = df['volume_sells']
    total_trade_count = df['count_trades']


    #TODO: Handle divisions by zero and replace with a meaningful value (not 'inf')
    df['buy_trades_to_sell_trades'] = count_buys / count_sells
    df['buy_volume_to_sell_volume'] = volume_buys / volume_sells



    #Interbar strength -->  (close-low) / (high-low)
    #TODO: Replace screwed up values with 0
    df['interbar_strength'] = (close - low) / (high - low)
    df['interbar_strength'].replace(np.nan, 0, inplace=True) #For cases where high-low = 0 AND close-low = 0

    df.replace(np.inf, 1, inplace=True)



    #TODO: This should be handled in the candles API --> Don't include index as an additional column
    if 'time_period_start.1' in df.columns.values:
        df.drop('time_period_start.1', inplace=True, axis=1)

    return df


#MA,  diff(MA), MA(diff(MA))
def compute_ma_diffs (df_in, ma_windows=[2,3,6], ignore_col=''):
    df = df_in.copy()

    for f in df.columns.values:
        if str(f) != ignore_col:
            for ma_window in ma_windows:
                #MA
                df[f + '_MA_' + str(ma_window)] = df[f].rolling(ma_window).mean()

                # diff(MA)     Should be T - (T-1) --> No data snooping
                df[f + '_DIFF(MA)_' + str(ma_window)] = df[f + '_MA_' + str(ma_window)] - df[f + '_MA_' + str(ma_window)].shift(1)

                # MA(diff(MA))
                df[f + '_MA(DIFF(MA))_' + str(ma_window)] = df[f + '_DIFF(MA)_' + str(ma_window)].rolling(ma_window).mean()

    return df

#TODO: Implement this
def compute_pct_changes(df_in, ignore_col=''):
    return



def compute_basic_technicals(df_in, high='price_high', low='price_low', close='price_close', volume='volume_traded', periods=None):
    df = df_in.copy()

    high = df[high].values
    low = df[low].values
    close = df[close].values
    volume = df[volume].values

    #Default periods for each indicator
    if periods == None:

        SMA_PERIODS = [5, 20, 50]
        MOM_PERIODS = [10]
        RSI_PERIODS = [14]
        ADX_PERIODS = [14]
        EMA_PERIODS = [5, 20, 50]

        '''
        WILLR_PERIODS = [3]

        ADXR_PERIODS = [5]
        TRIX_PERIODS = [5]
        ATR_PERIODS = [5]
        MFI_PERIODS = [5]
        PROC_PERIODS = [5]
        CCI_PERIODS = [5]
        '''

    else:
        SMA_PERIODS = periods['sma']
        WILLR_PERIODS = periods['willr']
        MOM_PERIODS = periods['mom']
        RSI_PERIODS = periods['rsi']
        ADX_PERIODS = periods['adx']
        ADXR_PERIODS = periods['adxr']
        TRIX_PERIODS = periods['trix']
        ATR_PERIODS = periods['atr']
        MFI_PERIODS = periods['mfi']
        PROC_PERIODS = periods['proc']
        CCI_PERIODS = periods['cci']

    # Compute SMA with various periods
    for i in SMA_PERIODS:
        df['SMA_' + str(i)] = talib.SMA(close, timeperiod=i)

    #Compute Momentum with various periods
    for i in MOM_PERIODS:
        df['MOM_' + str(i)] = talib.MOM(close, timeperiod=i)

    #Compute RSI with various periods
    for i in RSI_PERIODS:
        df['RSI_' + str(i)] = talib.RSI(close, timeperiod=i)

    #Compute Average Directional Index with various periods
    for i in ADX_PERIODS:
        df['ADX_' + str(i)] = talib.ADX(high, low, close, timeperiod=i)

    #Compute Average Directional Index with various periods
    for i in EMA_PERIODS:
        df['EMA_' + str(i)] = talib.EMA(close, timeperiod=i)

    return df


def shutdown_server ():
    #Detect if this is running in the cloud
    if platform.system() == 'Linux':
        os.system("sudo shutdown now -h")
    else:
        print ('Not running in the cloud, not shutting down.')

def restart_server ():
    #Detect if this is running in the cloud
    if platform.system() == 'Linux':
        os.system("sudo reboot")
    else:
        print ('Not running in the cloud, not restarting.')

#Takes a dataframe with 'y' (actual) and 'y_pred' (predicted) columns
def calc_regressor_performance (df):
    #NOTE: Keys of this dict must match the column names in the regressor_performance table
    metrics = dict()

    #All predictions and actuals
    all_y = df['y'].tolist()
    all_preds = df['y_pred'].tolist()
    df['sign_preds'] = df['y_pred'].apply(np.sign)
    df['sign_y'] = df['y'].apply(np.sign)

    df['sign_correct'] = 0
    df.loc[df.sign_preds * df.sign_y > 0, 'sign_correct'] = 1

    df['sign_incorrect'] = 0
    df.loc[df.sign_preds * df.sign_y < 0, 'sign_incorrect'] = 1

    df['is_predicted'] = df['sign_correct'] + df['sign_incorrect']

    df['result'] = df.sign_preds * df['y']


    metrics['accuracy'] = df['sign_correct'].sum()*1.0 / (df['is_predicted'].sum() * 1.0) * 100.0
    metrics['edge'] = df['result'].mean()
    #metrics['edge_std_dev'] = metrics['edge'].std()
    metrics['noise'] = df['y_pred'].diff().abs().mean()

    metrics['y_chg'] = df['y'].abs().mean()
    metrics['y_pred_chg'] = df['y_pred'].abs().mean()
    metrics['prediction_calibration'] = metrics['y_pred_chg'] / metrics['y_chg']
    metrics['capture_ratio'] = metrics['edge'] / (metrics['y_chg'] * 100)

    metrics['edge_up'] = df[df['sign_preds'] == 1].result.mean() - df['y'].mean()
    metrics['edge_down'] = df[df['sign_preds'] == -1].result.mean() - df['y'].mean()

    metrics['edge_win'] = df[df['sign_correct'] == 1].result.mean() - df['y'].mean()
    metrics['edge_lose'] = df[df['sign_incorrect'] == 1].result.mean() - df['y'].mean()

    #Up move predictions and actuals
    up_preds = df[df['y_pred'] > 0]
    up_y = up_preds['y'].tolist()
    up_preds = up_preds['y_pred'].tolist()

    #Down move predictions and actuals
    down_preds = df[df['y_pred'] < 0]
    print (down_preds.head())
    down_y = down_preds['y'].tolist()
    down_preds = down_preds['y_pred'].tolist()

    #Metrics on total dataset
    metrics['mean_squared_error_score'] = sk_metrics.mean_squared_error(all_y, all_preds)
    metrics['r2_score'] = sk_metrics.r2_score(all_y, all_preds)
    metrics['explained_variance_score'] = sk_metrics.explained_variance_score(all_y, all_preds)
    metrics['mean_absolute_error_score'] = sk_metrics.mean_absolute_error(all_y, all_preds)
    metrics['median_absolute_error_score'] = sk_metrics.median_absolute_error(all_y, all_preds)

    #Metrics on "up" predictions
    if (len(down_y) > 1) and (len(down_preds) > 1):
        metrics['up_mean_squared_error_score'] = sk_metrics.mean_squared_error(up_y, up_preds)
        metrics['up_r2_score'] = sk_metrics.r2_score(up_y, up_preds)
        metrics['up_explained_variance_score'] = sk_metrics.explained_variance_score(up_y, up_preds)
        metrics['up_mean_absolute_error_score'] = sk_metrics.mean_absolute_error(up_y, up_preds)
        metrics['up_median_absolute_error_score'] = sk_metrics.median_absolute_error(up_y, up_preds)

    #Metrics on "down" predictions
    if (len(down_y) > 1) and (len(down_preds) > 1):
        metrics['down_mean_squared_error_score'] = sk_metrics.mean_squared_error(down_y, down_preds)
        metrics['down_r2_score'] = sk_metrics.r2_score(down_y, down_preds)
        metrics['down_explained_variance_score'] = sk_metrics.explained_variance_score(down_y, down_preds)
        metrics['down_mean_absolute_error_score'] = sk_metrics.mean_absolute_error(down_y, down_preds)
        metrics['down_median_absolute_error_score'] = sk_metrics.median_absolute_error(down_y, down_preds)

    #Replace any 'nans' with 0
    for k, v in metrics.items():
        if math.isnan(v):
            metrics[k]=0

    return metrics

#TODO:
def calc_classifier_performance (y_ypred_df):
    #NOTE: Keys of this dict must match the column names in the regressor_performance table
    metrics = dict()

    up_preds = y_ypred_df[y_ypred_df['y_pred'] == 1]
    up_actuals = y_ypred_df[y_ypred_df['y'] == 1]
    num_up_preds = len(up_preds)
    num_up_actuals = len(up_actuals)
    correct_up_preds = up_preds[up_preds['y'] == 1]
    num_correct_up_preds = len(correct_up_preds)


    metrics['accuracy'] = sk_metrics.balanced_accuracy_score(y_ypred_df['y'], y_ypred_df['y_pred'])

    if (num_up_preds > 0) and (num_up_actuals > 0):
        metrics['up_tp_ratio'] = num_correct_up_preds / num_up_preds
        metrics['up_capture_ratio'] = num_correct_up_preds / num_up_actuals
    else:
        print('CANNOT COMPUTE UP ACCURACY')


    down_preds = y_ypred_df[y_ypred_df['y_pred'] == -1]
    down_actuals = y_ypred_df[y_ypred_df['y'] == -1]
    num_down_preds = len(down_preds)
    num_down_actuals = len(down_actuals)
    correct_down_preds = down_preds[down_preds['y'] == -1]
    num_correct_down_preds = len(correct_down_preds)

    if (num_down_preds > 0) and (num_down_actuals > 0):
        metrics['down_tp_ratio'] = num_correct_down_preds / num_down_preds
        metrics['down_capture_ratio'] = num_correct_down_preds / num_down_actuals
    else:
        print('CANNOT COMPUTE DOWN ACCURACY')


    neutral_preds = y_ypred_df[y_ypred_df['y_pred'] == 0]
    neutral_actuals = y_ypred_df[y_ypred_df['y'] == 0]
    num_neutral_preds = len(neutral_preds)
    num_neutral_actuals = len(neutral_actuals)
    correct_neutral_preds = neutral_preds[neutral_preds['y'] == 0]
    num_correct_neutral_preds = len(correct_neutral_preds)

    if (num_neutral_preds > 0) and (num_neutral_actuals > 0):
        metrics['neutral_tp_ratio'] = num_correct_neutral_preds / num_neutral_preds
        metrics['neutral_capture_ratio'] = num_correct_neutral_preds / num_neutral_actuals
    else:
        print('CANNOT COMPUTE NEUTRAL ACCURACY')

    y = y_ypred_df['y']
    y_pred = y_ypred_df['y_pred']

    metrics['f1_weighted'] = sk_metrics.f1_score(y, y_pred, average='weighted')
    f1_scores = sk_metrics.f1_score(y, y_pred, average=None)

    print('F1 SCORE:')
    print(f1_scores)

    if len(f1_scores) == 3:
        metrics['up_f1'] = f1_scores[2]
        metrics['down_f1'] = f1_scores[0]
        metrics['neutral_f1'] = f1_scores[1]
    elif len(f1_scores) == 2:
        metrics['up_f1'] = f1_scores[1]
        metrics['down_f1'] = f1_scores[0]
        #metrics['neutral_f1'] = 0

    return metrics



class MinMaxNormalizer:
    """Normalize a single tensor using min-max"""

    def __init__(self, ub: float = 1.0, lb: float = 0.0):
        self.min = None
        self.max = None
        self.ub = ub
        self.lb = lb

    def normalize(self, tensor: np.ndarray, axis: tuple = (0,)):
        self._calc_min_max(tensor, axis)
        return self._normalize(tensor)

    def _calc_min_max(self, tensor: np.ndarray, axis: tuple):
        self.min = tensor.min(axis=axis, keepdims=True)
        self.max = tensor.max(axis=axis, keepdims=True) + 1e-8

    def _normalize(self, tensor: np.ndarray):
        return (tensor - self.min) / (self.max - self.min) * (
            self.ub - self.lb) + self.lb
