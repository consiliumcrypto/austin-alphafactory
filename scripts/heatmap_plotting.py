'''
#Get all train and validation losses for each

select asset_pair,sampling_period , num_steps,epoch_num,   training_loss ,  validation_loss  from datasets d LEFT JOIN models m on (m.dataset_id = d.id) LEFT JOIN epochs e on (e.model_id = m.id);
WHERE asset_pair=''



#GET minimum validation loss for each

select asset_pair,sampling_period , num_steps, model_id, epoch_num, training_loss, validation_loss from(
 select *, (SELECT MIN(validation_loss) FROM epochs where poot.model_id = epochs.model_id) min_val_loss from (
   select asset_pair,sampling_period , num_steps, m.id as model_id, epoch_num, training_loss, validation_loss
   from datasets d
   LEFT JOIN models m on (m.dataset_id = d.id)
   LEFT JOIN epochs e on (e.model_id = m.id)
   )poot)pootxh where validation_loss = min_val_loss;



'''
import plotly
from src.DatabaseManager import *
from plotly import graph_objs as go
#from plotly.subplots import make_subplots
import plotly.tools as tls

kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
      'database': 'final_model_results',
      'ssl': {'ca': '/Users/austin/Documents/consilium/db_creds/server-ca.pem',
              'key': '/Users/austin/Documents/consilium/db_creds/client-key.pem',
              'cert': '/Users/austin/Documents/consilium/db_creds/client-cert.pem', 'check_hostname': False},
      'port': 3306}

db_manager=DatabaseManager (kwargs, 'test', use_dict_cursor=True )

#Get all asset pairs in DB
sql = "SELECT DISTINCT asset_pair FROM datasets"
asset_pairs = db_manager.fetch_mysql_result(sql)
asset_pairs = [x['asset_pair'] for x in asset_pairs]

#Get all different sampling periods in DB
sql = "SELECT DISTINCT sampling_period FROM datasets"
sampling_periods = db_manager.fetch_mysql_result(sql)
sampling_periods = [x['sampling_period'] for x  in sampling_periods]
sampling_periods = [int(x.split('T')[0]) for x in sampling_periods] #Remove 'T' from each so they can be sorted
sampling_periods.sort()
sampling_periods = [(str(x) + 'T') for x in sampling_periods]
print (sampling_periods)

#Get all different "num_steps" in DB
sql = "SELECT DISTINCT num_steps FROM models"
num_steps = db_manager.fetch_mysql_result(sql)
num_steps = [x['num_steps'] for x  in num_steps]
num_steps_labels = [(str(x) + ' Steps') for x in num_steps]

'''
for asset_pair in asset_pairs:
    for sampling_period in sampling_periods:
        print (asset_pair)

        sql = "select asset_pair, sampling_period, num_steps,epoch_num, training_loss, validation_loss FROM datasets d LEFT JOIN models m on (m.dataset_id = d.id) LEFT JOIN epochs e on (e.model_id = m.id) WHERE asset_pair='" + asset_pair + "'"
        sql += " WHERE asset_pair='" + asset_pair + "' AND sampling_period="
        print (db_manager.fetch_mysql_result(sql))

        exit()
'''


sql = """select asset_pair,sampling_period , num_steps, model_id, epoch_num, training_loss, validation_loss from(
             select *, (SELECT MIN(validation_loss) FROM epochs where poot.model_id = epochs.model_id) min_val_loss from (
               select asset_pair,sampling_period , num_steps, m.id as model_id, epoch_num, training_loss, validation_loss
               from datasets d
               LEFT JOIN models m on (m.dataset_id = d.id)
               LEFT JOIN epochs e on (e.model_id = m.id)
               )poot)pootxh where validation_loss = min_val_loss;"""


lowest_val_losses = db_manager.fetch_mysql_result(sql)


data_dict = {}

##### Initialize dictionaries #####
for asset_pair in asset_pairs:
    data_dict[asset_pair] = dict()
    for sampling_period in sampling_periods:
        data_dict[asset_pair][sampling_period] = dict()

#Store results from DB query into dictionary
for row in lowest_val_losses:
    data_dict[row['asset_pair']][row['sampling_period']][row['num_steps']] = row['validation_loss']

plot_data = []


all_fig = tls.make_subplots(rows=len(asset_pairs), cols=1)
plot_row = 1
for asset, v1 in data_dict.items():
    print (asset)
    losses = []

    for sample_p, v2 in v1.items():
        losses.append(list(v2.values()))


    trace = go.Heatmap(
                        z=losses,
                        x=num_steps_labels,
                        y=sampling_periods,
                        name=asset
                          )
    all_fig.append_trace(trace, row=plot_row, col=1)
    plot_row+=1
    fig=go.Figure(data=[trace])
    plotly.offline.plot(fig, auto_open=True, filename='results/'+asset+'_heatmap.html')

plotly.offline.plot(all_fig, auto_open=True, filename='results/all_assets_heatmap.html')









'''
fig = {'data':[go.Heatmap(
                    z=losses,
                    x=num_steps_labels,
                    y=sampling_periods
                      )]}

plotly.offline.plot(fig, auto_open=True, filename='results/'+asset+'_test_heatmap.html')


'''
