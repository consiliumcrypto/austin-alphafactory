import pandas as pd
from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh import extract_features
from src.api_utils import *
import platform

if platform.system() == 'Linux':
    PATH_TO_GS_CREDS = '/home/ubuntu/austin-alphafactory/credentials/cc-master-storage-restricted-readonly.json'
else:
    PATH_TO_GS_CREDS = '/Users/austin/Documents/consilium/bitbucket/austin-alphafactory/credentials/cc-master-storage-restricted-readonly.json'



df = pd.read_csv('candles.csv', parse_dates=True, index_col=0, header=0)
df['id'] = 0

rolled_df = roll_time_series(df, column_id="id")
rolled_df.to_csv('tsfresh-rolled.csv')


extracted_rolled_features = extract_features(rolled_df, column_id='id')

extracted_rolled_features.to_csv('tsfresh-rolled-features.csv')


'''
#Unsure how to use this properly with the target variable
from tsfresh.utilities.dataframe_functions import make_forecasting_frame

forecast_frame = make_forecasting_frame(df, kind=None, rolling_direction=1, max_timeshift=None)

forecast_frame.to_csv('forecast-frame.csv')
'''

'''
#Pull ETH_USDT candles from google storage
candles_df = get_trade_candles(exchange='binance', asset='ETH_USDT', date_from='2020-09-01', date_to='2021-02-06',
                                       candle_type='bars', resolution='1H', path_to_creds=PATH_TO_GS_CREDS,
                                       offset=None)
candles_df.to_csv('candles.csv')
'''
