from src.cc_ts_utils import *
from src.model_factory import ModelFactory
from sklearn.metrics import confusion_matrix
import pandas as pd
import numpy as np
import json

################################################################################
# Results from manual review of "model_runner.py" outputs
# Data scientist to look at plots and choose model and shift parameters manually
################################################################################
SHIFT_PERIOD = 48
MODEL_NAME = 'classifier_gnb'
SCORER_NAME = 'f1_score'
################################################################################
# Data config variables
################################################################################

RESULTS_CONFIG_FILE = 'results/bnb_btc_1H_final-results_target_momentum.json'
with open(RESULTS_CONFIG_FILE, 'r') as f:
    results_dict = json.load(f)

TEST_SET_PERC = results_dict['metadata']['test_set_perc']
DEV_SET_PERC = results_dict['metadata']['dev_set_perc']

#Retrieve shifted data file
for i in results_dict['results']:
    if i['shift_period'] == SHIFT_PERIOD and i['model_name'] == MODEL_NAME:
        raw_data_file = i['input_file']
        best_feature_cols = i['final_best_features']
        target_col_name = i['target_col']
        break

index_col_name = results_dict['metadata']['index_col_name']

################################################################################

raw_ohlcv_df = pd.read_csv(raw_data_file, header=0, parse_dates=[0], index_col=index_col_name)
ohlcv_columns = list(raw_ohlcv_df.columns.values)
target_col_df = raw_ohlcv_df[target_col_name]

########################## RAW TRAIN, DEV, TEST SETS #######################
X_train_raw_ohlcv_df, X_dev_raw_ohlcv_df, X_test_raw_ohlcv_df = train_dev_test_split(raw_ohlcv_df, dev_perc=DEV_SET_PERC, test_perc=TEST_SET_PERC)

#################### TA Features TRAIN, DEV, TEST SETS #####################
features_TA_train_df = compute_all_technicals(X_train_raw_ohlcv_df.copy())
features_TA_dev_df = compute_all_technicals(X_dev_raw_ohlcv_df.copy())
features_TA_test_df = compute_all_technicals(X_test_raw_ohlcv_df.copy())

#Drop NaNs
features_TA_train_df = features_TA_train_df.dropna()
features_TA_dev_df = features_TA_dev_df.dropna()
features_TA_test_df = features_TA_test_df.dropna()

############################################################################
#Create target variable DFs from TA dfs this is crucial so that features_TA DFs have same legnth as target DF
y_train_target_col_df = features_TA_train_df[target_col_name]
y_dev_target_col_df = features_TA_dev_df[target_col_name]
y_test_target_col_df = features_TA_test_df[target_col_name]

all_cols = set(list(features_TA_train_df.columns.values))
remove_cols = all_cols - set(best_feature_cols)

features_TA_train_df = features_TA_train_df.drop(remove_cols, axis=1)
features_TA_dev_df = features_TA_dev_df.drop(remove_cols, axis=1)
features_TA_test_df = features_TA_test_df.drop(remove_cols, axis=1)
############################################################################


################################################################################
# TODO:   OPTIMIZE HYPERPARAMETERS
################################################################################

#NOTE: Since results show "gnb_classifier" is the best fit, no need to optimize any hyperparameters

'''
param_grid = [
  {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
  {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']},
 ]

temp_X_train = features_TA_train_df.copy()
temp_X_dev = features_TA_dev_df.copy()

for i in param_grid:
    for c in i['C']:
        for k in i['kernel']:
            if 'gamma' in i:
                for g in i['gamma']:
                    print ('Kernel: ' + k + ' C:' + str(c) + ' Gamma:' + str(g))
                    svc_model = SVC(gamma=g, C=c, kernel=k)
                    svc_model.fit(temp_X_train, y_train_target_col_df)
                    preds = svc_model.predict(temp_X_dev)

                    score = f1_score(y_dev_target_col_df, preds)
                    print ('F1 Score: ' + str(score))
                else:
                    print ('Kernel: ' + k + ' C:' + str(c))

                    svc_model = SVC(C=c, kernel=k)
                    svc_model.fit(temp_X_train, y_train_target_col_df)
                    preds = svc_model.predict(temp_X_dev)

                    score = f1_score(y_dev_target_col_df, preds)
                    print ('F1 Score: ' + str(score))
'''

################################################################################
# Final out-of-sample test
################################################################################
temp_X_test = features_TA_test_df.copy()

#Combine train and dev sets together for final training
temp_X_traindev = pd.concat([features_TA_train_df.copy(), features_TA_dev_df.copy()])
temp_y_traindev = pd.concat([y_train_target_col_df.copy(), y_dev_target_col_df.copy()])

mf = ModelFactory()

#Train model on combined train/dev set, make predictions on test set
model = mf.get_model(MODEL_NAME)
model.fit(temp_X_traindev, temp_y_traindev)
preds = model.predict(temp_X_test)

#Get final model performance score
score = mf.get_score(SCORER_NAME, y_test_target_col_df, preds)
print ('F1 Score: ' + str(score))
print (confusion_matrix(y_test_target_col_df, preds))
