from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.GoogleStorageClient import GoogleStorageClient

from keras.models import load_model
import tensorflow as tf

#from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MinMaxScaler

import pandas as pd
import numpy
import json
import hashlib
import datetime
import time
import csv

import plotly
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot

from tensorflow.python.client import device_lib

import os
dirname = os.path.dirname(__file__)


'''
1. Get all models where model_status is "COMPLETE"
2. Get the dataset google storage link for each model
3. Pull dataset from google storage to local
4. Pull model from google storage to local
5. Get all model params from models table (batch size, num_steps, train/dev/test split sizes)
6. Generate predictions for train/dev
7. Measure all performance metrics for train/dev
8. Upload performance results to DB
'''


################################################################################
# Data config variables
################################################################################

numpy.random.seed(7)


db_conn=DatabaseConnection("35.239.75.139" ,"root" ,"Opjtftpln6HdPK8f", "final_model_results")
db_manager=DatabaseManager (db_conn, 'test', use_dict_cursor=True)


#TODO: Change to proper query
#sql = "SELECT * FROM models WHERE (model_status='COMPLETE' AND performance_measured='WAITING')"

#TODO: Select models where COUNT(select * epochs ) < models.max_epochs

#sql = "SELECT * FROM models WHERE (model_status='COMPLETE' AND id=76)"


sql = "select m.* from models m where m.id in" \
      "(select model_id from " \
      "(select model_id, count(*) as epochs_trained, (Select max_epochs from models where models.id = epochs.model_id) as max_epochs " \
      "from epochs group by model_id) foo " \
      "where epochs_trained < max_epochs);"

models_to_train = db_manager.fetch_mysql_result(sql)

print(models_to_train)
print(len(models_to_train))


if len(models_to_train) == 0:
    print('No eligible models in database')
    send_text_msg ('5146547219', 'Done scoring all models :)')
    time.sleep(120)
    shutdown_server()
    exit()
else:
    model_dict = random.choice(models_to_train)

model_id = model_dict['id']
print('Model ID:')
print (model_id)


sql = "UPDATE models SET model_status = 'RUNNING' WHERE id = '" + str(model_id) + "'"
db_manager.update_database(sql)


batch_size = model_dict['batch_size']
num_steps = model_dict['num_steps']
dataset_id = model_dict['dataset_id']
max_epochs = model_dict['max_epochs']
model_type = model_dict['model_type']
model_filename_gs = model_dict['model_file_gs']

#NOTE: This only works when running locally!!!!
model_filename_local = model_filename_gs

#Percentage split of dataset of train/dev/test
train_set_perc = model_dict['train_set_perc']
dev_set_perc = model_dict['dev_set_perc']
test_set_perc = model_dict['test_set_perc']

# Pull dataset from from google storage to local
#############################
sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) +  "'"
dataset_dict = db_manager.fetch_mysql_result(sql)[0]
bucket_name_gs = dataset_dict['bucket_name_gs']
print(bucket_name_gs)
google_storage_obj = GoogleStorageClient(bucket_name_gs)
#############################

# Pull model from from google storage to local
#############################
google_storage_obj.download_blob_to_dest(model_filename_gs, model_filename_local)
#############################

# Pull processed datasets from from google storage to local
#############################
datasets = ['TRAIN_X', 'DEV_X']

for i in datasets:
    filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + i + '.npy'
    google_storage_obj.download_blob_to_dest(filename, filename)

datasets = ['TRAIN_y', 'DEV_y']
for i in datasets:
    filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + i + '.csv'
    google_storage_obj.download_blob_to_dest(filename, filename)

#############################
train_X = np.load('processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TRAIN_X.npy')
with open('processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TRAIN_y.csv', 'r') as f:
    reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
    train_y = list(reader)
    train_y = [x[0] for x in train_y]

#############################

dev_X = np.load('processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_DEV_X.npy')
with open('processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_DEV_y.csv', 'r') as f:
    reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
    dev_y = list(reader)
    dev_y = [x[0] for x in dev_y]



print('Loading model')
print (model_filename_local)
model = load_model(model_filename_local)



train_losses = []
val_losses = []


sql = "SELECT MAX(epoch_num) FROM epochs WHERE model_id='" + model_id +"'"

current_epoch_num = db_manager.fetch_mysql_result(sql)[0]['MAX(epoch_num)']

print (current_epoch_num)
print (max_epochs)

#NOTE: This points to a saved .h5 file with the best PERFORMING model (not necessarily the lowest loss by the loss function used in training)
best_model_file = ''

for i in range(current_epoch_num+1, max_epochs+1):

    start_time = time.time()
    history = model.fit(train_X, train_y, epochs=1, batch_size=batch_size, verbose=2, shuffle=False, validation_data=(dev_X, dev_y))
    #history = model.fit(train_X, train_y, epochs=1, batch_size=batch_size, verbose=2, shuffle=False)
    run_time_seconds = time.time() - start_time

    train_loss = history.history['loss'][0]
    val_loss = history.history['val_loss'][0]

    train_losses.append(train_loss)
    val_losses.append(val_loss)

    sql = "INSERT INTO epochs (model_id, epoch_num, run_time_seconds, training_loss, validation_loss, end_datetime) VALUES ('%s', '%s', '%s', '%s', '%s', NOW())" % (str(model_id), str(i), str(int(run_time_seconds)), str(train_loss), str(val_loss))
    print(db_manager.update_database(sql))

    model.reset_states()

filename = 'models/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '.h5'
local_model_filename = os.path.join (dirname, filename)
model.save(local_model_filename)

try:
	cloud_model_filename = filename
	google_storage_obj.upload_blob_from_file(local_model_filename, cloud_model_filename)
	sql = "UPDATE models SET model_file_gs = '" + cloud_model_filename + "' WHERE id = '" + str(model_id) + "'"
	db_manager.update_database(sql)
except Exception as e:
	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id) + '   dataset id: ' + str(dataset_id))
	print(e)
	sql = "UPDATE models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
	db_manager.update_database(sql)
	exit()



restart_server()
########################################################################################################################################################
########################################################################################################################################################
########################################################################################################################################################







































'''
################################################################################
train_pred_traces = []

trace = go.Scatter(
    y = y_train_pred,
    name = 'Y predicted'
)

train_pred_traces.append(trace)

trace = go.Scatter(
    y = train_y,
    name = 'Train Y'
)

train_pred_traces.append(trace)

pred_fig=dict(data=train_pred_traces)
plotly.offline.plot(pred_fig, auto_open=False, filename='lstm-train-predictions-new.html')
################################################################################








################################################################################
pred_traces = []

trace = go.Scatter(
    y = y_test_pred,
    name = 'Y predicted'
)

pred_traces.append(trace)

trace = go.Scatter(
    y = test_y,
    name = 'Test Y'
)

pred_traces.append(trace)

pred_fig=dict(data=pred_traces)
plotly.offline.plot(pred_fig, auto_open=False, filename='fadi-live-model-lstm-test-predictions-new.html')
################################################################################


pred_traces = []

trace = go.Scatter(
    y = y_test_pred,
    name = 'Y predicted'
)

pred_traces.append(trace)

trace = go.Scatter(
    y = test_y,
    name = 'Test Y'
)

pred_traces.append(trace)

pred_fig=dict(data=pred_traces)
plotly.offline.plot(pred_fig, auto_open=False, filename='fadi-live-model-lstm-test-predictions-new.html')




print ('##################################')
print ('Losses:')
print (train_losses)
print ('##################################')
print ('##################################')
print ('Val Losses:')
#print (val_losses)
print ('##################################')



#train_losses = [x[0] for x in train_losses]
#val_losses = [x[0] for x in val_losses]

print ('LOSSES 2')
print (train_losses)


loss_traces = []

trace = go.Scatter(
    y = train_losses,
    name = 'Train Loss'
)
loss_traces.append(trace)


trace = go.Scatter(
    y = val_losses,
    name = 'Validation Loss'
)

loss_traces.append(trace)

loss_fig=dict(data=loss_traces)
plotly.offline.plot(loss_fig, auto_open=False, filename='lstm-losses.html')
'''




############################################################################
'''
filename = 'results/' + asset_name + '_' + sample_period + '_final-results_' + target_type
with open(filename+'.json', 'w') as f:
    json.dump(final_results_dict, f, indent=3)

plot_results_dict(final_results_dict, filename+'_plot')


send_text_msg ('5146547219', 'Code is done :)')
'''
