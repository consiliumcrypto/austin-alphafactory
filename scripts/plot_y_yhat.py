import pandas as pd
import numpy
import json
import hashlib
import datetime

import plotly
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot


index_col_name = 'date'
filename = 'austin-lstm-june14.csv'

df = pd.read_csv(filename,header=0, parse_dates=[0], index_col=index_col_name)


################################################################################
train_pred_traces = []

trace = go.Scatter(
    y = df['y'],
    name = 'Y Actual'
)

train_pred_traces.append(trace)

trace = go.Scatter(
    y = df['yhat'],
    name = 'Y predicted'
)

train_pred_traces.append(trace)

pred_fig=dict(data=train_pred_traces)
plotly.offline.plot(pred_fig, auto_open=False, filename='lstm-yhat-y-june14.html')
################################################################################
