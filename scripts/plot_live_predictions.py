import pandas as pd
import plotly
import plotly.graph_objs as go
from plotly.offline import plot, iplot
from src.api_utils import *

def calc_price_from_returns (row):
    price = float(row['price_open']) + (float(row['price_open']) * float(row['PercentReturnPredicted']))
    return price



CONSILIUM_API_KEY = 'cqzsjksnF1uBXfghhUZKhkwehRMrPU3DL2QAJ9KXaus'
PRED_INDEX_COL = 'Candle'


predictions_df = pd.read_csv('data/live_predictions/lstm_results.csv', header=0, index_col=PRED_INDEX_COL)
all_exchanges = list(predictions_df.Exchange.unique())

global_datetime_start = predictions_df.index.min()
global_datetime_end = predictions_df.index.max()

#Final format of this dict will be prediction_dfs[exchange][pair][resolution][model] = df
prediction_dfs = dict()
#Final format of this dict will be candle_dfs[exchange][pair][resolution] = df
candle_dfs = dict()


for exchange in all_exchanges:
    prediction_dfs[exchange] = dict()
    candle_dfs[exchange] = dict()

    temp_df_exchange = predictions_df[predictions_df['Exchange'] == exchange]

    all_pairs = list(temp_df_exchange.Pair.unique())

    for pair in all_pairs:
        prediction_dfs[exchange][pair] = dict()
        candle_dfs[exchange][pair] = dict()

        temp_df_pair = temp_df_exchange[temp_df_exchange['Pair'] == pair]
        all_candle_periods = list(temp_df_pair.Resolution.unique())

        for candle_period in all_candle_periods:
            if candle_period in candle_dfs[exchange][pair]:
                print ('Duplicate exchange/pair:  ' + pair)
                print ('Duplicate exchange/pair:  ' + candle_period)
            else:
                candle_df = get_bar_candles(exchange, pair, global_datetime_start, global_datetime_end, CONSILIUM_API_KEY, 'bars', candle_period)
                candle_dfs[exchange][pair][candle_period] = candle_df

            prediction_dfs[exchange][pair][candle_period] = dict()
            temp_df_periods = temp_df_pair[temp_df_pair['Resolution'] == candle_period]

            all_models = list(temp_df_periods.ModelName.unique())

            for model in all_models:
                temp_df_model = temp_df_periods[temp_df_periods['ModelName'] == model]
                temp_df_model =  pd.merge(candle_dfs[exchange][pair][candle_period], temp_df_model, left_index=True, right_index=True)
                temp_df_model['predicted_price'] = temp_df_model.apply(calc_price_from_returns, axis=1)
                temp_df_model['shifted_price_open'] = temp_df_model['price_open'].shift(-1)
                prediction_dfs[exchange][pair][candle_period][model] = temp_df_model.dropna()

                traces = []

                trace = go.Scatter(
                x = temp_df_model.index,
                y = temp_df_model['predicted_price'],
                name = pair + ' Predicted Price'
                )
                traces.append(trace)

                trace = go.Scatter(
                x = temp_df_model.index,
                y = temp_df_model['shifted_price_open'],
                name = pair + ' Actual Price'
                )
                traces.append(trace)

                fig=dict(data=traces)
                plotly.offline.plot(fig, auto_open=False, filename='results/live_prediction_plots/' + pair + '_' + candle_period + '_' + model + '.html')





'''

    trace = go.Scatter(
    x = diff_loan_profits_15D.index,
    y = diff_loan_profits_15D,
    name = pair + ' DIFF Loan Profits (BTC) for 15D'
    )
    diff_loan_profits_15D_traces.append(trace)





combined_df = pd.merge(ohlcv_df, preds_df, on=[dataset_index_col])
combined_df['shifted_price_open'] = combined_df['price_open'].shift(-1)


def calc_price_from_returns (row):
    price = float(row['price_open']) + (float(row['price_open']) * float(row['prediction']))
    return price
'''
