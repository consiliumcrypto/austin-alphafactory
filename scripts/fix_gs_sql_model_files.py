from src.cc_ts_utils import *
from src.GoogleStorageClient import *
from src.DatabaseManager import *


'''
*** 1. Get list of all model files in GS ***
2. Loop through list, extracting model_id
3. Select 'model_file_gs' field of that model in sql
4. Check if model filename from GS matches 'model_filename_gs' in SQL
5. If not, set 'model_filename_gs' to the model filename from GS

'''


BUCKET_NAME = 'consilium-austin-data'


db_conn=DatabaseConnection("35.239.75.139" ,"root" ,"Opjtftpln6HdPK8f", "final_model_results")
db_manager=DatabaseManager (db_conn, 'test', use_dict_cursor=True)


google_storage_obj = GoogleStorageClient(BUCKET_NAME)

#res = list_blobs_with_prefix(BUCKET_NAME, 'models')

all_model_filenames = google_storage_obj.list_blobs(prefix='models')
all_model_filenames = all_model_filenames[1:] #Drop first element in list (folder name, not a file)

#model_id_256_dataset_id_13.h5

for m_filename in all_model_filenames:
    m_id = m_filename.split('model_id_')[1].split('_')[0]

    sql = "SELECT `model_file_gs` FROM models WHERE id='" + str(m_id) + "'"
    model_filename_sql = db_manager.fetch_mysql_result(sql)[0]

    if model_filename_sql['model_file_gs'] == None:
        sql = "UPDATE models SET `model_file_gs` = '" + m_filename + "' WHERE id = '" + m_id + "'"
        db_manager.update_database(sql)

    elif model_filename_sql['model_file_gs'] == m_filename:
        print ('GOOD GOOD GOOD')
