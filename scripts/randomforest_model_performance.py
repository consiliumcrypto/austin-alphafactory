from src.cc_ts_utils import *
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *
from src.enum_classes import *

import pandas as pd
import numpy
import json
import hashlib
import datetime
import time
import random
import csv
import platform
import os

dirname = os.path.dirname(__file__)

################################################################################
# Establish connection to database
if platform.system() == 'Linux':
    # Establish connection to database
    with open('credentials/server_sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

else:
    # Establish connection to database
    with open('credentials/sql_db_creds.txt') as f:
        sql_db_creds = f.readlines()
        sql_db_creds = [x.strip() for x in sql_db_creds]

kwargs = {'host': sql_db_creds[0], 'user': sql_db_creds[1], 'password': sql_db_creds[2],
      'database': sql_db_creds[7],
      'ssl': {'ca': sql_db_creds[4],
              'key': sql_db_creds[5],
              'cert': sql_db_creds[6], 'check_hostname': False},
      'port': int(sql_db_creds[3])}

db_manager=DatabaseManager (kwargs, 'randomforest_launcher.py', use_dict_cursor=True )


BUCKET_NAME_GS = 'consilium-austin-alphafactory-march2020'

try:
    google_storage_obj = GoogleStorageClient(BUCKET_NAME_GS)
except Exception as e:
    print('Google storage exception!')
    print (e)
    send_text_msg ('5146547219', 'Code FAILED :( ')
    exit()


while True:
    start_time = time.process_time()

    ################################################################################
    sql = "SELECT * FROM random_forest_models WHERE (model_status='COMPLETE' AND performance_measured='WAITING')"

    finished_models = db_manager.fetch_mysql_result(sql)

    if len(finished_models) == 0:
    	print('No eligible models to score in database')
    	send_text_msg ('5146547219', 'Done scoring all models :)')
    	time.sleep(300)
    	shutdown_server()
    	exit()
    else:
    	model_to_run = random.choice(finished_models)


    ################################################################################
    #Get all relevant model parameters
    model_id = model_to_run['id']
    #train_predictions_file_gs = model_to_run['train_predictions_file_gs']
    #dev_predictions_file_gs = model_to_run['dev_predictions_file_gs']

    sql = "UPDATE random_forest_models SET performance_measured = 'RUNNING' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)

    ################################################################################

    for split in ['train', 'dev']:
        filename_gs = model_to_run[split + '_predictions_file_gs']
        filename_local = os.path.join(dirname, filename_gs)

        ##### LOAD FILE FROM GOOGLE STORAGE, SAVE LOCALLY #####
        try:
            #google_storage_obj = GoogleStorageClient(bucket_name_gs)
            google_storage_obj.download_blob_to_dest(filename_gs, filename_local)
        except Exception as e:
        	print('Google storage exception!')
        	print (e)
        	sql = "UPDATE random_forest_models SET model_status = 'WAITING' WHERE id = '" + str(model_id) + "'"
        	db_manager.update_database(sql)
        	send_text_msg ('5146547219', 'Code FAILED :(  model id: ' + str(model_id))
        	exit()

        y_ypred_df = pd.read_csv(filename_local, header=0, index_col='index')

        performance_metrics_dict = calc_classifier_performance(y_ypred_df)


        sql = "INSERT INTO classifier_performance (model_id,dataset_split,"

        for k in performance_metrics_dict.keys():
            sql += str(k) + ","

        sql = sql[:-1] #Remove "," comma from last entry

        sql += ") VALUES ("
        sql += str(model_id) + ",'" + split.upper() + "',"

        for v in performance_metrics_dict.values():
            sql += str(v) + ","

        sql = sql[:-1] #Remove "," comma from last entry
        sql += ")"

        db_manager.update_database(sql)

        os.remove(filename_local)



    sql = "UPDATE random_forest_models SET performance_measured = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
    db_manager.update_database(sql)
    elapsed_time = time.process_time() - start_time
    print ('Run time for performance:  ' + str(elapsed_time))
