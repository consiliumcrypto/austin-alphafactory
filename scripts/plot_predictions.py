from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.GoogleStorageClient import GoogleStorageClient

import plotly
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot



'''

1. Pull predictions
2. Pull OHLCV
3. Create comined DF
4. For reach row of DF, calculate predicted price (using predicted pct_return and open)
5.
6.

'''

## 22 (poor), 58 (good)
MODEL_ID = 121
DATABASE_NAME = 'model_results_gdax'


if platform.system() == 'Linux':
	print ('Connecting on linux server')
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
	'database': DATABASE_NAME,
	'ssl': {'ca': '/home/ubuntu/db_creds/server-ca.pem',
	'key': '/home/ubuntu/db_creds/client-key.pem',
	'cert': '/home/ubuntu/db_creds/client-cert.pem', 'check_hostname': False},
	'port': 3306}
else:
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
	'database': DATABASE_NAME,
	'ssl': {'ca': '/Users/austin/Documents/consilium/db_creds/server-ca.pem',
	'key': '/Users/austin/Documents/consilium/db_creds/client-key.pem',
	'cert': '/Users/austin/Documents/consilium/db_creds/client-cert.pem', 'check_hostname': False},
	'port': 3306}

db_manager=DatabaseManager (kwargs, 'model_performance.py', use_dict_cursor=True )


sql = "SELECT * FROM models WHERE id='" + str(MODEL_ID) + "'"

model_dict = db_manager.fetch_mysql_result(sql)[0]

print (model_dict)

train_predictions_filename = model_dict['train_predictions_file_gs']
dev_predictions_filename = model_dict['dev_predictions_file_gs']


dataset_id = model_dict['dataset_id']

sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) + "'"
dataset_dict = db_manager.fetch_mysql_result(sql)[0]
dataset_index_col = dataset_dict['index_col_name']
target_col_name = dataset_dict['target_col_name']
print(dataset_dict)


#Download original OHLCV dataset and create dataframe
ohlcv_data_filename = dataset_dict['filename_gs']
local_ohlcv_data_filename = 'data/' + ohlcv_data_filename
bucket_name_gs = dataset_dict['bucket_name_gs']
google_storage_obj = GoogleStorageClient(bucket_name_gs)
google_storage_obj.download_blob_to_dest(ohlcv_data_filename, local_ohlcv_data_filename)

ohlcv_df = pd.read_csv(local_ohlcv_data_filename, index_col=dataset_index_col, header=0, parse_dates=[0])


#Download predictions and create dataframe
bucket_name_gs = model_dict['bucket_name_gs']
google_storage_obj = GoogleStorageClient(bucket_name_gs)
google_storage_obj.download_blob_to_dest(dev_predictions_filename, dev_predictions_filename)

preds_df = pd.read_csv(dev_predictions_filename, names=[dataset_index_col, 'prediction'], index_col=dataset_index_col, parse_dates=[0])

print (preds_df.head())


combined_df = pd.merge(ohlcv_df, preds_df, on=[dataset_index_col])
combined_df['shifted_price_open'] = combined_df['price_open'].shift(-1)


def calc_price_from_returns (row):
    price = float(row['price_open']) + (float(row['price_open']) * float(row['prediction']))
    return price


combined_df['predicted_price'] = combined_df.apply(calc_price_from_returns, axis=1)


combined_df.to_csv('combined_df.csv')


print (combined_df.head())


traces = []

trace = go.Scatter(
x = combined_df.index,
y = combined_df['shifted_price_open'],
name = 'Actual Price'
)

traces.append(trace)

trace = go.Scatter(
x = combined_df.index,
y = combined_df['predicted_price'],
name = 'Predicted Price'
)

traces.append(trace)

pred_fig=dict(data=traces)
plotly.offline.plot(pred_fig, auto_open=False, filename='DEV_price_predictions.html')
