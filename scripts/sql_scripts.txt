

INSERT INTO `datasets` (`id`, `sampling_type`, `sampling_period`, `target_type`, `target_col_name`, `prediction_horizon_periods`, `asset_pair`, `exchange`, `start_datetime`, `end_datetime`, `filename_gs`, `file_hash`, `bucket_name_gs`, `index_col_name`)
VALUES
	(1, 'TIME_BARS', '5T', 'PERCENT_RETURN', 'target_perc_return_3', 3, 'BTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/BTC_USDT_binance_5T_time_3P_target_perc_return.csv', 'df6ef4c2e1a43c0f2274daca541e3829', 'consilium-austin-data', 'time_period_start'),
	(2, 'TIME_BARS', '3T', 'PERCENT_RETURN', 'target_perc_return_5', 5, 'BTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/BTC_USDT_binance_3T_time_5P_target_perc_return.csv', 'a25a3ef6e0a556d592da3c109a702db9', 'consilium-austin-data', 'time_period_start'),
	(3, 'TIME_BARS', '1T', 'PERCENT_RETURN', 'target_perc_return_15', 15, 'BTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/BTC_USDT_binance_1T_time_15P_target_perc_return.csv', '803b0e8eb0ddd3ca3279489de46a8b82', 'consilium-austin-data', 'time_period_start'),
	(4, 'TIME_BARS', '15T', 'PERCENT_RETURN', 'target_perc_return_1', 1, 'BTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/BTC_USDT_binance_15T_time_1P_target_perc_return.csv', '20c18eee5e3b91ed6df29d0b01bb80e3', 'consilium-austin-data', 'time_period_start'),
	(5, 'TIME_BARS', '5T', 'PERCENT_RETURN', 'target_perc_return_3', 3, 'ETH_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/ETH_USDT_binance_5T_time_3P_target_perc_return.csv', '26e999f4fe2987aa1b401564b318a99a', 'consilium-austin-data', 'time_period_start'),
	(6, 'TIME_BARS', '3T', 'PERCENT_RETURN', 'target_perc_return_5', 5, 'ETH_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/ETH_USDT_binance_3T_time_5P_target_perc_return.csv', '8097ea550cbc06ecca261e5aaace21d0', 'consilium-austin-data', 'time_period_start'),
	(7, 'TIME_BARS', '1T', 'PERCENT_RETURN', 'target_perc_return_15', 15, 'ETH_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/ETH_USDT_binance_1T_time_15P_target_perc_return.csv', 'c253920f7ed6327e8b70c5a9fceec086', 'consilium-austin-data', 'time_period_start'),
	(8, 'TIME_BARS', '15T', 'PERCENT_RETURN', 'target_perc_return_1', 1, 'ETH_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/ETH_USDT_binance_15T_time_1P_target_perc_return.csv', '5ac58cdb6a50fd3f17a1a638c6e506e8', 'consilium-austin-data', 'time_period_start'),
	(9, 'TIME_BARS', '5T', 'PERCENT_RETURN', 'target_perc_return_3', 3, 'EOS_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/EOS_USDT_binance_5T_time_3P_target_perc_return.csv', 'bf1b2fde749284ba0e92ce85d48d85df', 'consilium-austin-data', 'time_period_start'),
	(10, 'TIME_BARS', '3T', 'PERCENT_RETURN', 'target_perc_return_5', 5, 'EOS_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/EOS_USDT_binance_3T_time_5P_target_perc_return.csv', '2cae6661e08670c7c1eb28a688d56406', 'consilium-austin-data', 'time_period_start'),
	(11, 'TIME_BARS', '1T', 'PERCENT_RETURN', 'target_perc_return_15', 15, 'EOS_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/EOS_USDT_binance_1T_time_15P_target_perc_return.csv', 'c25b7797cf14965a0801a2413955aa64', 'consilium-austin-data', 'time_period_start'),
	(12, 'TIME_BARS', '15T', 'PERCENT_RETURN', 'target_perc_return_1', 1, 'EOS_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/EOS_USDT_binance_15T_time_1P_target_perc_return.csv', '03a2bfd24d5668c8faf77808f44869c0', 'consilium-austin-data', 'time_period_start'),
	(13, 'TIME_BARS', '5T', 'PERCENT_RETURN', 'target_perc_return_3', 3, 'LTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/LTC_USDT_binance_5T_time_3P_target_perc_return.csv', 'cfc39b771b0ec0fe3a14baeabaa1d2a1', 'consilium-austin-data', 'time_period_start'),
	(14, 'TIME_BARS', '3T', 'PERCENT_RETURN', 'target_perc_return_5', 5, 'LTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/LTC_USDT_binance_3T_time_5P_target_perc_return.csv', 'afd935eee763efe0e1e95438f9a897e8', 'consilium-austin-data', 'time_period_start'),
	(15, 'TIME_BARS', '1T', 'PERCENT_RETURN', 'target_perc_return_15', 15, 'LTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/LTC_USDT_binance_1T_time_15P_target_perc_return.csv', 'f56727411895fd86e98fd238e562cfcd', 'consilium-austin-data', 'time_period_start'),
	(16, 'TIME_BARS', '15T', 'PERCENT_RETURN', 'target_perc_return_1', 1, 'LTC_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/LTC_USDT_binance_15T_time_1P_target_perc_return.csv', '669bae5409992a5938e7a9b128aae0cd', 'consilium-austin-data', 'time_period_start'),
	(17, 'TIME_BARS', '5T', 'PERCENT_RETURN', 'target_perc_return_3', 3, 'XRP_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/XRP_USDT_binance_5T_time_3P_target_perc_return.csv', '146480e7fd670a65084ee2f8bebf6bce', 'consilium-austin-data', 'time_period_start'),
	(18, 'TIME_BARS', '3T', 'PERCENT_RETURN', 'target_perc_return_5', 5, 'XRP_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/XRP_USDT_binance_3T_time_5P_target_perc_return.csv', '6eb57ebe4ae79abe38da78b4579177fc', 'consilium-austin-data', 'time_period_start'),
	(19, 'TIME_BARS', '1T', 'PERCENT_RETURN', 'target_perc_return_15', 15, 'XRP_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/XRP_USDT_binance_1T_time_15P_target_perc_return.csv', '3c3d1dc360f8053badfeef16e190ebdd', 'consilium-austin-data', 'time_period_start'),
	(20, 'TIME_BARS', '15T', 'PERCENT_RETURN', 'target_perc_return_1', 1, 'XRP_USDT', 'binance', '2018-09-01 00:00:00', '2019-06-02 00:00:00', 'time_bars/target_perc_return/XRP_USDT_binance_15T_time_1P_target_perc_return.csv', '57e5682db758c6cf3b25b22f93cf6cd9', 'consilium-austin-data', 'time_period_start');




DELETE FROM epochs
WHERE model_id in (SELECT id FROM models WHERE (model_status = 'RUNNING') )

UPDATE models SET model_status = 'WAITING'
WHERE model_status = 'RUNNING'


SELECT * FROM epochs
WHERE model_id in (SELECT id FROM models WHERE (model_status = 'RUNNING') )
