import json
from src.target_labeler import TargetLabeler
import multiprocessing as mp
from src.cc_ts_utils import get_bar_candles
from src.GoogleStorageClient import GoogleStorageClient
from src.DatabaseManager import *

import os
dirname = os.path.dirname(__file__)

import hashlib
import platform


################################################################################
# Cloud storage variables
################################################################################
GOOGLE_STORAGE_BUCKET_NAME = 'testing-consilium-austin-data'
google_storage_obj = GoogleStorageClient(GOOGLE_STORAGE_BUCKET_NAME)

if platform.system() == 'Linux':
	print ('Connecting on linux server')
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
		  'database': 'model_results_testing',
		  'ssl': {'ca': '/home/ubuntu/db_creds/server-ca.pem',
				  'key': '/home/ubuntu/db_creds/client-key.pem',
				  'cert': '/home/ubuntu/db_creds/client-cert.pem', 'check_hostname': False},
		  'port': 3306}
else:
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
		  'database': 'model_results_testing',
		  'ssl': {'ca': '/Users/austin/Documents/consilium/db_creds/server-ca.pem',
				  'key': '/Users/austin/Documents/consilium/db_creds/client-key.pem',
				  'cert': '/Users/austin/Documents/consilium/db_creds/client-cert.pem', 'check_hostname': False},
		  'port': 3306}

db_manager=DatabaseManager (kwargs, 'test', use_dict_cursor=True )

################################################################################
################################################################################
# Necessary variables for creating new datasets
################################################################################
#Need to be in "XXX_YYY" or similar standardized consilium format
#ASSET_NAMES = ['BTC_USDT', 'ETH_USDT', 'EOS_USDT', 'LTC_USDT', 'XRP_USDT']
ASSET_NAMES = ['BTC_USDT']
EXCHANGE_NAME = ['binance']
'''
MOMENTUM_DESTINATION_FOLDER = 'data/time_bars/target_momentum'
PRICE_DESTINATION_FOLDER = 'data/time_bars/target_price'
ABS_RETURN_DESTINATION_FOLDER = 'data/time_bars/target_abs_return'
PERC_RETURN_DESTINATION_FOLDER = 'data/time_bars/target_perc_return'
'''


MOMENTUM_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_momentum')
PRICE_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_price')
ABS_RETURN_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_abs_return')
PERC_RETURN_DESTINATION_FOLDER = os.path.join(dirname, 'data/time_bars/target_perc_return/returns_distributions')


#DATASET_PERIODS = [('5T', 3), ('3T', 5), ('1T', 15), ('15T', 1)]
TIME_PERIODS = ['5T', '15T']
TARGET_SHIFT_PERIOD = 1


LABEL_MOMENTUM = False
LABEL_PERC_RETURN = True


TESTING = True
################################################################################

if TESTING:
    date_from = '2019-01-01'
    date_to = '2019-01-14'
else:
    date_from = '2018-09-01'
    date_to = '2019-06-02'
'''
else:
    date_from = '2018-09-01'
    date_to = '2019-04-01'
'''





'''
with mp.Pool(processes=len(mp_args)) as pool:
    labeled_files = pool.map(func=label_all_perc_return, iterable=mp_args)
'''



RAW_DATA_DICTS = list()

for asset_pair in ASSET_NAMES:
    for exchange in EXCHANGE_NAME:
        for sampling_period in TIME_PERIODS:
            local_filename = 'data/original_data/' + asset_pair + '_' + sampling_period + '_' + exchange + '.csv'
            #filename = os.path.join(dirname, filename)
            cloud_filename = 'original_data/' + asset_pair + '_' + sampling_period + '_' + exchange + '.csv'
            print ('SAMPLING PERIOD: ' + sampling_period)
            df = get_bar_candles(exchange, asset_pair, date_from , date_to, 'cqzsjksnF1uBXfghhUZKhkwehRMrPU3DL2QAJ9KXaus', sampling_period)
            print (df.head(25))
            df.to_csv(local_filename)
            google_storage_obj.upload_blob_from_file(local_filename, cloud_filename)
            print ('Saving filename:     ' + local_filename)
            data_dict = {'filename': local_filename, 'asset_name': asset_pair, 'exchange_name': exchange, 'sampling_period': sampling_period, 'col_to_label': 'price_open'}
            RAW_DATA_DICTS.append(data_dict)


print (RAW_DATA_DICTS)
print ('Done pulling data')

################################################################################

def label_all_momentum (args):
    destination_folder = MOMENTUM_DESTINATION_FOLDER

    raw_data_dict = args[0]
    shift_period = args[1]

    data_file = raw_data_dict['filename']
    asset_name = raw_data_dict['asset_name']
    exchange_name = raw_data_dict['exchange_name']
    sampling_period = raw_data_dict['sampling_period']
    col_to_label = raw_data_dict['col_to_label']

    target_labeler = TargetLabeler(data_file, asset_name, exchange_name, sampling_period, shift_period, col_to_label)

    labeled_file = target_labeler.label_momentum(destination_folder)

    return labeled_file



def label_all_perc_return (args):
    destination_folder = PERC_RETURN_DESTINATION_FOLDER

    raw_data_dict = args[0]
    shift_period = args[1]

    data_file = raw_data_dict['filename']
    asset_name = raw_data_dict['asset_name']
    exchange_name = raw_data_dict['exchange_name']
    sampling_period = raw_data_dict['sampling_period']
    col_to_label = raw_data_dict['col_to_label']

    target_labeler = TargetLabeler(data_file, asset_name, exchange_name, sampling_period, shift_period, col_to_label)

    labeled_file, df = target_labeler.label_perc_return(destination_folder)

    return labeled_file, df


################################################################################
mp_args = []
#TODO: Is this even necessary?

for raw_data_dict in RAW_DATA_DICTS:
    for sampling_period in TIME_PERIODS:
        print ('############################')
        print (raw_data_dict['sampling_period'])
        print (sampling_period)
        print ('############################')
        if raw_data_dict['sampling_period'] == sampling_period:
            mp_args.append([raw_data_dict, TARGET_SHIFT_PERIOD])


#######################  LABEL MOMENTUM ##################################
metadata = dict()
metadata['destination_folder'] = MOMENTUM_DESTINATION_FOLDER
metadata['perc_threshold'] = 0.005

metadata['bar_type'] = 'time'
metadata['target_type'] = 'target_momentum'
metadata['index_col_name'] = 'time_period_start'
metadata['labeled_files'] = []


if LABEL_MOMENTUM:
    if len(mp_args) > 0:
        with mp.Pool(processes=len(mp_args)) as pool:
            labeled_files = pool.map(func=label_all_momentum, iterable=mp_args)


        for labeled_file in labeled_files:
            metadata['labeled_files'].append(labeled_file)

        with open(MOMENTUM_DESTINATION_FOLDER + '/' + 'momentum-labeler-metadata.json', 'w') as f:
            json.dump(metadata, f, indent=3)


################################################################################

###################### LABEL PERCENT RETURN ######################

metadata = dict()
metadata['destination_folder'] = PERC_RETURN_DESTINATION_FOLDER
metadata['shift_periods'] = TARGET_SHIFT_PERIOD
metadata['perc_threshold'] = ''

metadata['bar_type'] = 'TIME_BARS'
metadata['target_type'] = 'PERCENT_RETURN'
metadata['index_col_name'] = 'time_period_start'
metadata['labeled_files'] = []




if LABEL_PERC_RETURN:
    print ('LABELING PERCENT RETURN')
    if len(mp_args) > 0:

        with mp.Pool(processes=len(mp_args)) as pool:
            labeled_files = pool.map(func=label_all_perc_return, iterable=mp_args)



        print (labeled_files[0][0])
        print (labeled_files[0][1].head())
        print (labeled_files[1][0])
        print (labeled_files[1][1].head())
        #print (dfs)
        exit()


        for labeled_file in labeled_files:
            metadata['labeled_files'].append(labeled_file)
            print ('LABELED FILE: ' + str(labeled_file))
            google_storage_obj.upload_blob_from_file(labeled_file, labeled_file.split('data/')[1])







        for raw_data_dict in RAW_DATA_DICTS:
            for sampling_period in TIME_PERIODS:
                '''
                print ('############################')
                print (raw_data_dict['sampling_period'])
                print (sampling_period)
                print ('############################')
                '''
                if raw_data_dict['sampling_period'] == sampling_period:
                    print ('############################')
                    print (raw_data_dict)
                    print ('############################')

                    datafile_dict = {}

                    datafile_dict['sampling_period'] = sampling_period
                    datafile_dict['date_from'] = date_from
                    datafile_dict['date_to'] = date_to


                    #TODO: Include hash?
                    datafile_dict['gs_filename'] = 'time_bars/target_perc_return/' + raw_data_dict['asset_name'] + '_' + raw_data_dict['exchange_name'] + '_' + str(raw_data_dict['sampling_period']) + '_time_' + str(TARGET_SHIFT_PERIOD) + 'P_target_perc_return.csv'
                    gs_filename = 'time_bars/target_perc_return/' + raw_data_dict['asset_name'] + '_' + raw_data_dict['exchange_name'] + '_' + str(raw_data_dict['sampling_period']) + '_time_' + str(TARGET_SHIFT_PERIOD) + 'P_target_perc_return.csv'

                    datafile_dict['sampling_type'] = 'TIME_BARS'
                    sampling_type = 'TIME_BARS'

                    datafile_dict['target_type'] = metadata['target_type']
                    target_type = metadata['target_type']

                    datafile_dict['prediction_horizon_periods'] = TARGET_SHIFT_PERIOD
                    prediction_horizon_periods = TARGET_SHIFT_PERIOD

                    datafile_dict['target_col_name'] = 'target_perc_return_' + str(prediction_horizon_periods)
                    target_col_name = 'target_perc_return_' + str(prediction_horizon_periods)

                    datafile_dict['asset_pair'] = raw_data_dict['asset_name']
                    asset_pair = raw_data_dict['asset_name']

                    datafile_dict['exchange_name'] = raw_data_dict['exchange_name']
                    exchange_name = raw_data_dict['exchange_name']
                    #date_from  date_to

                    datafile_dict['bucket_name_gs'] = GOOGLE_STORAGE_BUCKET_NAME
                    bucket_name_gs = GOOGLE_STORAGE_BUCKET_NAME

                    m = hashlib.md5()
                    m.update(str(datafile_dict).encode('utf-8'))
                    file_hash = m.hexdigest()


                    sql = "INSERT INTO datasets (sampling_type, sampling_period, target_type, index_col_name, target_col_name, prediction_horizon_periods, asset_pair, exchange, start_datetime, end_datetime, filename_gs, file_hash, bucket_name_gs) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (str(datafile_dict['sampling_type']), str(datafile_dict['sampling_period']), str(datafile_dict['target_type']), 'time_period_start', str(datafile_dict['target_col_name']), str(datafile_dict['prediction_horizon_periods']), str(datafile_dict['asset_pair']), str(datafile_dict['exchange_name']), str(datafile_dict['date_from']), str(datafile_dict['date_to']), str(datafile_dict['gs_filename']), str(file_hash), str(datafile_dict['bucket_name_gs']))

                    db_manager.update_database(sql)


        exit()

        with open(PERC_RETURN_DESTINATION_FOLDER + '/' + 'perc-return-labeler-metadata.json', 'w') as f:
            json.dump(metadata, f, indent=3)
