from src.cc_ts_utils import *
from src.DatabaseManager import *
from src.GoogleStorageClient import GoogleStorageClient

from keras.models import load_model
import tensorflow as tf

#from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MinMaxScaler

import pandas as pd
import numpy
import json
import hashlib
import datetime
import time
import csv

import os
dirname = os.path.dirname(__file__)


'''
1. Get all models where model_status is "COMPLETE"
2. Get the dataset google storage link for each model
3. Pull dataset from google storage to local
4. Pull model from google storage to local
5. Get all model params from models table (batch size, num_steps, train/dev/test split sizes)
6. Generate predictions for train/dev
7. Measure all performance metrics for train/dev
8. Upload performance results to DB
'''


################################################################################
# Data config variables
################################################################################

#DATABASE_NAME = 'performance_test_model_results_gdax'
DATABASE_NAME = 'austin_march26_2020'

if platform.system() == 'Linux':
	print ('Connecting on linux server')
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
	'database': DATABASE_NAME,
	'ssl': {'ca': '/home/ubuntu/db_creds/server-ca.pem',
	'key': '/home/ubuntu/db_creds/client-key.pem',
	'cert': '/home/ubuntu/db_creds/client-cert.pem', 'check_hostname': False},
	'port': 3306}
else:
	kwargs = {'host': '35.239.75.139', 'user': 'root', 'password': 'Opjtftpln6HdPK8f',
	'database': DATABASE_NAME,
	'ssl': {'ca': '/Users/austin/Documents/consilium/db_creds/server-ca.pem',
	'key': '/Users/austin/Documents/consilium/db_creds/client-key.pem',
	'cert': '/Users/austin/Documents/consilium/db_creds/client-cert.pem', 'check_hostname': False},
	'port': 3306}

db_manager=DatabaseManager(kwargs, 'model_performance.py', use_dict_cursor=True)

sql = "SELECT * FROM models WHERE (model_status='COMPLETE' AND performance_measured='WAITING')"
#sql = "SELECT * FROM models WHERE id='55'"


finished_models = db_manager.fetch_mysql_result(sql)

if len(finished_models) == 0:
	print('No eligible models to score in database')
	send_text_msg ('5146547219', 'Done scoring all models :)')
	time.sleep(300)
	shutdown_server()
	exit()
else:
	model_dict = random.choice(finished_models)

model_id = model_dict['id']
print ('MODEL ID:  ' + str(model_id))

sql = "UPDATE models SET performance_measured = 'RUNNING' WHERE id = '" + str(model_id) + "'"
db_manager.update_database(sql)

batch_size = model_dict['batch_size']
num_steps = model_dict['num_steps']
dataset_id = model_dict['dataset_id']
max_epochs = model_dict['max_epochs']
model_type = model_dict['model_type']
model_filename_gs = model_dict['model_file_gs']
random_seed = model_dict['random_seed']
#NOTE: This only works when running locally!!!!
model_filename_local = model_filename_gs
model_filename_local = os.path.join(dirname, model_filename_local)

#Percentage split of dataset of train/dev/test
train_set_perc = model_dict['train_set_perc']
dev_set_perc = model_dict['dev_set_perc']
test_set_perc = model_dict['test_set_perc']

if random_seed == '':
	numpy.random.seed(7)
else:
	numpy.random.seed(random_seed)

# Pull dataset from from google storage to local
#############################
# Select dataset row from DB, load it as a python dict --> "dataset_dict"
sql = "SELECT * FROM datasets WHERE id='" + str(dataset_id) +  "'"
dataset_dict = db_manager.fetch_mysql_result(sql)[0]

index_col_name = dataset_dict['index_col_name']
target_col_name = dataset_dict['target_col_name']
bucket_name_gs = dataset_dict['bucket_name_gs']
google_storage_obj = GoogleStorageClient(bucket_name_gs)

#NOTE: This is the "raw" dataset that includes OHLCV/standard fields AND the target_label column
dataset_filename_gs = dataset_dict['filename_gs']
dataset_filename_local = 'data/' + dataset_filename_gs
dataset_filename_local = os.path.join(dirname, dataset_filename_local)

google_storage_obj.download_blob_to_dest(dataset_filename_gs, dataset_filename_local)

#############################



# Pull model from from google storage to local
#############################
print('Loading model')
print (model_filename_local)
google_storage_obj.download_blob_to_dest(model_filename_gs, model_filename_local)
model = load_model(model_filename_local)
os.remove(model_filename_local)
#############################

# Pull processed datasets from from google storage to local
#############################
datasets = ['TRAIN_X', 'DEV_X', 'TRAIN_y', 'DEV_y',]

for i in datasets:
	filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + i + '.npy'
	google_storage_obj.download_blob_to_dest(filename, os.path.join(dirname, filename))

datasets = ['TRAIN_index', 'DEV_index']
for i in datasets:
	filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_' + i + '.csv'
	google_storage_obj.download_blob_to_dest(filename, os.path.join(dirname, filename))

#############################
train_X_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TRAIN_X.npy'
train_X_filename = os.path.join(dirname, train_X_filename)
train_X = np.load(train_X_filename)
os.remove(train_X_filename)

train_y_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TRAIN_y.npy'
train_y_filename = os.path.join(dirname, train_y_filename)
train_y = np.load(train_y_filename)
os.remove(train_y_filename)

train_index_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_TRAIN_index.csv'
train_index_filename = 	os.path.join(dirname, train_index_filename)
with open(train_index_filename, 'r') as f:
	reader = csv.reader(f)
	train_index = list(reader)
	train_index = [x[0] for x in train_index]
os.remove(train_index_filename)


dev_X_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_DEV_X.npy'
dev_X_filename = os.path.join(dirname, dev_X_filename)
dev_X = np.load(dev_X_filename)
os.remove(dev_X_filename)

dev_y_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_DEV_y.npy'
dev_y_filename = os.path.join(dirname, dev_y_filename)
dev_y = np.load(dev_y_filename)
os.remove(dev_y_filename)

dev_index_filename = 'processed_model_inputs/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_DEV_index.csv'
dev_index_filename = os.path.join(dirname, dev_index_filename)
with open(dev_index_filename, 'r') as f:
	reader = csv.reader(f)
	dev_index = list(reader)
	dev_index = [x[0] for x in dev_index]
os.remove(dev_index_filename)

print (train_X)
print ('Making predictions')
train_y_pred = model.predict(train_X, verbose=2, batch_size=batch_size)
print (train_y_pred)
exit()
#train_y_pred = [x[0] for x in train_y_pred]

#NOTE: This must be included for a stateful LSTM
#model.reset_states()

dev_y_pred = model.predict(dev_X, verbose=2, batch_size=batch_size)
dev_y_pred = [x[0] for x in dev_y_pred]

#final_datasets = {'TRAIN': [train_y_index, train_X, train_y, train_y_pred], 'DEV': [dev_y_index, dev_X, dev_y, dev_y_pred], 'TEST': [test_y_index, test_X, test_y]}
final_datasets = {'TRAIN': [train_index, train_X, train_y, train_y_pred], 'DEV': [dev_index, dev_X, dev_y, dev_y_pred]}




print ('#############################')
print ('FINAL DATASETS')
print (final_datasets.keys())
print ('#############################')


if model_type == 'REGRESSOR':
	for dataset_split, data_list in final_datasets.items():
		'''
		#If there is no predictions, continue (this is the TEST set)
		if len(data_list) < 4:
			continue
			'''
		y_index = data_list[0]
		y = data_list[2]
		y_pred = data_list[3]


		print ('Length of y_index: ' + str(len(y_index)))
		print ('Length of predictions:  ' + str(len(y_pred)))
		print ('Length of y-actual:  ' + str(len(y)))

		'''
		print ('#############################')
		print('Y')
		print (y[:10])
		print ('#############################')
		print('Y-Predictions')
		print (y_pred[:10])
		print ('#############################')
		print('Index')
		print (y_index[:10])
		print ('#############################')
		'''

		d = {'y':y, 'y_pred':y_pred}

		y_ypred_df = pd.DataFrame(d, index=y_index)


		print (y_ypred_df.head(10))


		#NOTE: The keys of this dict must match the column names in the SQL table
		performance_metrics_dict = calc_regressor_performance(y_ypred_df)

		print (performance_metrics_dict)

		sql = "INSERT INTO regressor_performance (model_id,dataset_split,"

		for k in performance_metrics_dict.keys():
			sql += str(k) + ","

		sql = sql[:-1] #Remove "," comma from last entry

		sql += ") VALUES ("
		sql += str(model_id) + ",'" + dataset_split + "',"

		for v in performance_metrics_dict.values():
			sql += str(v) + ","

		sql = sql[:-1] #Remove "," comma from last entry
		sql += ")"

		#sql = "INSERT INTO regressor_performance (model_id, mean_squared_error_score, r2_score, explained_variance_score, mean_absolute_error_score, median_absolute_error_score) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')" % (str(model_id), str(mse_score), str(r2_score), str(explained_variance_score), str(mean_abs_error_score), str(median_abs_error_score))
		print(db_manager.update_database(sql))

		sql = "SELECT LAST_INSERT_ID();"
		performance_id = db_manager.fetch_mysql_result(sql)[0]['LAST_INSERT_ID()']
		print ('ID:  ' + str(performance_id))

		pred_filename =  'predictions/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_model_performance_id_' + str(performance_id) + '_' + dataset_split + '.csv'
		pred_filename_local = os.path.join(dirname, pred_filename)
		print (pred_filename_local)
		#NOTE: This only works if the bucket/folder structure in Google Storage is the same as local
		pred_filename_gs = pred_filename

		with open(pred_filename_local, "w") as f:
			writer = csv.writer(f)
			for i in range(0, len(y_index)):
				writer.writerow([y_index[i], y_pred[i]])

		google_storage_obj.upload_blob_from_file(pred_filename_local, pred_filename_gs)
		os.remove(pred_filename_local)

		if dataset_split == 'TRAIN':
			sql = "UPDATE models SET train_predictions_file_gs = '" + pred_filename_gs + "' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)
		elif dataset_split == 'DEV':
			sql = "UPDATE models SET dev_predictions_file_gs = '" + pred_filename_gs + "' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)

			sql = "UPDATE models SET performance_measured = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)




elif model_type == 'CLASSIFIER':
	print ('CLASSIFIER')
	#ROC, F1,
	#calc_classifier_performance()


	for dataset_split, data_list in final_datasets.items():
		'''
		#If there is no predictions, continue (this is the TEST set)
		if len(data_list) < 4:
			continue
			'''
		y_index = data_list[0]
		y = data_list[2]
		y_pred = data_list[3]


		print ('Length of y_index: ' + str(len(y_index)))
		print ('Length of predictions:  ' + str(len(y_pred)))
		print ('Length of y-actual:  ' + str(len(y)))

		'''
		print ('#############################')
		print('Y')
		print (y[:10])
		print ('#############################')
		print('Y-Predictions')
		print (y_pred[:10])
		print ('#############################')
		print('Index')
		print (y_index[:10])
		print ('#############################')
		'''

		d = {'y':y.argmax(axis=1), 'y_pred':y_pred.argmax(axis=1)}
		'''
		print ('YYYYYYYYY')
		print (dataset_split)
		print (y)
		print (y.argmax(axis=1))
		print ('Y-PRED')
		print (y_pred)
		print (y_pred.argmax(axis=1))
		'''
		y_ypred_df = pd.DataFrame(d, index=y_index)

		y_ypred_df.to_csv(str(model_id) + '-y-yhat-' + dataset_split +'.csv')

		print (y_ypred_df.head(10))


		#NOTE: The keys of this dict must match the column names in the SQL table
		performance_metrics_dict = calc_classifier_performance(y_ypred_df['y'], y_ypred_df['y_pred'])

		print (performance_metrics_dict)

		sql = "INSERT INTO regressor_performance (model_id,dataset_split,"

		for k in performance_metrics_dict.keys():
			sql += str(k) + ","

		sql = sql[:-1] #Remove "," comma from last entry

		sql += ") VALUES ("
		sql += str(model_id) + ",'" + dataset_split + "',"

		for v in performance_metrics_dict.values():
			sql += str(v) + ","

		sql = sql[:-1] #Remove "," comma from last entry
		sql += ")"

		#sql = "INSERT INTO regressor_performance (model_id, mean_squared_error_score, r2_score, explained_variance_score, mean_absolute_error_score, median_absolute_error_score) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')" % (str(model_id), str(mse_score), str(r2_score), str(explained_variance_score), str(mean_abs_error_score), str(median_abs_error_score))
		print(db_manager.update_database(sql))

		sql = "SELECT LAST_INSERT_ID();"
		performance_id = db_manager.fetch_mysql_result(sql)[0]['LAST_INSERT_ID()']
		print ('ID:  ' + str(performance_id))

		pred_filename =  'predictions/model_id_' + str(model_id) + '_dataset_id_' + str(dataset_id) + '_model_performance_id_' + str(performance_id) + '_' + dataset_split + '.csv'
		pred_filename_local = os.path.join(dirname, pred_filename)
		print (pred_filename_local)
		#NOTE: This only works if the bucket/folder structure in Google Storage is the same as local
		pred_filename_gs = pred_filename

		with open(pred_filename_local, "w") as f:
			writer = csv.writer(f)
			for i in range(0, len(y_index)):
				writer.writerow([y_index[i], y_pred[i]])

		google_storage_obj.upload_blob_from_file(pred_filename_local, pred_filename_gs)
		os.remove(pred_filename_local)

		if dataset_split == 'TRAIN':
			sql = "UPDATE models SET train_predictions_file_gs = '" + pred_filename_gs + "' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)
		elif dataset_split == 'DEV':
			sql = "UPDATE models SET dev_predictions_file_gs = '" + pred_filename_gs + "' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)

			sql = "UPDATE models SET performance_measured = 'COMPLETE' WHERE id = '" + str(model_id) + "'"
			db_manager.update_database(sql)

else:
	print ('Unknown model type (regressor/classifier)')


restart_server()
