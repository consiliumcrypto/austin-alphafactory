# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 35.239.75.139 (MySQL 5.7.25-google-log)
# Database: TEST-austin_jan2_2020
# Generation Time: 2021-01-12 05:54:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table backtest_runs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `backtest_runs`;

CREATE TABLE `backtest_runs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `strategy_id` int(11) DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `sharpe_ratio` int(11) DEFAULT NULL,
  `sortino_ratio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table backtest_trades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `backtest_trades`;

CREATE TABLE `backtest_trades` (
  `backtest_id` int(11) DEFAULT NULL,
  `quote_volume` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `trade_datetime` int(11) DEFAULT NULL,
  `trade_type` enum('BUY','SELL','TIME_STOP','PRICE_STOP','TAKE_PROFIT') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table classifier_performance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `classifier_performance`;

CREATE TABLE `classifier_performance` (
  `model_id` int(11) unsigned DEFAULT NULL,
  `dataset_split` enum('TRAIN','DEV','TEST') DEFAULT NULL,
  `up_tp_ratio` float DEFAULT NULL,
  `down_tp_ratio` float DEFAULT NULL,
  `neutral_tp_ratio` float DEFAULT NULL,
  `f1_weighted` float DEFAULT NULL,
  `up_f1` float DEFAULT NULL,
  `down_f1` float DEFAULT NULL,
  `neutral_f1` float DEFAULT NULL,
  `up_capture_ratio` float DEFAULT NULL,
  `down_capture_ratio` float DEFAULT NULL,
  `neutral_capture_ratio` float DEFAULT NULL,
  `accuracy` float DEFAULT NULL,
  KEY `model_id` (`model_id`),
  CONSTRAINT `classifier_performance_ibfk_1` FOREIGN KEY (`model_id`) REFERENCES `random_forest_models` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table datasets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `datasets`;

CREATE TABLE `datasets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sampling_type` enum('TIME_BARS','VOLUME_BARS','TICK BARS') DEFAULT NULL,
  `sampling_period` tinytext,
  `target_type` enum('PRICE','VOLUME','VOLATILITY','PERCENT_RETURN','ABSOLUTE_RETURN','BINARY_MOMENTUM','TERNARY_MOMENTUM') DEFAULT NULL,
  `target_col_name` tinytext,
  `prediction_horizon_periods` int(11) DEFAULT NULL,
  `prediction_horizon` tinytext,
  `asset_pair` tinytext,
  `exchange` tinytext,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `filename_gs` text,
  `file_hash` tinytext,
  `bucket_name_gs` text,
  `index_col_name` tinytext,
  `dataset_hash` tinytext,
  `neutral_perc_threshold` float DEFAULT NULL,
  `num_up_labels` int(11) DEFAULT NULL,
  `num_down_labels` int(11) DEFAULT NULL,
  `num_neutral_labels` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table lstm_models
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lstm_models`;

CREATE TABLE `lstm_models` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `loss_type` tinytext,
  `optimizer` tinytext,
  `max_epochs` int(11) DEFAULT NULL,
  `model_file_gs` tinytext,
  `batch_size` int(11) DEFAULT NULL,
  `dataset_id` int(11) unsigned NOT NULL DEFAULT '1',
  `model_status` enum('COMPLETE','RUNNING','WAITING') DEFAULT NULL,
  `num_steps` int(11) DEFAULT NULL,
  `instance_specs` int(11) DEFAULT NULL,
  `server_performance_logs_file` int(11) DEFAULT NULL,
  `software_versions` int(11) DEFAULT NULL,
  `model_type` enum('REGRESSOR','CLASSIFIER') DEFAULT NULL,
  `train_set_perc` float DEFAULT NULL,
  `dev_set_perc` float DEFAULT '0.1',
  `test_set_perc` float DEFAULT '0.1',
  `performance_measured` enum('COMPLETE','RUNNING','WAITING') DEFAULT 'WAITING',
  `processed_features` int(11) DEFAULT NULL,
  `early_stopping_triggered` enum('TRUE','FALSE') DEFAULT NULL,
  `normalization_type` enum('BATCH_NORM','SHORT_ORDER','Z_SCORE','MIN_MAX') DEFAULT NULL,
  `early_stop_patience` int(11) DEFAULT NULL,
  `early_stop_delta` float DEFAULT NULL,
  `train_predictions_file_gs` tinytext,
  `dev_predictions_file_gs` tinytext,
  `bucket_name_gs` text,
  `random_seed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `d_id` (`dataset_id`),
  CONSTRAINT `lstm_models_ibfk_1` FOREIGN KEY (`dataset_id`) REFERENCES `datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table random_forest_models
# ------------------------------------------------------------

DROP TABLE IF EXISTS `random_forest_models`;

CREATE TABLE `random_forest_models` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `split_criterion` enum('gini','entropy','OPTUNA') DEFAULT 'gini',
  `pca_variance_perc` float DEFAULT '0',
  `model_file_gs` tinytext,
  `dataset_id` int(11) unsigned NOT NULL DEFAULT '1',
  `model_status` enum('COMPLETE','RUNNING','WAITING') DEFAULT NULL,
  `instance_specs` int(11) DEFAULT NULL,
  `server_performance_logs_file` int(11) DEFAULT NULL,
  `software_versions` int(11) DEFAULT NULL,
  `model_type` enum('REGRESSOR','CLASSIFIER') DEFAULT NULL,
  `train_set_perc` float DEFAULT NULL,
  `dev_set_perc` float DEFAULT '0.1',
  `test_set_perc` float DEFAULT '0.1',
  `performance_measured` enum('COMPLETE','RUNNING','WAITING') DEFAULT 'WAITING',
  `processed_features` int(11) DEFAULT NULL,
  `normalization_type` enum('BATCH_NORM','SHORT_ORDER','Z_SCORE','MIN_MAX') DEFAULT NULL,
  `train_predictions_file_gs` tinytext,
  `dev_predictions_file_gs` tinytext,
  `test_predictions_file_gs` tinytext,
  `bucket_name_gs` text,
  `random_seed` tinyint(4) DEFAULT NULL,
  `max_depth` tinytext,
  `max_features` enum('sqrt','log2','OPTUNA') DEFAULT 'sqrt',
  `max_samples` tinytext,
  `num_estimators` tinytext,
  `train_time_seconds` float DEFAULT NULL,
  `num_optuna_model_trials` int(11) NOT NULL DEFAULT '50',
  `pca_params_dict` text,
  `optuna_ta_dict` text,
  PRIMARY KEY (`id`),
  KEY `d_id` (`dataset_id`),
  CONSTRAINT `random_forest_models_ibfk_1` FOREIGN KEY (`dataset_id`) REFERENCES `datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table regressor_performance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regressor_performance`;

CREATE TABLE `regressor_performance` (
  `model_id` int(11) unsigned DEFAULT NULL,
  `dataset_split` enum('TRAIN','DEV','TEST') DEFAULT NULL,
  `accuracy` float DEFAULT NULL,
  `edge` float DEFAULT NULL,
  `noise` float DEFAULT NULL,
  `y_chg` float DEFAULT NULL,
  `y_pred_chg` float DEFAULT NULL,
  `prediction_calibration` float DEFAULT NULL,
  `capture_ratio` float DEFAULT NULL,
  `edge_up` float DEFAULT NULL,
  `edge_down` float DEFAULT NULL,
  `edge_win` float DEFAULT NULL,
  `edge_lose` float DEFAULT NULL,
  `r2_score` float DEFAULT NULL,
  `up_r2_score` float DEFAULT NULL,
  `down_r2_score` float DEFAULT NULL,
  `mean_squared_error_score` float DEFAULT NULL,
  `up_mean_squared_error_score` float DEFAULT NULL,
  `down_mean_squared_error_score` float DEFAULT NULL,
  `explained_variance_score` float DEFAULT NULL,
  `up_explained_variance_score` float DEFAULT NULL,
  `down_explained_variance_score` float DEFAULT NULL,
  `mean_absolute_error_score` float DEFAULT NULL,
  `up_mean_absolute_error_score` float DEFAULT NULL,
  `down_mean_absolute_error_score` float DEFAULT NULL,
  `median_absolute_error_score` float DEFAULT NULL,
  `up_median_absolute_error_score` float DEFAULT NULL,
  `down_median_absolute_error_score` float DEFAULT NULL,
  KEY `model_id` (`model_id`),
  CONSTRAINT `regressor_performance_ibfk_1` FOREIGN KEY (`model_id`) REFERENCES `lstm_models` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table strategies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `strategies`;

CREATE TABLE `strategies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table trading_signals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trading_signals`;

CREATE TABLE `trading_signals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL,
  `strategy_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `strategy_id` (`strategy_id`),
  CONSTRAINT `model_id` FOREIGN KEY (`model_id`) REFERENCES `lstm_models` (`id`),
  CONSTRAINT `strategy_id` FOREIGN KEY (`strategy_id`) REFERENCES `strategies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
